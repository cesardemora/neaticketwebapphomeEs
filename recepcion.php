<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if(!empty($_POST)){
      $directorio = $_POST['directorio'];
      $nombre1 = $_POST['archivo'];
      if (subir_fichero($directorio, 'archivo')){
         echo 'Archivo recibido correctamente /n';
         return true;
      }else{ 
         echo "No se ha recibido el archivo correctamente /n";
         return false;
      }
}
else{ 
    echo "Llamada vacia";
    return false;
}

/**
 * subir_fichero()
 *
 * Sube una imagen al servidor  al directorio especificado teniendo el Atributo 'Name' del campo archivo.
 *
 * @param string $directorio_destino Directorio de destino dónde queremos dejar el archivo
 * @param string $nombre_fichero Atributo 'Name' del campo archivo
 * @return boolean
 */
function subir_fichero($directorio_destino, $nombre_fichero)
{
    $tmp_name = $_FILES[$nombre_fichero]['tmp_name'];
    echo "Directorio Destino: " .$directorio_destino."\n";
    echo "Nombre fichero: " .$nombre_fichero."\n";
    echo "Existe direc: " . is_dir($directorio_destino)."\n";
    echo "tmp_name: " .$tmp_name."\n";
    
    if(!is_dir($directorio_destino)) {
        echo "No existe directorio, intento crearlo /n";
        if(!mkdir($directorio_destino, 0777, true)) {
            $directorio_destino = "TicketsPDF";
            die('Fallo al crear las carpetas...');
        }
    }

    //si hemos enviado un directorio que existe realmente y hemos subido el archivo    
    if (is_dir($directorio_destino) && is_uploaded_file($tmp_name))
    {
        $img_file = $_FILES[$nombre_fichero]['name'];
        $img_type = $_FILES[$nombre_fichero]['type'];
        echo "Directorio OK! /n";
        echo "img_type: " .$img_type ."/n";
        // Si se trata de una imagen   
//        if (((strpos($img_type, "pdf") || strpos($img_type, "xml") ||
//            strpos($img_type, "txt")) || strpos($img_type, "png")))
//        {
            //¿Tenemos permisos para subir la imágen?
            echo "¿Tenemos permisos?";
            echo "/n";
            if (move_uploaded_file($tmp_name, $directorio_destino . '/' . $img_file))
            {
                echo dirname(__FILE__); 
                echo "SI /n";
                return true;
            }else
                echo "NO /n";
//        }else
//            echo "tipo de archivo no permitido /n";
    }else
            echo "directorio no existe o error al subir archivo temporal /n";
    //Si llegamos hasta aquí es que algo ha fallado
    return false;
}

?>

