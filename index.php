<?php
	require_once ('config.php');
	require('languages.php');

	$lang = null;
	if ( isset($_GET['lang']) ){
		$lang = $_GET['lang'];
	}
?>

<!DOCTYPE html>
<!-- <html lang="en"> -->
<html lang="es">

	<head>

        <meta charset="utf-8" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
        <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1">

		<!-- New -->
		<meta name="title" content="<?php echo PROJECT_NAME; ?>">
		<title><?php echo PROJECT_NAME; ?></title>

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta content="index, follow" name="robots" />
		<meta http-equiv="x-ua-compatible" content="chrome=1">
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="Neaticket: Tus tickets siempre contigo">
		<meta name="author" content="Neaticket">
		<meta name="keywords" content="ticket, tickets, facturas, compras, organizar, gestion">

		<!-- Meta Apple -->
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap3/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/animate.css">
		<link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/media-queries.css">

        <link rel="stylesheet" href="assets/font/typicons.min.css">

        <link rel="stylesheet" href="amd/daterangepicker.css">

        <!-- ICON NEEDS FONT AWESOME FOR CHEVRON UP ICON -->
		<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

	</head>

    <body>

		<!-- Loader -->
	    	<div class="loader">
	    		<div class="loader-img"></div>
	    	</div>

		<!-- Top menu -->
			<nav class="navbar navbar-inverse navbar-fixed-top navbar-no-bg" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="javascript:" id="return-to-top2"><?php echo PROJECT_NAME; ?></a>
					</div>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="top-navbar-1">
						<ul class="nav navbar-nav navbar-right">
							<li><a class="scroll-link" href="#top-content"><?php echo __(Inicio, $lang) ?></a></li>
							<li><a class="scroll-link" href="#features"><strong><?php echo __(Usuarios, $lang) ?></strong></a></li>
							<li><a class="scroll-link" href="#how-it-works" style="color: #66d6c5;"><strong><?php echo __('Empresas', $lang) ?></strong></a></li>
							<li><a class="scroll-link" href="#testimonials"><?php echo __(App, $lang) ?></a></li>
							<li><a class="scroll-link" href="#about-us"><?php echo __(Clientes, $lang) ?></a></li>
							<li><a class="scroll-link" href="#contacto"><?php echo __(Contacto, $lang) ?></a></li>
							<li><a class="btn-warning" href="neaticketwebapp/index.php#page-top"><strong><?php echo __(Login, $lang) ?></strong></a></li>
						</ul>
					</div>
				</div>
			</nav>

		<!-- Top content -->
	        <div class="top-content">

	            <div class="inner-bg">
	                <div class="container">

	                    <div class="row">
	                        <div class="col-sm-8 col-sm-offset-2 home text">
	                            <h1 class="wow fadeInLeftBig"><?php echo __(WelcomeText, $lang) ?></h1>
	                            <h3 class="wow fadeInLeftBig"><?php echo __(WelcomeText1, $lang) ?>
	                            <div class="description wow fadeInLeftBig">
	                            	<p></p>
	                            </div>
	                            <div class="text-center mar-t-40">
									<a href="http://bit.ly/1X8u6Br" target="_blank">
										<!--<img class="mar-t-20" style="width: 15%;min-width: 198px;" src="https://static1.fintonic.com/resources/img/landing/google_play_transparent_v0.png"/>-->
										<img class="mar-t-20" style="width: 30%;min-width: 198px;" src="assets/img/about/googlePlay.png">
									</a>
									<a href="https://itunes.apple.com/es/" target="_blank">
										<img class="mar-t-20" style="width: 30%;min-width: 198px;" src="assets/img/about/appStore.png">
										<!--<img class="mar-t-20" style="width: 15%;min-width: 198px;" src="https://static.fintonic.com/resources/img/landing/app_store_transparent_v0.png"/>-->
									</a>
								</div>
	                            <div class="top-big-link wow fadeInUp">

									<!-- <div class="col-sm-8 col-sm-offset-4 home2">
										<div class="dropdown">

										  <button class="btn btn3 btn-default dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-globe"></i> Select Language
										  <span class="caret"></span></button>
										  <ul class="dropdown-menu">
										    <li><a href="http://localhost/neaticketwebapphomeEs/index.php?lang=en">English</a></li>
										    <li><a href="http://localhost/neaticketwebapphomeEs/">Español</a></li>
										  </ul>
										</div>
									</div> -->

									<div class="container-fluid">
									    <div class="row centered">
									        <div class="col-sm-6 col-md-6">
												<div class="dropdown">

												  <button class="btn btn3 dropdown-toggle" type="button" data-toggle="dropdown" data-hover="dropdown" data-delay="100" data-close-others="false aria-haspopup="true" aria-expanded="true"><i class="fa fa-globe"></i> Select Language
												  <span class="caret"></span></button>
												  <ul class="dropdown-menu dropdown-menu-right">
												    <li><a tabindex="-1" href="http://192.168.1.138/neaticketwebapphomeEs/index.php?lang=en">English</a></li>
												    <li><a href="http://192.168.1.138/neaticketwebapphomeEs/">Español</a></li>
												  </ul>
												</div>
									        </div>
									    </div>
									</div>

	                            </div>
	                        </div>
	                    </div>

	                </div>
	            </div>

	        </div>

      <div class="container">
        <div class="row">
        	<input type="text" id="daterange" value="01/01/2015 - 01/31/2015" />

          <!-- <input type="text" name="birthdate" value="01/01/2016" /> -->
        </div>
      </div>


		<!-- Features -->
	        <div class="features-container section-container">
		        <div class="container">
		            <div class="row">
		                <div class="col-sm-12 features section-description wow fadeIn">
		                    <?php echo __(UsuariosText, $lang) ?>
		                    <div class="divider-1 wow fadeInUp"><span></span></div>
		                </div>
		            </div>
		            <div class="row">
	                	<div class="col-sm-4 features-box wow fadeInUp">
		                	<div class="features-box-icon">
		                		<i class="fa fa-thumbs-o-up"></i>
		                	</div>
		                    <?php echo __(UsuariosText1, $lang) ?>
	                    </div>
	                    <div class="col-sm-4 features-box wow fadeInDown">
		                	<div class="features-box-icon">
		                		<i class="fa fa-clock-o"></i>
		                	</div>
		                	<?php echo __(UsuariosText2, $lang) ?>
	                    </div>
	                    <div class="col-sm-4 features-box wow fadeInUp">
		                	<div class="features-box-icon">
		                		<i class="fa fa-list-alt"></i>
		                	</div>
		                    <?php echo __(UsuariosText3, $lang) ?>
	                    </div>
		            </div>
		            <div class="row">
		            	<div class="col-sm-12 section-bottom-button wow fadeInUp">
		            		<a class="btn btn2 btn-link-1b" href="neaticketwebapp/index_registroNew.php"><strong>Regístrate Gratis</strong></a>
		            	</div>
		            	<!-- <div class="col-sm-12 section-bottom-button wow fadeInUp">
	                        <a class="btn btn-link-1 scroll-link" href="#more-features">Leer más</a>
		            	</div> -->
		            </div>
		        </div>
	        </div>

		<!-- Great support -->
	        <!-- <div class="more-features-container section-container"> -->
	        <div class="more-features-container section-container section-container-gray-bg">
		        <div class="container">
		            <div class="row">
		                <div class="col-sm-12 more-features section-description wow fadeIn">
		                    <h2>¿Qué es <strong>Neaticket</strong>?</h2>
		                    <div class="divider-1 wow fadeInUp"><span></span></div>
		                </div>
		            </div>
		            <div class="row">
		            	<div class="col-sm-7 more-features-box wow fadeInLeft">
		                    <div class="more-features-box-text more-features-box-text-left">
		                    	<!-- <h3>¿Qué es Neaticket?</h3> -->
		                    	<p>
									Neaticket es sistema de tickets de compra digitales para no perderlos nunca y tenerlos siempre disponibles al instante desde tu móvil. Además<strong class=""> Neaticket</strong> ofrece unas serie de funcionalidades extra muy útiles (caducidad de devolución y de garantía, ofertas, etc...) que te facilitan la vida enormemente.
		                    	</p>
		                    	<p class="medium-paragraph">
		                    	Todos los tickets que hayas pedido en Neaticket los tendrás accesibles en tu cuenta de Neaticket. Podrás acceder a ella de dos formas:
		                    	<li>A través de tu <a href="neaticketwebapp/index.php#page-top">zona privada</a> con tu cuentas.</li>
								<li>A través de la app descargándola de las tiendas de <a href="http://bit.ly/1X8u6Br" target="_blank">Google</a></span> y <span class="colored-text">Apple</span>.</li>
		                    	</p>
		                    </div>
		                </div>
		                <div class="col-sm-5 more-features-box wow fadeInUp">
		                    <img src="assets/img/devices/pc01.png" alt="">
		                </div>
		            </div>
		        </div>
	        </div>

		<!-- How it works -->
	        <div class="how-it-works-container section-container section-container-image-bg">
		        <div class="container">
		            <div class="row">
		                <div class="col-sm-12 how-it-works section-description wow fadeIn">
		                    <h2>Empresas:<strong> Neaticket</strong> te permite...</h2>
		                    <div class="divider-1 wow fadeInUp"><span></span></div>
		                </div>
		            </div>
		            <div class="row">
	                	<div class="col-sm-4 col-sm-offset-1 how-it-works-box wow fadeInUp">
		                	<div class="how-it-works-box-icon">1</div>
		                    <h3>Empresa tecnológica</h3>
		                    <p>Enviar a tus clientes el ticket electrónico situando tu empresa en la vanguardia de la tecnología.</p>
	                    </div>
	                    <div class="col-sm-4 col-sm-offset-2 how-it-works-box wow fadeInDown">
		                	<div class="how-it-works-box-icon">2</div>
		                    <h3>Conoce a tus clientes</h3>
		                    <p>Conoce los hábitos de consumo de tus clientes y adapta tu oferta a sus gustos.</p>
	                    </div>
	                </div>
	                <div class="row">
	                    <div class="col-sm-4 col-sm-offset-1 how-it-works-box wow fadeInUp">
		                	<div class="how-it-works-box-icon">3</div>
		                    <h3>Fácil instalación</h3>
		                    <p>Neaticket es una tecnología única y compatible con cualquier TPV del mercado. Olvídate de integraciones complejas, Neaticket se instala en tan solo 2 minutos.</p>
	                    </div>
	                    <div class="col-sm-4 col-sm-offset-2 how-it-works-box wow fadeInDown">
		                	<div class="how-it-works-box-icon">4</div>
		                    <h3>Rápido y Seguro</h3>
		                    <p>Solicita a tus clientes el email y en tan solos unos segundos recibirá su ticket de compra por email y automáticamente lo tendrá almacenado en su perfil de Neaticket.
		                    </p>
	                    </div>
		            </div>
		            <div class="row">
		            	<div class="col-sm-12 section-bottom-button wow fadeInUp">
		            	<a class="btn btn2 btn-link-1b" href="neaticketwebapp/index_registroNewEmp.php"><strong>Regístrate Gratis</strong></a>
		            	</div>
		            </div>
		        </div>
	        </div>

		<!-- Testimonials -->
	        <div class="testimonials-container section-container">
		        <div class="container">
		            <div class="row">
		                <div class="col-sm-12 testimonials section-description wow fadeIn">
		                    <h2><strong>Nuestra</strong> App</h2>
		                    <div class="divider-1 wow fadeInUp"><span></span></div>
		                </div>
		            </div>

			<!-- Carousel ****************************************************************************************************** -->

						        <div class="row">
						            <div class="col-sm-10 col-sm-offset-1 screenshots-box wow fadeInUp">
						                <div id="carousel-example-generic" class="carousel slide">
										  <!-- Indicators -->
										  <ol class="carousel-indicators">
										    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
										    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
										    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
										  </ol>
										  <!-- Wrapper for slides -->
										  <div class="carousel-inner">
										    <div class="item active">
										      <img src="assets/img/devices/1a2.png" alt="">
											<div class="testimonial-text">
												<p>
												"Descárgate la aplicación gratuita y entra con tu email y contraseña..."<br>
												</p>
											</div>
										    </div>
										    <div class="item">
										      <img src="assets/img/devices/1b2.png" alt="">
										      <div class="testimonial-text">
												<p>
												"Ten todos tus tickets de compra ordenados y recibe ofertas exclusivas..."<br>
												</p>
											</div>
										    </div>
										    <div class="item">
										      <img src="assets/img/devices/3b.png" alt="">
										      <div class="testimonial-text">
												<p>
												"Tus tickets con todos los detalles y válidos para cambios o devoluciones..."<br>
												</p>
											</div>
										    </div>
										  </div>
										   <!-- Controls -->
										<!--   <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
										    <span class="icon-prev"></span>
										  </a>
										  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
										    <span class="icon-next"></span>
										  </a> -->
										</div>
						            </div>
						        </div>

			<!-- Carousel End ****************************************************************************************************** -->

		        </div>
	        </div>

	        <!-- Call to action -->
	        <div class="call-to-action-container section-container section-container-image-bg">
		        <div class="container">
		            <div class="row">
		                <div class="col-sm-12 call-to-action section-description wow fadeInLeftBig">
		                    <h2><strong>Descarga</strong> neaticket</h2>
		                    <div class="divider-1 wow fadeInUp"><span></span></div>
								<p>
								"Descárgate nuestra app gratuita y olvídate del papel."
								</p>
		                </div>
		            </div>
					<div class="text-center mar-t-40">
						<a href="http://bit.ly/1X8u6Br" target="_blank">
							<!--<img class="mar-t-20" style="width: 15%;min-width: 198px;" src="https://static1.fintonic.com/resources/img/landing/google_play_transparent_v0.png"/>-->
							<img class="mar-t-20" style="width: 30%;min-width: 198px;" src="assets/img/about/googlePlay.png">
						</a>
						<a href="https://itunes.apple.com/es/" target="_blank">
							<img class="mar-t-20" style="width: 30%;min-width: 198px;" src="assets/img/about/appStore.png">
							<!--<img class="mar-t-20" style="width: 15%;min-width: 198px;" src="https://static.fintonic.com/resources/img/landing/app_store_transparent_v0.png"/>-->
						</a>
					</div>
		            <div class="row">
		            	<!-- <div class="col-sm-12 section-bottom-button wow fadeInUp">
		            	<a href="http://bit.ly/1X8u6Br" target="_blank">
							<img src="assets/img/about/app_google.jpg"></a>
						<a href="https://itunes.apple.com/es/" target="_blank">
							<img src="assets/img/about/app_ios.jpg"></a> -->
	    					<!-- <div class="vendors wow fadeInUp animated">
							    <a href="http://bit.ly/1X8u6Br" target="_blank"><span title="Descarga la app para Android" class="typcn typcn-vendor-android"></span></a>
							    <a href="#"><span title="En breve tendremos la versión para iOS" class="typcn typcn-vendor-apple"></span></a>
							    <a href="#"><span class="typcn typcn-vendor-microsoft"></span></a>
							</div> -->
	                        <!-- <a class="btn btn-link-1 scroll-link" href="#top-content">Sign up now</a> -->
		            	</div>
		            </div>
		        </div>
	        </div>

		<!-- About us -->
	        <div class="about-us-container section-container">
		        <div class="container">
		            <div class="row">
		                <div class="col-sm-12 about-us section-description wow fadeIn">
		                    <h2>Nuestros<strong> Clientes</strong></h2>
		                    <div class="divider-1 wow fadeInUp"><span></span></div>
		                </div>
		            </div>
		            <div class="row">
		                <div class="col-sm-4 about-us-box wow fadeInUp">
			                <div class="about-us-photo">
			                	<img src="assets/img/about/1b.jpg" alt="" data-at2x="assets/img/about/1b.jpg">
			                </div>
		                    <h3>Ulanka</h3>
		                    <p>"Neaticket es una de las mejores aplicaciones para gestionar y organizar los tickets".</p>
		                    <div class="about-us-social">
			                	<a href="https://www.facebook.com/ulankaonline/"><i class="fa fa-facebook"></i></a>
			                    <a href="https://twitter.com/ulankaonline?lang=es"><i class="fa fa-twitter"></i></a>
			                </div>
		                </div>
		                <div class="col-sm-4 about-us-box wow fadeInDown">
			                <div class="about-us-photo">
			                	<img src="assets/img/about/2b.jpg" alt="" data-at2x="assets/img/about/2b.jpg">
			                </div>
		                    <h3>GloboSur</h3>
		                    <p>"Más del 80% de nuestros clientes nos solicitan el ticket electrónico".</p>
		                    <div class="about-us-social">
			                	<a href="https://es-es.facebook.com/Globo-Sur-558808154174617/"><i class="fa fa-facebook"></i></a>
			                    <!-- <a href="#"><i class="fa fa-twitter"></i></a> -->
			                </div>
		                </div>
		                <div class="col-sm-4 about-us-box wow fadeInUp">
			                <div class="about-us-photo">
			                	<img src="assets/img/about/3b.jpg" alt="" data-at2x="assets/img/about/3b.jpg">
			                </div>
		                    <h3>Maydo</h3>
		                    <p>"Nuestros clientes estan encantados con el servicio que les ofrecemos".</p>
		                    <div class="about-us-social">
			                	<!-- <a href="#"><i class="fa fa-facebook"></i></a>
			                    <a href="#"><i class="fa fa-twitter"></i></a> -->
			                </div>
		                </div>
		            </div>
		        </div>
	        </div>


		<!-- Top content: CONTACTO PRUEBA -->
	        <div class="top-content2 contacto-container section-container">

	            <div class="inner-bg2">
	                <div class="container">

	                    <div class="row">
	                        <div class="col-sm-8 col-sm-offset-2 text">
	                            <h1 class=" wow fadeInLeftBig"><strong>Contacta</strong> con Nosotros</h1>
	                            <div class="divider-1 wow fadeInUp"><span></span></div>
	                            <p class="wow fadeInLeftBig">¡Llámanos al teléfono <strong>963 259 608</strong> o envíanos un email!</p>
	                            <div class="description wow fadeInLeftBig">
	                            	<p></p>
	                            </div>
	                        </div>
							<div class="col-sm-6 col-sm-offset-3 form-box">
									<div class="form-top">
										<div class="form-top-left">
											<h3>Donde estamos</h3>
											<address>
												<i class="fa fa-phone"></i>
												&nbsp; 963 259 608<br>
												<i class="fa fa-map-marker"></i>
												<a href="https://www.google.es/maps/place/Carrer+de+Don+Juan+de+Austria,+9,+46002+Val%C3%A8ncia,+Valencia/@39.4702862,-0.3720176,17z/data=!4m5!3m4!1s0xd6048b37e3555ad:0x9c503cd16be92f3d!8m2!3d39.4704353!4d-0.3726291" target="_blank">
												&nbsp; Don Juan de Austria_9<br>
												&nbsp; &nbsp; 46002 Valencia _Spain</a><br>
												<i class="fa fa-envelope"></i>
												<a href="mailto:info@neaticket.com" target="_top">&nbsp; info@neaticket.com</a>
											</address>
											<p class="consulta">Escríbenos tu consulta:</p>
										</div>
										<div class="form-top-right">
											<i class="fa fa-envelope"></i>
										</div>
									</div>
									<div class="form-bottom contact-form">
										<form role="form" action="assets/contacto.php" method="post" enctype="plain" class="form-group">
											<div class="form-group">
												<label class="sr-only" for="nombre">Subject</label>
												<input class="contact-subject form-control" id="nombre" type="text" name="nombre" placeholder="Nombre..." required="">
											</div>
											<div class="form-group ">
												<label class="sr-only" for="email">Email</label>
												<input class="contact-email form-control" id="email" type="email" name="email" placeholder="ejemplo@correo.com" required="">
											</div>
											<div class="form-group">
												<label class="sr-only" for="mensaje">Message</label>
												<textarea class="contact-message form-control" id="mensaje" name="mensaje" placeholder="Escribe tu mensaje..." required=""></textarea>
											</div>
											<!-- <input class="btn" id="submit" type="submit" name="submit" value="Enviar" /> -->
											<!-- <button class="btn" id="submit" type="submit" name="submit" value="Enviar">Enviar Mensaje</button> -->
											<!-- <button class="btn form-control" id="submit" type="submit" name="submit">Enviar</button> -->
											<input class="btn btn2 btn-link-1b" id="submit" type="submit" name="submit" value="Enviar Mensaje">
										</form>
									</div>
							</div>
	                    </div>

	                </div>
	            </div>

	        </div>



		<!-- Footer -->
	        <footer>
	        	<div class="container section-container">
					<div class="row">
						<div class="col-sm-12 footer-copyright footer__copyrights">
							<div class="container">
								<div class="app-store-links-footer-wrapper">
									<div class="app-store-footer-link">
										<a id="download-android-bottom" href="http://bit.ly/1X8u6Br" target="_blank"><img src="./assets/img/about/google_play.png"></a>
									</div>
									<div class="app-store-footer-link">
										<!-- <a id="download-iphone-bottom" href="https://itunes.apple.com/app/apple-store/id879185767?pt=96143108&amp;ct" onclick="window._tfa = window._tfa || []; _tfa.push({notify:'action', name:'user-download'});"><img src="./assets/img/about/app_store.png"></a> -->
										<a id="download-iphone-bottom" href="https://itunes.apple.com" onclick="window._tfa = window._tfa || []; _tfa.push({notify:'action', name:'user-download'});"  target="_blank"><img src="./assets/img/about/app_store.png"></a>
									</div>
								</div>
								<div class="divider-1 wow fadeInUp"><span></span></div>
								<span class="launch-modal footer__copyrights-link">© 2016 Neaticket.com</span>
								<a href="#" class="launch-modal footer__copyrights-link" data-modal-id="modal-privacy">Política de Privacidad</a>
								<a href="#" class="launch-modal footer__copyrights-link" data-modal-id="modal-privacy2">Aviso Legal</a>
								<a href="#" class="launch-modal footer__copyrights-link" data-modal-id="modal-faq">FAQ</a>
		                    </div>
		                </div>
			        </div>
				</div>
				<!-- Scroll to Top ================================================== -->
				<a href="javascript:" id="return-to-top"><i class="fa fa-chevron-up" aria-hidden="true"></i></a> <!-- ARROW UP -->
	        </footer>


		<!-- MODAL: Privacy policy -->
			<?php include("neaticketwebapp/lib/modalNew.php"); ?>

		<!-- Javascript -->

        <script src="assets/js/jquery-1.11.3.min.js"></script>
        <script src="assets/bootstrap3/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/retina-1.1.0.min.js"></script>
        <script src="assets/js/waypoints.min.js"></script>
        <script src="assets/js/scripts.js"></script>

        <!-- <script src="amd/require.js" data-main="amd/main.js"></script> -->

        <!-- <script src="assets/js/scripts-dropdown.js"></script> -->
        <script src="assets/js/bootstrap-hover-dropdown.min.js"></script>


        <!-- <script src="assets/js/js-contact/scripts.js"></script> -->

        <!-- Bootstrap Carousel Script ================================================== -->
		<script>
		$('.carousel').carousel({
		  interval: 3500
		})
		</script>

        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>