requirejs.config({
    "paths": {
      "jquery": "https://code.jquery.com/jquery-1.11.3.min",
      // "jquery": "../assetsNew/js/jquery-1.11.3.min",
      "moment": "moment",
      "daterangepicker": "daterangepicker"
    }
});

requirejs(['jquery', 'moment', 'daterangepicker'] , function ($, moment) {
  $(document).ready(function() {

    updateConfig();

    function updateConfig() {
      var options = {};

        // $(function() {
        //     $('input[name="daterange"]').daterangepicker();
        //     $('#daterange').daterangepicker({
        //       locale: { cancelLabel: 'Clear' }
        //     });

        //     $('#daterange').on('cancel.daterangepicker', function(ev, picker) {
        //       //do something, like clearing an input
        //       $('#daterange').val('');
        //     });
        //     //create a new date range picker
        //     // $('#daterange').daterangepicker({ startDate: '01/01/2015', endDate: '01/02/2015' });
        // });

        $(function() {

        function cb(start, end) {
            $('#daterange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        cb(moment().subtract(29, 'days'), moment());

        $('#daterange').daterangepicker({
            ranges: {
               'Hoy': [moment(), moment()],
               'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
               'Últimos 7 Dias': [moment().subtract(6, 'days'), moment()],
               'Últimos 30 Dias': [moment().subtract(29, 'days'), moment()],
               'Éste Mes': [moment().startOf('month'), moment().endOf('month')],
               'Último Mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
             locale: {
                "format": "DD-MM-YYYY",
                "separator": "  ·  ",
                "applyLabel": "Aplicar",
                "cancelLabel": "Cancelar",
                "fromLabel": "Desde",
                "toLabel": "Hasta",
                "customRangeLabel": "Elegir Fechas",
                "weekLabel": "S",
                "daysOfWeek": [
                    "Do",
                    "Lu",
                    "Ma",
                    "Mi",
                    "Ju",
                    "Vi",
                    "Sa"
                ],
                "monthNames": [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agosto",
                    "Septiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                ],
                "firstDay": 1
            }
        }, cb);

    });
    }
  });
});


