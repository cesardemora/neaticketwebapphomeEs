<!DOCTYPE html>
<!-- <html lang="en"> -->
<html lang="es">

    <head>

        <meta charset="utf-8" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
        <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1">

        <!-- New -->
        <meta name="title" content="<?php echo PROJECT_NAME; ?>">
        <title><?php echo PROJECT_NAME; ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="index, follow" name="robots" />
        <meta http-equiv="x-ua-compatible" content="chrome=1">
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="Neaticket: Tus tickets siempre contigo">
        <meta name="author" content="Neaticket">
        <meta name="keywords" content="ticket, tickets, facturas, compras, organizar, gestion">

        <!-- Meta Apple -->
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap3/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <!-- <link rel="stylesheet" href="assets/css/form-elements.css"> -->
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/media-queries.css">

        <link rel="stylesheet" href="assets/font/typicons.min.css">

        <!-- ICON NEEDS FONT AWESOME FOR CHEVRON UP ICON -->
        <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">



    </head>

    <body>

    <div class="container">
        <div class="row demo">
          <input type="text" id="daterange" value="01/01/2016">
          <!-- <input type="text" name="daterange" value="01/01/2015 - 01/31/2015" /> -->
        </div>

    </div>

                        <input type="text" name="birthdate" value="01/01/2015 - 01/31/2015" />


        <div class="well well-sm">
          <form name="myForm" role="form" novalidate="novalidate" class="form-inline">
            <div class="form-group">
              <label>Busqueda por:
              <select id="cband" class="form-control">
                <option value="C15+">Día</option>
                <option value="C12-14">Semana</option>
                <option value="Other">Mes</option>
              </select>
                  </label>
            </div>
            <div class="form-group">
              <label>C-Band
              <select ng-model="form.cband2" id="cband2" class="form-control">
                <option value="C15+">C15+</option>
                <option value="C12-14">C12-14</option>
                <option value="Other">Other</option>
              </select>
                  </label>
            </div>
            <button class="btn btn-primary">Filter</button>
          </form>
        </div>

        <select id="e1">
        <option value="AL">Alabama</option>
        ...
        <option value="WY">Wyoming</option>
    </select>

        <div class="container2">

        <div class="row">
          <div class="col-md-9">.col-md-9 .col-md-push-3
            <div class="form-group">

                <div class="row">
                    <div class="col-lg-4">
                        <form method="POST" class="form" action=""">
                            <label class="control-label">Búsqueda por Día:</label>
                            <div class="input-group">
                                <input type="date" class="form-control" name="dia" placeholder="Dia" required="">
                                <!-- <input placeholder="Día" class="form-control" type="text" name="dia" onfocus="this.type='date';this.setAttribute('onfocus','');this.blur();this.focus();"> -->
                                <span class="input-group-btn">
                                <button class="btn btn-secondary" type="submit" name="BuscarDia">Ver</button>
                                </span>
                            </div>
                        </form>
                    </div>

                    <div class="col-lg-4">
                        <form method="POST" class="form" action="">
                            <label class="control-label">Búsqueda por Semana:</label>
                            <div class="input-group">
                                <input type="week" class="form-control" name="semana" placeholder='' required="">
                                <!-- <input placeholder="Semana" class="form-control" type="text" name="semana" onfocus="this.type='week';this.setAttribute('onfocus','');this.blur();this.focus();"> -->
                                <span class="input-group-btn">
                                <button class="btn btn-secondary" type="submit" name="BuscarSemana">Ver</button>
                                </span>
                            </div>
                        </form>
                    </div>

                    <div class="col-lg-4">
                        <form method="POST" class="form" action="">
                            <label class="control-label">Búsqueda por Mes:</label>
                            <div class="input-group">
                                <input type="month" class="form-control" name="mes" placeholder='Mes' required="">
                                <!-- <input placeholder="Mes" class="form-control" type="text" name="mes" onfocus="this.type='month';this.setAttribute('onfocus','');this.blur();this.focus();"> -->
                                <span class="input-group-btn">
                                <button class="btn btn-secondary" type="submit" name="BuscarMes">Ver</button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
          </div>
          <div class="col-md-3">.col-md-3 .col-md-pull-9

          </div>
        </div>

            
        </div>
        <!-- end -->

                                            <!-- Javascript -->

        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap3/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/retina-1.1.0.min.js"></script>
        <script src="assets/js/waypoints.min.js"></script>
        <script src="assets/js/scripts.js"></script>
        <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>

        <!-- <script src="assets/js/scripts-dropdown.js"></script> -->
        <script src="assets/js/bootstrap-hover-dropdown.min.js"></script>


        <!-- <script src="assets/js/js-contact/scripts.js"></script> -->


<input type="text" name="birthdate" value="10/24/1984" />
 
<script type="text/javascript">
$(function() {
    $('input[name="birthdate"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
    }, 
    function(start, end, label) {
        var years = moment().diff(start, 'years');
        alert("You are " + years + " years old.");
    });
});
</script>

        <!-- Bootstrap Carousel Script ================================================== -->
        <script>
        $('.carousel').carousel({
          interval: 3500
        })
        </script>



        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>