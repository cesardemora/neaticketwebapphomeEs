<?php
	require('languages.php');

	$lang = null;
	if ( isset($_GET['lang']) ){
		$lang = $_GET['lang'];
	}
?>
<!DOCTYPE html>
<html>
	<head><meta charset="utf-8"></head>
	<body>
		<nav>
			<a href="#"><?php echo __('About us', $lang) ?></a>
			<a href="#"><?php echo __('What we do?', $lang) ?></a>
			<a href="#"><?php echo __('Where we are?', $lang) ?></a>
			<a href="#"><?php echo __('Contact', $lang) ?></a>
		</nav>
	</body>
</html>