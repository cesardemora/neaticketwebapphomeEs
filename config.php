<?php

// define("DB_HOST", "localhost");
// define("DB_USER", "root");
// define("DB_PASS", "root");
// define("DB_NAME", "usuarios");

// define("DB_HOST", "db571105757.db.1and1.com");
// define("DB_USER", "dbo571105757");
// define("DB_PASS", "ticket4yourmamma");
// define("DB_NAME", "db571105757");

// define("DB_HOST", "217.160.131.215");
// define("DB_USER", "AdminBD");
// define("DB_PASS", "neaticket00");
// define("DB_NAME", "Neaticket");

define('DB_DRIVER', 'mysql');
define("DB_SERVER", "217.160.131.215");
define("DB_SERVER_USERNAME", "AdminBD");
define("DB_SERVER_PASSWORD", "neaticket00");
define("DB_DATABASE", "Neaticket");

// Meta Tittle **************************************************** //

define('PROJECT_NAME', 'Neaticket: Tus tickets siempre contigo.');

// Textos Español **************************************************** //

define('WelcomeText', 'Welcome To <strong>Neaticket</strong>');
define('WelcomeText1', '¡Todos tus <strong>tickets</strong> de compra en la nube de manera automática, busca y ordénalos rápidamente!</h3>');

define('UsuariosText', '<h2><strong>Usuarios</strong> ¿Como Funciona?</h2>');
define('UsuariosText1', '<h3>Fácil de usar</h3><p>Todos tus tickets de compra en la nube, ordenados y seguros. Olvídate de tickets perdidos, borrados o ilegibles.</p>');
define('UsuariosText2', '<h3>Todo al instante</h3><p>Te enviamos el ticket electrónico a tu email y ademas podrás consultarlo al instante desde la web o desde la app de Neaticket.</p>');
define('UsuariosText3', '<h3>Ordena tus tickets</h3><p>Gestiona y ordena tus tickets desde la web o desde la app y configura tus alertas de devolución. Recibe promociones exclusivas.</p>');

?>