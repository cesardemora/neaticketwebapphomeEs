
function scroll_to(clicked_link, nav_height) {
	var element_class = clicked_link.attr('href').replace('#', '.');
	var scroll_to = 0;
	if(element_class != '.top-content') {
		element_class += '-container';
		scroll_to = $(element_class).offset().top;
	}
	if($(window).scrollTop() != scroll_to) {
		$('html, body').stop().animate({scrollTop: scroll_to}, 1000);
	}
}


jQuery(document).ready(function() {

	/*
	    Navigation
	*/
	$('a.scroll-link').on('click', function(e) {
		e.preventDefault();
		scroll_to($(this), $('nav').outerHeight());
	});
	// toggle "navbar-no-bg" class
	$('.top-content .text').waypoint(function() {
		$('nav').toggleClass('navbar-no-bg');
	});

    /*
        Background slideshow
    */
    $('.top-content').backstretch("assets/img/backgrounds/2c.jpg");
    $('.how-it-works-container').backstretch("assets/img/backgrounds/2d.jpg");
    $('.call-to-action-container').backstretch("assets/img/backgrounds/2f.jpg");
    $('.contacto').backstretch("assets/img/backgrounds/2g.jpg");

    $('#top-navbar-1').on('shown.bs.collapse', function(){
    	$('.top-content').backstretch("resize");
    });
    $('#top-navbar-1').on('hidden.bs.collapse', function(){
    	$('.top-content').backstretch("resize");
    });

    /*
        Wow
    */
    new WOW().init();

});


jQuery(window).load(function() {

	/*
		Menu Collapse HIDE (Esconder)
	*/

	$(function(){
		var navMain = $("#top-navbar-1");
			navMain.on("click", "a", null, function () {
			navMain.collapse('hide');
		});
	});

	/*
		Loader
	*/
	$(".loader-img").fadeOut();
	$(".loader").delay(1000).fadeOut("slow");

	/*
		Hidden images
	*/
	$(".testimonial-image img").attr("style", "width: auto !important; height: auto !important;");



    /*
        Modals
    */
    $('.launch-modal').on('click', function(e){
        e.preventDefault();
        $( '#' + $(this).data('modal-id') ).modal();
    });



});



/* Top-content2 ********************************/

// function scroll_to(clicked_link, nav_height) {
// 	var element_class = clicked_link.attr('href').replace('#', '.');
// 	var scroll_to = 0;
// 	if(element_class != '.top-content2') {
// 		element_class += '-container';
// 		scroll_to = $(element_class).offset().top;
// 	}
// 	if($(window).scrollTop() != scroll_to) {
// 		$('html, body').stop().animate({scrollTop: scroll_to}, 1000);
// 	}
// }


jQuery(document).ready(function() {

	/*
	    Navigation
	*/
	$('a.scroll-link').on('click', function(e) {
		e.preventDefault();
		scroll_to($(this), $('nav').outerHeight());
	});
	// toggle "navbar-no-bg" class

	// $('.top-content2 .text').waypoint(function() {
	// 	$('nav').toggleClass('navbar-no-bg');
	// });

    /*
        Background slideshow
    */
    $('.top-content2').backstretch("assets/img/backgrounds/2g.jpg");

    $('#top-navbar-1').on('shown.bs.collapse', function(){
    	$('.top-content2').backstretch("resize");
    });
    $('#top-navbar-1').on('hidden.bs.collapse', function(){
    	$('.top-content2').backstretch("resize");
    });

    /*
        Wow
    */
    new WOW().init();

});


// ===== Scroll to Top ====
$(window).scroll(function() {
    if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
        $('#return-to-top').fadeIn(200);    // Fade in the arrow
    } else {
        $('#return-to-top').fadeOut(200);   // Else fade out the arrow
    }
});
$('#return-to-top').click(function() {      // When arrow is clicked
    $('body,html').animate({
        scrollTop : 0                       // Scroll to top of body
    }, 500);
});

// ===== Scroll to Top ====
$(window).scroll(function() {
    if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
        $('#return-to-top2');    // Fade in the arrow
    } else {
        $('#return-to-top2');   // Else fade out the arrow
    }
});
$('#return-to-top2').click(function() {      // When arrow is clicked
    $('body,html').animate({
        scrollTop : 0                       // Scroll to top of body
    }, 500);
});

/* Scroll to Top END ********************************/
