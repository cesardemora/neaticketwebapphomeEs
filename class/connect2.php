<?php

require_once ('config.php');
require_once ('lineaVacia.php');
// require_once ('funcTicket.php');

session_start();

function dbConnect (){
    
    $host = DB_HOST;
    $db =   DB_NAME;
    $user = DB_USER;
    $pwd =  DB_PASS;
    $conn = null;

    try {
        $conn = new PDO('mysql:host='.$host.';dbname='.$db, $user, $pwd);
        
        //echo 'Connected succesfully.<br>';
    }
    catch (PDOException $e) {
        echo '<p>Cannot connect to database !!</p>';
        exit;
    }
    return $conn;
 }

function generaPass(){

    $caracteres = "1234567890"; //posibles caracteres a usar
    $numerodeletras= 4; //numero de letras para generar el texto
    $cadena = ""; //variable para almacenar la cadena generada
        for($i=0;$i<$numerodeletras;$i++)
        {
        $cadena .= substr($caracteres,rand(0,strlen($caracteres)),1); /*Extraemos 1 caracter de los caracteres 
        entre el rango 0 a Numero de letras que tiene la cadena */
        }

    return $cadena;
}


    /////////////////////////////////////////////////////////////////////////////////////////

class connect {

    var $host = DB_HOST;
    var $user = DB_USER;
    var $pwd  = DB_PASS;
    var $db   = DB_NAME;
    var $con;

	
	public function dbConnects (){
		$con = mysql_connect($this->host, $this->user, $this->pwd)or die (mysql_error());
		mysql_select_db($this->db,$con) or die (mysql_error());
        if ($con === false){
            die("ERROR: No se estableció la conexión. ". mysqli_connect_error());
        } 
	
	}

    public function dbConnect2 (){
        $con = new mysqli($host, $user, $pwd, $db);
        if ($con === false){
            die("ERROR: No se estableció la conexión. ". mysqli_connect_error());
        } 

    }

	/////////////////////////////////////////////////////////////////////////////////////////

	/*con esta opcion verificamos que el usuario este registrado y logeado correctamente*/
	function user_login() {
	    if(!$_SESSION['id'])
	    {
	        exit ("<h5>Solo usuarios registrados y logeados podrán acceder a esta Página,</h5> <a href='javascript:history.back(-1)'>Volver</a>");
	    }

	}

	/////////////////////////////////////////////////////////////////////////////////////////

	function limpiar($var) {
		$var = trim($var);
		$var = htmlspecialchars($var);
		$var = str_replace(chr(160),'',$var);
		return $var;

	}

	/////////////////////////////////////////////////////////////////////////////////////////

	/*validamos que el mail esta escrito correctamente.*/
	function validar_email($email) {
	    $mail_correcto = 0; 
	    //compruebo unas cosas primeras 
	    if ((strlen($email) >= 6) && (substr_count($email,"@") == 1) && (substr($email,0,1) != "@") && (substr($email,strlen($email)-1,1) != "@"))
	    { 
	       if ((!strstr($email,"'")) && (!strstr($email,"\"")) && (!strstr($email,"\\")) && (!strstr($email,"\$")) && (!strstr($email," ")))
	       {//miro si tiene caracter .
	          if (substr_count($email,".")>= 1)
	          {//obtengo la terminacion del dominio 
	             $term_dom = substr(strrchr ($email, '.'),1); 
	             //compruebo que la terminacion del dominio sea correcta 
	             if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) )
	             {//compruebo que lo de antes del dominio sea correcto 
	                $antes_dom = substr($email,0,strlen($email) - strlen($term_dom) - 1); 
	                $caracter_ult = substr($antes_dom,strlen($antes_dom)-1,1); 
	                if ($caracter_ult != "@" && $caracter_ult != ".")
	                { 
	                   $mail_correcto = 1; 
	                }
	             }
	          }
	       }
	    }
	    if ($mail_correcto) 
	       return 1;
	    else 
	       return 0;

	}

	/////////////////////////////////////////////////////////////////////////////////////////

    

}

?>