<!-- Top menu -->
<nav class="navbar navbar-inverse navbar-fixed-top navbar-no-bg" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="javascript:" id="return-to-top2"><?php echo PROJECT_NAME; ?></a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="top-navbar-1">
			<ul class="nav navbar-nav navbar-right">
				<li><a class="scroll-link" href="#top-content"><?php echo __(Inicio, $lang) ?></a></li>
				<li><a class="scroll-link" href="#features"><strong><?php echo __(Usuarios, $lang) ?></strong></a></li>
				<li><a class="scroll-link" href="#how-it-works" style="color: #66d6c5;"><strong><?php echo __('Empresas', $lang) ?></strong></a></li>
				<li><a class="scroll-link" href="#testimonials"><?php echo __(App, $lang) ?></a></li>
				<li><a class="scroll-link" href="#about-us"><?php echo __(Clientes, $lang) ?></a></li>
				<li><a class="scroll-link" href="#contacto"><?php echo __(Contacto, $lang) ?></a></li>
				<li><a class="btn-warning" href="neaticketwebapp/index.php#page-top"><strong><?php echo __(Login, $lang) ?></strong></a></li>
			</ul>
		</div>
	</div>
</nav>