<?php

$email_cliente = $_POST['email'];
$pass = $_POST['password'];
$tipo = $_POST['tipo'];

$empresa = "";
$telefono = "";
$empresa = $_POST['empresa'];
$telefono = $_POST['telefono'];


crearMail($tipo,$email_cliente,$pass,$empresa,$telefono);

    /////////////////////////////////////////////////////////////////////////////////////////

    function generaPass(){

        $str = "1234567890";
        $cadena = "";
        for($i=0;$i<=4;$i++) {
        $cadena .= substr($str,rand(0,10),1);
        }

        return $cadena;
    }
    
    function crearMail($tipo, $email_user, $pass, $empresa, $telefono){

        require_once ("lib/class.phpmailer.php");
        require_once('lib/class.smtp.php');

        $mail = new PHPMailer(true);

        $name       = "NEATICKET"; // También puede ser un campo del formulario como asunto o mensaje.
        $SPLangDir  = "phpmailer/language/";
        
        switch ($tipo) {
            case "Recuperar":
                $subject    = "Recuperar Contraseña Neaticket"; 
                break;
            case "RegistroApp":
            case "RegistroDesktop":
                $subject    = "Nuevo Usuario de Neaticket"; 
                break;

            default:
                break;
        }
        $Mensage = obtenerBody($tipo, $email_user, $pass, $empresa, $telefono);

        $mail->IsSMTP();
        $mail->SetLanguage("es", $SPLangDir); // Set mailer to use SMTP
        $mail->Host = 'smtp.1and1.es'; // Specify main and backup SMTP servers
        $mail->SMTPAuth = true; // Enable SMTP authentication
        $mail->Username = "info@neaticket.com"; // SMTP username
        $mail->Password = "147258369"; // SMTP password
        $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;

        $mail->SetFrom('info@neaticket.com', 'Neaticket');

        $mail->From = "info@neaticket.com";
        $mail->FromName = utf8_decode($name);
        $mail->AddAddress("$email_user");        // Esta es la dirección a donde enviamos.

        $mail->SMTPDebug = 2;
        $mail->MailerDebug = false; // XXXXXXXXXXXXXXXXXXXXXX
        $mail->CharSet = "utf-8";
        $mail->IsHTML(true);
        $mail->Priority = 1;

        $mail->Subject = $subject; // Este es el titulo del email. Vamos el asunto.
        $mail->Body = $Mensage; // Mensaje a enviar. Yo aquí uso la plantilla que te comentaba.

        $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically

         if($mail->Send()){ // Envía el correo.
            $result .= "<b>El correo fue enviado correctamente.</b>";
            echo $result;
        }else{
            error_log($mail->ErrorInfo, 0);
            $result .= "<b class='red'>Hubo un inconveniente.</b><br />".$mail->ErrorInfo."Por favor,
            inténtalo más tarde.<br /><br />Recuerde que puede ponerse en contacto con nosotros por teléfono
            o directamente por correo electrónico.<br />Perdonen por las molestias causadas.";
            echo $result;
        }


    }
    
    function obtenerBody($tipo, $email_user, $pass, $empresa, $telefono){
        
        $body = "";
        switch ($tipo) {
            case "Recuperar":
                $body = "<html><head>"
                    . "<title>Neaticket</title>"
                    . "</head>"
                    . "<body>"

                    . "<div style=\"color:#000000; text-align: center;\">"
                    . "<img src=\"http://www.neaticket.com/assetsNew/img/logo_neatNew2.png\" /><hr>"
                    . "<h2>Recuperar Contraseña <b>Neaticket!</b></h2></div>"

                    . "<div style=\"color:#000000;\">"
                    . "Si recibes este correo es porque recientemente has intentado recuperar tu contraseña desde la App de <b>Neaticket.</b>Si no es tu caso, puedes ignorar su contenido.<br><br>"
                    . "</div>"

                    . "<div style=\"color:#000000;\">"
                    . "Ya puedes entrar en la app o en www.neaticket.com y gestionar tus tickets con las siguentes claves:"
                    . "</div>"

                    . "<div style=\"color:#000000;\">"
                    . "<h3>Usuario: ".$email_user."<br>"
                    . "Contraseña: <font color=#3366BB>".$pass."</font></h3>"
                    . "</div>"

                    . "<div style=\"color:#000000;\">"
                    . "Gracias por confiar en <b>Neaticket.</b><br>"
                    . "</div>"

                    . "<div style=\"color:#000000;\">"
                    . "<i>El Equipo Neaticket.</i><hr>"
                    . "<div>"

                    . "<div style=\"color:#696969; font-size:11px; text-align: center;\">"
                    . "<em>Copyright © 2016 www.neaticket.com - All rights reserved</em>"
                    . "</div>"
                    . "</body></html>";

                break;
            case "RegistroApp":
                $body = "<html><head>"
                    . "<title>Neaticket</title>"
                    . "</head>"
                    . "<body>"

                    . "<div style=\"color:#000000; text-align: center;\">"
                    . "<img src=\"http://www.neaticket.com/assetsNew/img/logo_neatNew2.png\" /><hr>"
                    . "<h2>Bienvenido a <b>Neaticket!</b></h2></div>"

                    . "<div style=\"color:#000000;\">"
                    . "Si recibes este correo es porque recientemente te has registrado desde la App de <b>Neaticket.</b><br><br>"
                    . "</div>"

                    . "<div style=\"color:#000000;\">"
                    . "Ya puedes entrar en la app o en www.neaticket.com y gestionar tus tickets con las siguentes claves:"
                    . "</div>"

                    . "<div style=\"color:#000000;\">"
                    . "<h3>Usuario: ".$email_user."<br>"
                    . "Contraseña: <font color=#3366BB>".$pass."</font></h3>"
                    . "</div>"

                    . "<div style=\"color:#000000;\">Ten en cuenta que a partir de ahora encontrarás tus tickets electrónicos en www.neaticket.com<br>" 
                    . "Y recuerda que nuestra <b>app gratuita te ayuda a gestionar las fechas de devolución</b> y garantía, "
                    . "además de informarte de las ofertas de tus establecimientos favoritos.<br><br></div>"

                    . "<div style=\"color:#000000;\">"
                    . "Gracias por confiar en <b>Neaticket.</b><br>"
                    . "</div>"

                    . "<div style=\"color:#000000;\">"
                    . "<i>El Equipo Neaticket.</i><hr>"
                    . "<div>"

                    . "<div style=\"color:#696969; font-size:11px; text-align: center;\">"
                    . "<em>Copyright © 2016 www.neaticket.com - All rights reserved<br><br></em>"
                    . "</div>"
                    
                    . "<div style=\"color:#696969; font-size:11px;\">"
                    . "<em>De acuerdo con lo establecido por la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal (LOPD), le informamos que sus datos serán incorporados a un fichero del que es titular THINKANDCLOUD,S.L. con la finalidad de realizar la gestión del ticket electrónico.<br>"
                    . "Así mismo le informamos que sus datos serán tratados únicamente desde nuestra aplicación y por parte del comercio donde realice sus compras y solicite el ticket electrónico, los cuales podrán enviarle comunicaciones comerciales sobre sus productos y servicios.<br>"
                    . "Asimismo, declaro haber sido informado de la posibilidad de ejercer los derechos de acceso, rectificación, cancelación y oposición de mis datos en el domicilio fiscal de THINKANDCLOUD,S.L. sito en C/ Don Juan de Austria, 9-1 - 46002 - Valencia - VALENCIA - info@thinkandcloud.com</em>"
                    . "</div>"
                    
                    . "</body></html>";
                break;
            case "RegistroDesktop":
                $body = "<html><head>"
                    . "<title>Neaticket</title>"
                    . "</head>"
                    . "<body>"

                    . "<div style=\"color:#000000; text-align: center;\">"
                    . "<img src=\"http://www.neaticket.com/assetsNew/img/logo_neatNew2.png\" /><hr>"
                    . "<h2>Bienvenido a <b>Neaticket!</b></h2></div>"

                    . "<div style=\"color:#000000;\">"
                    . "Si recibes esto es porque recientemente has aceptado un ticket electrónico en el comercio asociado a <b>Neaticket:</b><br><br>"
                    . "</div>"

                    . "<div style=\"color:#000000;\">"
                    . "Comercio: <b>".$empresa."</b><br>"
                    . "Telefono:".$telefono.""
                    . "</div><br>"

                    . "<div style=\"color:#000000;\">"
                    . "Ya puedes entrar en www.neaticket.com y gestionar tus tickets con las siguentes claves:"
                    . "</div>"

                    . "<div style=\"color:#000000;\">"
                    . "<h3>Usuario: ".$email_user."<br>"
                    . "Contraseña: <font color=#3366BB>".$pass."</font></h3>"
                    . "</div>"

                    . "<div style=\"color:#000000;\">Ten en cuenta que a partir de ahora encontrarás tus tickets electrónicos en www.neaticket.com<br>" 
                    . "Y recuerda que tienes disponible nuestra <b>app gratuita que te ayuda a gestionar las fechas de devolución</b> y garantía, "
                    . "además de informarte de las ofertas de tus establecimientos favoritos.</div>"

                    . "<div style=\"color:#000000; text-align: center;\">"
                    . "<h3>Descarga <b>Neaticket </b>en:</h3>"
                    . "<a href=\"https://play.google.com/store/apps/details?id=com.thinkandcloud.neaticket\" width=\"50%\" height=\"50%\" />"
                    . "<img src=\"http://quikcu.com/wp-content/themes/responsivo/img/app_google.png\" width=\"140\" /></a>"

                    // + "<a href=\"https://bit.ly/1X8u6Br\" width=\"50%\" height=\"50%\" />"
                    . "<img src=\"http://quikcu.com/wp-content/themes/responsivo/img/app_ios.png\" width=\"140\" /></a>"
                    . "</div><br>"

                    . "<div style=\"color:#000000;\">"
                    . "Gracias por comprar en <b>".$empresa.".</b><br>"
                    . "</div>"

                    . "<div style=\"color:#000000;\">"
                    . "<i>El Equipo Neaticket.</i><hr>"
                    . "<div>"

                    . "<div style=\"color:#696969; font-size:11px; text-align: center;\">"
                    . "<em>Copyright © 2016 www.neaticket.com - All rights reserved<br><br></em>"
                    . "</div>"
                                        
                    . "<div style=\"color:#696969; font-size:11px;\">"
                    . "<em>De acuerdo con lo establecido por la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal (LOPD), le informamos que sus datos han sido facilitados por el/los comercios donde a realizado la petición del ticket electrónico, éstos serán incorporados a un fichero del que es titular THINKANDCLOUD,S.L. con la finalidad de realizar la gestión del ticket electrónico.<br>"
                    . "Así mismo le informamos que sus datos serán tratados únicamente desde nuestra aplicación y por parte del comercio donde realice sus compras y solicite el ticket electrónico, los cuales podrán enviarle comunicaciones comerciales sobre sus productos y servicios.<br>"
                    . "Asimismo, declaro haber sido informado de la posibilidad de ejercer los derechos de acceso, rectificación, cancelación y oposición de mis datos en el domicilio fiscal de THINKANDCLOUD,S.L. sito en C/ Don Juan de Austria, 9-1 - 46002 - Valencia - VALENCIA - info@thinkandcloud.com</em>"
                    . "</div>"
                    
                    . "</body></html>";
                break;

            default:
                break;
        }
        return $body;
    }

 ?>