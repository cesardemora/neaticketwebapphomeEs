<?php
$texts = array(
	Inicio => 'Home',
	Usuarios => 'Users',
	Empresas => 'Business',
	App => 'App',
	Clientes => 'Customers',
	Contacto => 'Contact',
	Login => 'Login',

	WelcomeText => 'Welcome To <strong>Neaticket</strong>',
	WelcomeText1 => 'All your <strong>tickets</strong> purchase in the cloud automatically, locate and arrange them quickly!',

	UsuariosText => '<h2><strong>Users</strong> How does it work?</h2>',
	UsuariosText1 => '<h3>Easy to use</h3>
	<p>All your purchase receipts in the cloud, orderly and safe. Forget lost, deleted or illegible tickets.</p>',
	UsuariosText2 => '<h3>All at once</h3>
	<p> We send the electronic ticket to your email and you can also consult instantly from the web or from the app Neaticket.</p>',
	UsuariosText3 => '<h3>Order your tickets </h3>
	<p> Manage and order your tickets from the web or from the app and set your alerts return. Receive exclusive promotions.</p>'



);
?>