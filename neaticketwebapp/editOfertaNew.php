<?php
	require_once ('class/connect.php');

	$conn = dbConnect();
	$OK = true; // We use this to verify the status of the update.
	$msg = '';
        
        $clase = new connect;
        $clase->dbConnects();
        //$cif_empresa = $_SESSION['cif_empresa'];
        $nombre_marca = $_SESSION['nombre_marca'];
            
	// If 'buscar' is in the array $_POST proceed to make the query.
	if (isset($_GET['id_oferta'])) {
		// Create the query
		$sql = 'SELECT * FROM OFERTAS WHERE id = ? AND nombre_marca = ?';
		// we have to tell the PDO that we are going to send values to the query
                $conn->exec("set names utf8");
		$stmt = $conn->prepare($sql);
		// Now we execute the query passing an array toe execute();
		$results = $stmt->execute(array($_GET['id_oferta'],$nombre_marca));
		// Extract the values from $result
		$row = $stmt->fetch();

		if (empty($row)) {
			$result = "No se encontraron resultados !!";
		}                  
	}
                    
        if (isset($_GET['funcion'])) {
            $funcion = $_GET['funcion'];
        }
        if ($funcion == "Eliminar"){
            
            $activado = "disabled";
            
            $fecha_actual = date("y-m-d");
            $dateFin = date("y-m-d", strtotime($row['fecha_fin']));
            $dateIni = date("y-m-d", strtotime($row['fecha_ini']));

            if ($dateIni <= $fecha_actual && $dateFin >= $fecha_actual){
                $boton = "";               
                $msg = '<h5><font color="#FF0000">La oferta está vigente. No se puede eliminar si no modifica sus fechas.</font></h5>';
                       
            }else{
                $boton = "<button type='submit' name='delete' class='btn4'>Eliminar</button>";
            }
            
        }else{
            $activado = "";
            if ($funcion == "Editar"){
                $boton = "<button type='submit' name='update' class='btn4'>Actualizar</button>";
            }
        }
	// remove comment to verify the information in the $_POST array.
	// var_dump($_POST);
	if (array_key_exists('update', $_POST)) {
                        
            // Comprobamos si ha ocurrido un error.
            if (isset($_FILES["imagen"]) && $_FILES["imagen"]["error"] === 0)
            {
                // Verificamos si el tipo de archivo es un tipo de imagen permitido.
                // y que el tamaño del archivo no exceda los 16MB
                $permitidos = array("image/jpg", "image/jpeg", "image/png");
                $limite_kb = 16384;

                if (in_array($_FILES['imagen']['type'], $permitidos) && $_FILES['imagen']['size'] <= $limite_kb * 1024)
                {
                    // Archivo temporal
                    $imagen_temporal = $_FILES['imagen']['tmp_name'];

                    // Leemos el contenido del archivo temporal en binario.
                    $fp = fopen($imagen_temporal, 'r+b');
                    $data = fread($fp, filesize($imagen_temporal));
                    fclose($fp);

                    // Escapamos los caracteres para que se puedan almacenar en la base de datos correctamente.
                    $data = mysql_escape_string($data);
                    
                    $sql = "UPDATE OFERTAS SET nombre = '".$_POST['nick']."', texto = '".$_POST['texto']."', fecha_ini = '".$_POST['fecha_ini']."', fecha_fin = '".$_POST['fecha_fin']."', imagen_oferta = '".$data."' WHERE id = '".$_GET['id_oferta']."' AND nombre_marca ='".$nombre_marca."'";  
                        
                }else{
                    echo "Formato de archivo no permitido o excede el tamaño límite de $limite_kb Kbytes.";
                }           
            }else{
                
                $sql = "UPDATE OFERTAS SET nombre = '".$_POST['nick']."', texto = '".$_POST['texto']."', fecha_ini = '".$_POST['fecha_ini']."', fecha_fin = '".$_POST['fecha_fin']."' WHERE id = '".$_GET['id_oferta']."' AND nombre_marca ='".$nombre_marca."'";               
            }
            
            if(! mysql_query($sql)){
                die('Ocurrio un error ejecutando el query [' . mysql_error() . ']');
            }else{
                header("refresh: 0;index_ofertas.php");    
                exit;
            }
            
	}
        if (array_key_exists('delete', $_POST)) {
                                           
            // Create the query
            $sql = 'UPDATE OFERTAS SET estado = ? WHERE id = ? AND nombre_marca = ?';
            // we have to tell the PDO that we are going to send values to the query
            $stmt = $conn->prepare($sql);
            // Now we execute the query passing an array to execute();
            $OK = $stmt->execute(array('borrada',$_GET['id_oferta'],$nombre_marca));
            // In case of any error, we get the values.
            $error = $stmt->errorInfo();
            // We use this to verify the integrity of the update.
            if (!$OK) {
                    echo $error[2];

            } else {
                    // echo '<p>El registro se actualizo correctamente</p>';
                    header("refresh: 0;index_ofertas.php");
                    exit;
            }
	}
//        if ($data === 'vacia'){
//            $imagen = $row['imagen_oferta'];
//        }else{
//            $imagen = $data; 
//        }
 ?>

<!DOCTYPE html>
<html lang="en">

    <head>

        <?php include("lib/headNew.php"); ?>

    </head>

    <body>

        <!-- Top menu -->
        <nav class="navbar navbar-inverse navbar-no-bg" role="navigation">

            <?php include("lib/navNew.php"); ?>

        </nav>

        <!-- Loader -->
        <div class="loader">
            <div class="loader-img"></div>
        </div>

            <div class="container">
                <div class="row">
                    <div  id="capa" class="container wow fadeInUp">

                        <?php
                            // If there are no records.
                            if(!$OK) :
                                    echo "";
                            else :
                        ?>

                                <div class="jumbotron">
                        <h1><?php echo $funcion; ?><strong> Oferta</strong></h1>
                            <form form role="form" class="registration-form" name="imagen" id="form1" method="post" action="" enctype="multipart/form-data">
                                <div>

                                    <div class="form-group">
                                        <label class="sr-only2" for="form-id">Id</label>
                                        <input type="text" name="id" id="id" disabled="disabled" value="<?php echo $row['id'];?>" class="form-first-name form-control">
                                    </div>

                                    <div class="form-group">
                                        <label class="sr-only2" for="form-nick">Nombre</label>
                                        <input type="text" name="nick" id="nick" value="<?php echo $row['nombre'];?>" <?php echo $activado;?> class="form-first-name form-control">
                                    </div>

                                    <div class="form-group">
                                        <label class="sr-only2" for="form-id">Texto</label>
                                        <input type="text" name="texto" id="texto" value="<?php echo $row['texto'];?>" <?php echo $activado;?> class="form-first-name form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label class="sr-only2" for="form-id">Fecha Inicio</label>
                                        <input type="date" name="fecha_ini" id="fecha_ini" value="<?php echo $row['fecha_ini'];?>" <?php echo $activado;?> class="form-first-name form-control">
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only2" for="form-id">Fecha Fin</label>
                                        <input type="date" name="fecha_fin" id="fecha_fin" value="<?php echo $row['fecha_fin'];?>" <?php echo $activado;?> class="form-first-name form-control">
                                    </div>       
                                    <div class="form-group">
                                        <label class="sr-only2" for="form-id">Imagen Actual</label><br>
                                        <img src="data:image/jpeg;base64,<?php echo base64_encode($row['imagen_oferta']);?>"  name="vistaPrevia">
                                        
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only2" for="form-id">Nueva Imagen</label><br>                                       
                                        <input type='file' class='form-control-static' placeholder='imagen' name="imagen" id="imagen" <?php echo $activado;?>>
                                    </div>
                                    
                                    <div class="form-group btn2">
                                        <?php echo $boton;?>
                                        <?php echo $msg;?>
<!--                                        <button type="submit" name="update" class="btn4">Actualizar</button>-->
                                        <a href="index_ofertas.php"><button name="boton2" id="boton2" type="button" class="btn">
                                        <span class="typcn typcn-arrow-left"></span> Volver</button></a>
                                    </div>

                                </div>

                        <?php endif;?>
                            </form>
                    </div>
                            <!-- </form> -->
                    </div>
            </div>
    </div> <!--/ .container -->

<!-- MODAL: Privacy policy -->
        <?php include("lib/modalNew.php"); ?>

        <!-- Javascript -->
        <script type="" src="assetsNew/js/jquery-1.11.3.min.js"></script>
        <script type="" src="assetsNew/js/bootstrap.min.js"></script>
        <script type="" src="assetsNew/js/jquery.backstretch.min.js"></script>
        <script type="" src="assetsNew/js/retina-1.1.0.min.js"></script>
        <script type="" src="assetsNew/js/wow.min.js"></script>
        <script type="" src="assetsNew/js/scripts.js"></script>
        <script type="" src="assetsNew/js/scripts-screen.js"></script>
        <script type="" src="assetsNew/js/main.js"></script>

    </body>

</html>