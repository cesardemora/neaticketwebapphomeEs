       <!-- Loader -->
        <div class="loader">
            <div class="loader-img"></div>
        </div>

        <div class="top-content">

        <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text wow fadeInUp">
                            <h1>Welcome to <strong>Neaticket</strong></h1>
                            <div class="description">
                                <p>
                                    Gestiona tus compras.
                                    La forma más fácil de gestionar tus tickets.
                                    La mejor App para organizar tus compras.
                                    Cambia, devuelve,... tus productos.!
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 phone wow fadeInLeft">
                            <img src="assetsNew/img/neaticket_mobile4.png" alt="">
                        </div>
                        <div class="col-sm-5 form-box wow fadeInUp">
                            <div class="form-top">
                                <div class="form-top-left">
                                    <h3>Login de Usuario</h3>
                                    <p>Entra ahora para ver tus tickets:</p>
                                </div>
                                <div class="form-top-right">
                                    <span aria-hidden="true" class="typcn typcn-pencil"></span>
                                </div>
                                <!-- <div class="form-top-right">
                                    <i class="fa fa-pencil"></i>
                                </div> -->
                            </div>
                            <div class="form-bottom">

                                <!-- <div class="social-buttons">
                                Login via
                                    <a href="#" class="btn btn-fb"><i class="fa fa-facebook"></i> Facebook</a>
                                    <a href="#" class="btn btn-tw"><i class="fa fa-twitter"></i> Twitter</a>
                                </div> -->
                                <form form role="form" class="registration-form" name="login_user" id="form1" method="post" action="">
                                <?php
                                    include("lib/loginNew.php");
                                ?>
                                </form>
                            </div>
                            <!-- <div class="top-big-link">
                                <a class="btn btn-link-1" href="index2.php">Usuarios</a>
                                <a class="btn btn-link-2" href="index2emp.php">Empresas</a>
                            </div> -->
                        </div>
                    </div>
                        <div class="top-big-link wow fadeInUp">
                            <a class="btn btn-link-1" href="#">Download it</a>
                            <a class="btn btn-link-2 scroll-link" href="#app-features">Learn more</a>
                        </div>
                        <div class="vendors">
                            <a href="http://bit.ly/1X8u6Br"><span class="typcn typcn-vendor-android"></span></a>
                            <a href="#"><span class="typcn typcn-vendor-apple"></span></a>
                            <!-- <a href="#"><span class="typcn typcn-vendor-microsoft"></span></a> -->
                        </div>
                        <div class="form-links description">
                            <a href="#" class="launch-modal" data-modal-id="modal-privacy">Privacy Policy</a> -
                            <a href="#" class="launch-modal" data-modal-id="modal-faq">FAQ</a>
                        </div>

                        <!-- <div class="" >
                            <a href="#"><img src="http://techulus.com/buttons/fb.png" /></a><br/>
                            <a href="#"><img src="http://techulus.com/buttons/tw.png" /></a><br/>
                            <a href="#"><img src="http://techulus.com/buttons/gplus.png" /></a>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>