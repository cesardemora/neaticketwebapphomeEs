<!-- MODAL: Privacy policy -->
        <div class="modal fade" id="modal-privacy" tabindex="-1" role="dialog" aria-labelledby="modal-privacy-label" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                        </button>
                        <h2 class="modal-title" id="modal-privacy-label">Política de Privacidad</h2>
                    </div>
                    <div class="modal-body">
                        <h3>1. Política de Privacidad</h3>
                        <p>
							Cuando precisemos obtener información por su parte, siempre le solicitaremos que nos la proporcione voluntariamente de forma expresa.
							Los datos recabados a través de los formularios de recogida de datos del sitio web u otras vías serán incorporados a un
							fichero de datos de carácter personal debidamente inscrito en el Registro General de Protección de Datos de la Agencia Española de 
							Protección de Datos, del cual es responsable Thinkandcloud S.L. (en adelante: Neaticket).
							Esta entidad tratará los datos de forma confidencial y exclusivamente con la finalidad de ofrecer los servicios solicitados, 
							con todas las garantías legales y de seguridad que impone la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos 
							de Carácter Personal, elReal Decreto 1720/2007, de 21 de diciembre y la Ley 34/2002, de 11 de julio, de Servicios de la 
							Sociedad de la Información y de Comercio Electrónico. 
							Neaticket no cederá ni compartirá los datos con terceros sin su consentimiento expreso.

                        </p>
                        <p>
                            Asimismo, Neaticket, cancelará o rectificará los datos cuando resulten inexactos, incompletos o hayan dejado de ser necesarios o 
							pertinentes para su finalidad, de conformidad con lo previsto en la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de 
							Datos de Carácter Personal. El usuario podrá revocar el consentimiento prestado y ejercer los derechos de acceso, rectificación, 
							cancelación y oposición dirigiéndose a tal efecto en el domicilio siguiente: Thinkandcloud S.L., sito en 
							C/ Juan de Austria, 9, Piso 1 – 46002 Valencia (ESPAÑA) identificándose debidamente e indicando de forma expresa el concreto 
							derecho que se quiere ejercer.
                        </p>
                        <h3>2. Niveles de seguridad</h3>
                        <p>
                            Neaticket adopta los niveles de seguridad correspondientes requeridos por la citada Ley Orgánica 15/1999 y demás normativa aplicable. 
							No obstante, no puede garantizar la absoluta invulnerabilidad de los sistemas, por tanto, no asume ninguna responsabilidad por los daños y 
							perjuicios derivados de alteraciones que terceros puedan causar en los sistemas informáticos, documentos electrónicos o ficheros del
							usuario.
                        </p>
                        <h3>3. Responsabilidad</h3>
                        <p>
                            Si opta a abandonar nuestro sitio web a través de enlaces a sitios web no pertenecientes a nuestra entidad, Neaticket no se hará 
							responsable de las políticas de privacidad de dichos sitios web ni de las cookies que éstos puedan almacenar en el ordenador del
							usuario.
                        </p>
                        <p>
                            Nuestra política con respecto al correo electrónico se centra en remitir únicamente comunicaciones que usted haya solicitado recibir. 
							Si prefiere no recibir estos mensajes por correo electrónico le ofreceremos a través de los mismos la posibilidad de ejercer su 
							derecho de cancelación y renuncia a la recepción de estos mensajes, en conformidad con lo dispuesto en el Título III, 
							artículo 22 de la Ley 34/2002, de Servicios para la Sociedad de la Información y de Comercio Electrónico.
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- MODAL: Privacy policy -->
        <div class="modal fade" id="modal-privacy2" tabindex="-1" role="dialog" aria-labelledby="modal-privacy-label" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                        </button>
                        <h2 class="modal-title" id="modal-privacy-label">Aviso Legal</h2>
                    </div>
                    <div class="modal-body">
                        <h3>La utilización de www.neaticket.com</h3>
                        <p>
                            El presente aviso legal regula el uso y utilización del sitio web www.neaticket.com, del que es titular Thinkandcloud S.L. (en adelante, Neaticket).
                            La navegación por el sitio web de Neaticket le atribuye la condición de Usuario del mismo y conlleva su aceptación plena y sin reservas de todas y cada una de las condiciones publicadas en este aviso legal, advirtiendo de que dichas condiciones podrán ser modificadas sin notificación previa por parte de Neaticket, en cuyo caso se procederá a su publicación y aviso con la máxima antelación posible.
                            Por ello es recomendable leer atentamente su contenido en caso de desear acceder y hacer uso de la información y de los servicios ofrecidos desde este sitio web.
                            El usuario además se obliga a hacer un uso correcto del sitio web de conformidad con las leyes, la buena fe, el orden público, los usos del tráfico y el presente Aviso Legal, y responderá frente a Neaticket o frente a terceros, de cualesquiera daños y perjuicios que pudieran causarse como consecuencia del incumplimiento de dicha obligación.
                            Cualquier utilización distinta a la autorizada está expresamente prohibida, pudiendo Neaticket denegar o retirar el acceso y su uso en cualquier momento.
                        </p>
                        <p>
                            Asimismo, Neaticket, cancelará o rectificará los datos cuando resulten inexactos, incompletos o hayan dejado de ser necesarios o 
                            pertinentes para su finalidad, de conformidad con lo previsto en la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de 
                            Datos de Carácter Personal. El usuario podrá revocar el consentimiento prestado y ejercer los derechos de acceso, rectificación, 
                            cancelación y oposición dirigiéndose a tal efecto en el domicilio siguiente: Thinkandcloud S.L., sito en 
                            C/ Juan de Austria, 9, Piso 1 – 46002 Valencia (España) identificándose debidamente e indicando de forma expresa el concreto 
                            derecho que se quiere ejercer.
                        </p>
                        <h3>1. Identificación</h3>
                        <p>
                            Neaticket, en cumplimiento de la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Información y de Comercio Electrónico, le informa de que:
                            <ul>
                                <li>Su denominación social es: Thinkandcloud S.L.</li>
                                <li>Su nombre comercial es: Neaticket</li>
                                <li>Su CIF es: B98492754</li>
                                <li>Su domicilio social está en: C/ Don Juan de Austria, 9, Piso 1 – 46002 Valencia</li>
                                <li>Está inscrita en el Registro Mercantil de Valencia • Tomo 9557, Libro:6839, Folio:176, Hoja:V-151493, Inscripción 1ª</li>
                            </ul>
                        </p>
                        <h3>2. Comunicaciones</h3>
                        <p>
                            Para comunicarse con nosotros, ponemos a su disposición diferentes medios de contacto que detallamos a continuación:
                            <ul>
                                <li>Tlf: 963259608</li>
                                <li>Email: info@thinkandcloud.com</li>
                            </ul>
                            Todas las notificaciones y comunicaciones entre los usuarios y Neaticket se considerarán eficaces, a todos los efectos, cuando se realicen a través de cualquier medio de los detallados anteriormente.
                        </p>
                        <h3>3. Condiciones de acceso y utilización</h3>
                        <p>
                            El sitio web y sus servicios son de acceso libre y gratuito. No obstante, Neaticket puede condicionar la utilización de algunos de los servicios ofrecidos en su web a la previa cumplimentación del correspondiente formulario.
                            El usuario garantiza la autenticidad y actualidad de todos aquellos datos que comunique a Neaticket y será el único responsable de las manifestaciones falsas o inexactas que realice.
                            El usuario se compromete expresamente a hacer un uso adecuado de los contenidos y servicios de Neaticket y a no emplearlos para, entre otros:
                            <ol type="a">
                                <li>Difundir contenidos, delictivos, violentos, pornográficos, racistas, xenófobos, ofensivos, de apología del terrorismo o, en general, contrarios a la ley o al orden público.</li>
                                <li>Introducir en la red virus informáticos o realizar actuaciones susceptibles de alterar, estropear, interrumpir o generar errores o daños en los documentos electrónicos, datos o sistemas físicos y lógicos de Neaticket o de terceras personas; así como obstaculizar el acceso de otros usuarios al sitio web y a sus servicios mediante el consumo masivo de los recursos informáticos a través de los cuales Neaticket presta sus servicios.</li>
                                <li>Intentar acceder a las cuentas de correo electrónico de otros usuarios o a áreas restringidas de los sistemas informáticos de Neaticket o de terceros y, en su caso, extraer información.</li>
                                <li>Vulnerar los derechos de propiedad intelectual o industrial, así como violar la confidencialidad de la información de Neaticket o de terceros.</li>
                                <li>Suplantar la identidad de cualquier otro usuario.</li>
                                <li>Reproducir, copiar, distribuir, poner a disposición o de cualquier otra forma.Aviso Legal de: www.neaticket.com comunicar públicamente, transformar o modificar los contenidos, a menos que se cuente con la autorización del titular de los correspondientes derechos o ello resulte legalmente permitido.</li>
                                <li>Recabar datos con finalidad publicitaria y de remitir publicidad de cualquier clase y comunicaciones con fines de venta u otras de naturaleza comercial sin que medie su previa solicitud o consentimiento.</li>
                            </ol>
                            Todos los contenidos del sitio web, como textos, fotografías, gráficos, imágenes, iconos, tecnología, software, así como su diseño gráfico y códigos fuente, constituyen una obra cuya propiedad pertenece a Neaticket, sin que puedan entenderse cedidos al usuario ninguno de los derechos de explotación sobre los mismos más allá de lo
                            estrictamente necesario para el correcto uso de la web.
                            En definitiva, los usuarios que accedan a este sitio web pueden visualizar los contenidos y efectuar, en su caso, copias privadas autorizadas siempre que los elementos reproducidos no sean cedidos posteriormente a terceros, ni se instalen a servidores conectados a redes, ni sean objeto de ningún tipo de explotación.
                            Asimismo, todas las marcas, nombres comerciales o signos distintivos de cualquier clase que aparecen en el sitio web son propiedad de Neaticket, sin que pueda entenderse que el uso o acceso al mismo atribuya al usuario derecho alguno sobre los mismos.
                            La distribución, modificación, cesión o comunicación pública de los contenidos y cualquier otro acto que no haya sido expresamente autorizado por el titular de los derechos de explotación quedan prohibidos.
                            El establecimiento de un hiperenlace no implica en ningún caso la existencia de relaciones entre Neaticket y el propietario del sitio web en la que se establezca, ni la aceptación y aprobación por parte de Neaticket de sus contenidos o servicios.
                            Neaticket no se responsabiliza del uso que cada usuario le dé a los materiales puestos a
                            disposición en este sitio web ni de las actuaciones que realice en base a los mismos.<br>

                            3.1 Exclusión de Garantias y de responsabilidad en el acceso y la utilización:
                            El contenido del presente sitio web es de carácter general y tiene una finalidad meramente informativa, sin que se garantice plenamente el acceso a todos los contenidos, ni su exhaustividad, corrección, vigencia o actualidad, ni su idoneidad o utilidad para un objetivo específico.
                            Neaticket excluye, hasta donde permite el ordenamiento jurídico, cualquier responsabilidad por los daños y perjuicios de toda naturaleza derivados de:
                            a) La imposibilidad de acceso al sitio web o la falta de veracidad, exactitud, exhaustividad y/o actualidad de los contenidos, así como la existencia de vicios y defectos de toda clase de los contenidos transmitidos, difundidos, almacenados, puestos a disposición a los que se haya accedido a través del sitio web o de los
                            servicios que se ofrecen.
                            b) La presencia de virus o de otros elementos en los contenidos que puedan producir alteraciones en los sistemas informáticos, documentos electrónicos o datos de los usuarios.
                            c) El incumplimiento de las leyes, la buena fe, el orden público, los usos del tráfico y el presente aviso legal como consecuencia del uso incorrecto del sitio web. En particular, y a modo ejemplificativo, Neaticket no se hace responsable de las actuaciones de terceros que vulneren derechos de propiedad intelectual e industrial,
                            secretos empresariales, derechos al honor, a la intimidad personal y familiar y a la propia imagen, así como la normativa en materia de competencia desleal y publicidad ilícita.
                            Asimismo, Neaticket declina cualquier responsabilidad respecto a la información que se halle fuera de esta web y no sea gestionada directamente por nuestro webmaster. La función de los links que aparecen en esta web es exclusivamente la de informar al usuario sobre la existencia de otras fuentes susceptibles de ampliar los contenidos que ofrece este sitio web. Neaticket no garantiza ni se responsabiliza del funcionamiento o
                            accesibilidad de los sitios enlazados; ni sugiere, invita o recomienda la visita a los mismos, por lo que tampoco será responsable del resultado obtenido. Neaticket no se responsabiliza del establecimiento de hipervínculos por parte de terceros.<br>

                            3.2 Procedimiento en caso de realización de actividades de caracter ilícito:
                            En el caso de que cualquier usuario o un tercero considere que existen hechos o circunstancias que revelen el carácter ilícito de la utilización de cualquier contenido y/o de la realización de cualquier actividad en las páginas web incluidas o accesibles a través del sitio web, deberá enviar una notificación a Neaticket identificándose
                            debidamente y especificando las supuestas infracciones.
                            Aviso Legal de: www.neaticket.com
                            En el caso de que cualquier usuario o un tercero considere que existen hechos o
                            circunstancias que revelen el carácter ilícito de la utilización de cualquier contenido
                            y/o de la realización de cualquier actividad en las páginas web incluidas o accesibles a
                            través del sitio web, deberá enviar una notificación a Neaticket identificándose
                            debidamente y especificando las supuestas infracciones.<br>

                            3.3 Publicaciones:
                            La información administrativa facilitada a través del sitio web no sustituye la publicidad legal de las leyes, normativas, planes, disposiciones generales y actos que tengan que ser publicados formalmente a los diarios oficiales de las administraciones públicas, que constituyen el único instrumento que da fe de su autenticidad y
                            contenido. La información disponible en este sitio web debe entenderse como una guía sin propósito de validez legal.
                        </p>
                        <h3>4. Política de privacidad</h3>
                        <p>
                            Cuando precisemos obtener información por su parte, siempre le solicitaremos que nos la proporcione voluntariamente de forma expresa. Los datos recabados a través de los formularios de recogida de datos del sitio web u otras vías serán incorporados a un fichero de datos de carácter personal debidamente inscrito en el Registro General de Protección de Datos de la Agencia Española de Protección de Datos, del cual es responsable Thinkandcloud S.L. (en adelante: Neaticket). Esta entidad tratará los datos de forma confidencial y exclusivamente con la finalidad de ofrecer los servicios solicitados, con todas las garantías legales y de seguridad que impone la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal, el Real Decreto 1720/2007, de 21 de diciembre y la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Información y de Comercio Electrónico.
                            Neaticket no cederá ni compartirá los datos con terceros sin su consentimiento expreso.
                            Asimismo, Neaticket, cancelará o rectificará los datos cuando resulten inexactos, incompletos o hayan dejado de ser necesarios o pertinentes para su finalidad, de conformidad con lo previsto en la Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal.
                            El usuario podrá revocar el consentimiento prestado y ejercer los derechos de acceso, rectificación, cancelación y oposición dirigiéndose a tal efecto en el domicilio siguiente: Thinkandcloud S.L., sito en C/ Juan de Austria, 9, Piso 1 – 46002 Valencia (España) identificándose debidamente e indicando de forma expresa el concreto derecho que se quiere ejercer.
                            Neaticket adopta los niveles de seguridad correspondientes requeridos por la citada Ley Orgánica 15/1999 y demás normativa aplicable. No obstante, no puede garantizar la absoluta invulnerabilidad de los sistemas, por tanto, no asume ninguna responsabilidad por los daños y perjuicios derivados de alteraciones que terceros puedan causar en los sistemas informáticos, documentos electrónicos o ficheros del usuario.
                            Si opta a abandonar nuestro sitio web a través de enlaces a sitios web no pertenecientes a nuestra entidad, Neaticket no se hará responsable de las políticas de privacidad de dichos sitios web ni de las cookies que éstos puedan almacenar en el ordenador del usuario.
                            Nuestra política con respecto al correo electrónico se centra en remitir únicamente comunicaciones que usted haya solicitado recibir. Si prefiere no recibir estos mensajes por correo electrónico le ofreceremos a través de los mismos la posibilidad de ejercer su derecho de cancelación y renuncia a la recepción de estos mensajes, en conformidad con lo dispuesto en el Título III, artículo 22 de la Ley 34/2002, de Servicios para la Sociedad de la Información y de Comercio Electrónico.
                        </p>
                        <h3>5. Legislación aplicable</h3>
                        <p>
                            Las condiciones presentes se regirán por la ley española. Para la resolución de cualquier duda, discrepancia o divergencia que pudiera suscitarse en el cumplimiento e interpretación del presente documento, las partes se someten a los Juzgados y Tribunales de Valencia. La lengua utilizada será el Castellano.
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- MODAL: Frequent questions -->
        <div class="modal fade" id="modal-faq" tabindex="-1" role="dialog" aria-labelledby="modal-faq-label" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                        </button>
                        <h2 class="modal-title" id="modal-faq-label">Preguntas frecuentes</h2>
                    </div>
                    <div class="modal-body">
                        <h3>1. ¿Qué es Neaticket?</h3>
                        <p>
                            Neaticket es sistema de tiques de compra digitales para no perderlos nunca y tenerlos siempre disponibles al instante desde tu móvil. Además Neaticket ofrece unas serie de funcionalidades extra muy útiles (caducidad de devolución y de garantía, ofertas, etc) que te facilitan la vida enormemente.
                        </p>
                        <h3>2. ¿Qué puedo hacer en Neaticket?</h3>
                        <p>
                            Neaticket funciona como un repositorio de tique de compra que te permite acceder a ellos, buscarlos y ordenarlos estés donde estés. Neaticket te avisa cuando la garantía de devolución va a caducar con tiempo suficiente para que lo hagas, de forma que nunca se te pase la fecha de devolución. Neaticket también te avisa cuando la garantía general va a caducar por si el producto tiene una avería y tienes que arreglarlo, dándote información además sobre los datos de contacto del servicio técnico de la emrpesa. Por si fuera poco, Neaticket te informa de las ofertas que tus establecimientos favoritos tienen disponibles en cada momento, para que no pierdas la oportunidad de conseguirlas.
                        </p>
                        <h3>3. ¿Qué NO puedo hacer en Neaticket?</h3>
                        <p>
                            Neaticket se centra en ayudarte a gestionar los tique de compra. Neaticket no es una plataforma de pago. No accedemos a tus cuentas bancarias ni nada parecido. Lo único que hacemos es facilitarte la vida eliminando el infierno que supone la gestión de tique de compra.
                        </p>
                        <h3>4. ¿Cómo puedo utilizar Neaticket?</h3>
                        <p>
                            Muy sencillo, cuando compres en un establecimiento con Neaticket piede tu tique electrónico. La dependienta o dependiente te pedirá tu email y listo. Tras esta primera compra recibirás un email con el tique adjunto en pdf y unas claves de acceso a tu cuenta www.neaticket.com. Entra y encontrarás tus tique. Todas las demás funcionalidades (ordenar, avisos, ofertas, etc.) las tendrás en la app que podrás descargarte de las tiendas de Google y Apple. Ten en cuenta que cuantos más establecimientos tengan Neaticket mejor, pues en una sola app podrás gestionar todos los tique de diferentes tiendas, así que donde no tengan Neaticket … pídelo!
                        </p>
                        <h3>5. ¿Puedo usar Neaticket en un establecimiento no asociado?</h3>
                        <p>
                            Si pides Neaticket en un establecimiento no asociado no podrás encontrar tu tique digital. Pero no desesperes. En el futuro implementaremos una mejora de la app para que puedas hacerle una foto al tique físico de los establecimientos sin Neaticket y tenerlos guardados en la app. Sin embargo, ten en cuenta que las funcionalidades de Neaticket no serán óptimas con estos tique al ser simples imágenes, por lo que no dejes de pedir Neaticket en el establecimiento aunque no lo tengan, pues eso te permitirá obtener mucha más información y gestionar mejor los tique.
                        </p>
                        <h3>6. ¿Los tique digitales de Neaticket tienen validez fiscal?</h3>
                        <p>
                            Por el momento Neaticket es complementario a los sistemas de tique físico de las tiendas, por lo que si necesitas el tique de compra físicamente te recomendamos que lo pidas en la tienda (sin dejar de pedir el tique digital de Neaticket). Recuerda que Neaticket simplemente es una réplica digital del tique físico de la tienda, que te permite acceder a él desde tu móvil, por lo que la tienda siempre tiene copia física de tu tique digital.
                        </p>
                        <h3>7. ¿Cómo puedo acceder a mis tiques?</h3>
                        <p>
                            Todos los tiques que hayas pedido en Neaticket los tendrás accesibles en tu cuenta de Neaticket. Podrás acceder a ella de dos formas:
                            <ol type="1">
                                <li>A través de la web en www.neaticket.com con tus cuentas.</li>
                                <li>A través de la app descargándola de las tiendas de Apple y Google.</li>
                            </ol>
                        </p>
                        <h3>8. ¿Es completamente gratuito?</h3>
                        <p>
                            Sí. No tiene ningún coste para los clientes de la tiendas.
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- MODAL: Frequent questions -->
        <div class="modal fade" id="form-content" tabindex="-1" role="dialog" aria-labelledby="modal-faq-label" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                        </button>
                        <h2 class="modal-title" id="modal-faq-label">Contacto</h2>
                    </div>
                    <div class="modal-body">
                        <h3>Donde estamos</h3>
                        <address>
                        <i class="fa fa-phone"></i>
                        <abbr title="Teléfono"> T</abbr>: 0039 333 12 68 347<br/>
                        <i class="fa fa-map-marker"></i>
                        Calle Don Juan de Austria_9<br/>
                        46002 Valencia _Spain
                        </address>
                        <p><i class="fa fa-envelope"></i> Email: <a href="">info@neaticket.com</a></p>
                        <h3>Escríbenos tu consulta</h3>
                        <!-- <form role="form" name="contact" class="contact form-group"> -->
                        <form role="form" action="lib/contacto.php" method="post" enctype="plain" class="form-group">
                            <label for="nombre"></label>
                            <input class="form-control" id="nombre" type="text" name="nombre" placeholder="Nombre y Apellido" required="" />
                            <label for="email"></label>
                            <input class="form-control" id="email" type="email" name="email" placeholder="ejemplo@correo.com" required="" />
                            <label for="mensaje"></label>
                            <textarea class="form-control" id="mensaje" name="mensaje" placeholder="Escribe tu mensaje" required=""></textarea>
                            <!-- <input class="btn" id="submit" type="submit" name="submit" value="Enviar" /> -->
                            <div class="form-group btn2">
                                <!-- <button class="btn form-control" id="submit" type="submit" name="submit">Enviar</button> -->
                                <input class="btn3" id="submit" type="submit" name="submit" value="Enviar" />
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

<!--         <div class="modal fade" id="form-content2" tabindex="-1" role="dialog" aria-labelledby="modal-faq-label" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                        </button>
                        <h2 class="modal-title" id="modal-faq-label">Contacto</h2>
                    </div>
                    <div class="modal-body">
                        <h3>Donde estamos</h3>
                        <p>
                        Calle Don Juan de Austria_9<br/>
                        46002 Valencia _Spain
                        </p>
                        <h3>Escríbenos tu consulta</h3>
                    <form class="contact" name="contact">
                        <label class="label" for="name">Your Name</label><br>
                        <input type="text" name="name" class="input-xlarge"><br>
                        <label class="label" for="email">Your E-mail</label><br>
                        <input type="email" name="email" class="input-xlarge"><br>
                        <label class="label" for="message">Enter a Message</label><br>
                        <textarea name="message" class="input-xlarge"></textarea>
                    </form>
                </div>
                <div class="modal-footer">
                    <input class="btn btn-success" type="submit" value="Send!" id="submit">
                    <a href="#" class="btn" data-dismiss="modal">Nah.</a>
                </div>
            </div>
        </div>
    </div> -->
