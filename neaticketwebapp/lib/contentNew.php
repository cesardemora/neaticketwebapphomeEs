<div class="top-content">
    <div class="inner-bg">
        <div class="container">
            <div class="">
                <input type="text" id="daterange" value="01/01/2016">
                <!-- <input type="text" name="daterange" value="01/01/2015 - 01/31/2015" /> -->
            </div>
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 text wow fadeInUp">
                    <h1>Welcome to <strong>Neaticket</strong></h1>
                    <div class="description">
                        <p>
                            Todos tus tiques de compra en una app de manera automática.
                            Guarda, ordena y búscalos rápidamente.
                            Recibe notificaciones para cambiar o devolver los productos.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 screenshots-box wow fadeInLeft">
                    <?php include("lib/imgNew1b.php"); ?>
                </div>

                <section id="login" name="login"></section>
                <div class="col-sm-5 form-box wow fadeInUp">
                    <div class="form-top">
                        <div class="form-top-left">
                            <h3>Login de Usuarios</h3>
                            <p>Entra ahora para ver tus tickets:</p>
                        </div>
                        <div class="form-top-right">
                            <span aria-hidden="true" class="typcn typcn-pencil"></span>
                        </div>
                        <!-- <div class="form-top-right">
                            <i class="fa fa-pencil"></i>
                        </div> -->
                    </div>
                    <div class="form-bottom">

                        <!-- <div class="social-buttons">
                        Login via
                            <a href="#" class="btn btn-fb"><i class="fa fa-facebook"></i> Facebook</a>
                            <a href="#" class="btn btn-tw"><i class="fa fa-twitter"></i> Twitter</a>
                        </div> -->
                        <form form role="form" class="registration-form" name="login_user" id="form1" method="post" action="">
                        <?php include("lib/loginNew.php"); ?>
                        </form>
                    </div>
                    <!-- <div class="top-big-link">
                        <a class="btn btn-link-1" href="index2.php">Usuarios</a>
                        <a class="btn btn-link-2" href="index2emp.php">Empresas</a>
                    </div> -->
                </div>
            </div>
                <?php include("lib/footerNew.php"); ?>
            </div>
        </div>
    </div>
</div>