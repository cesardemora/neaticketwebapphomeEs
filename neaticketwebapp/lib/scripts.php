 <!--Scripts en todos los index ================================================== -->

<script src="assetsNew/js/jquery-1.11.3.min.js"></script>
<script src="assetsNew/bootstrap/js/bootstrap.min.js"></script>
<script src="assetsNew/js/smoothscroll.js"></script>
<script src="assetsNew/js/jquery.backstretch.min.js"></script>
<script src="assetsNew/js/retina.min.js"></script>
<script src="assetsNew/js/wow.min.js"></script>
<script src="assetsNew/js/scripts.js"></script>
<script src="assetsNew/js/scripts-screen.js"></script>
<!-- <script src="assetsNew/js/addclear.js"></script> -->

<!-- ??????? ================================================== -->
<!-- <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js" ></script> -->
<!-- <script src="assetsNew/js/classie.js"></script> -->
<!-- <script src="assetsNew/js/cbpAnimatedHeader.js"></script> -->
<!-- <script src="assetsNew/js/jqBootstrapValidation.js"></script> -->
<script src="assetsNew/js/agency.js"></script>
<script src="assetsNew/js/main.js"></script>

<!-- Input "X·" Borrar contenido (addclear.js) ================================================== -->
<script charset="utf-8">
  $(function(){
    $( "input[type!='checkbox']" ).addClear();
  });
</script>

<!-- Bootstrap Carousel ================================================== -->
<script>
$('.carousel').carousel({
  interval: 3500
})
</script>
<script type="text/javascript">
    function TiendaSelectHandler(select){
        if(select.value == 'Todas')
                show('Provincias');
            else 
                hide('Provincias');
    }
    function ProvinciaSelectHandler(select){
        if(select.value == 'Todas')
                show('Tiendas');
            else 
                hide('Tiendas');
    }
    function hide(select){
        document.getElementById(select).style.opacity = '0.5';
        document.getElementById(select).disabled = true;
    }

    function show(select){
        document.getElementById(select).style.opacity = '1';
        document.getElementById(select).disabled = false;
    }
</script>

<!--[if lt IE 10]>
    <script src="assetsNew/js/placeholder.js"></script>
<![endif]-->

<!--[if lt IE 10]>
    <script src="assetsNew/js/placeholder.js"></script>
<![endif]
