<meta charset="utf-8" content="">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="title" content="<?php echo PROJECT_NAME; ?>">
<title><?php echo PROJECT_NAME; ?></title>

<!-- <title>Neaticket: Tus tickets siempre contigo.</title> -->


<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta content="index, follow" name="robots" />
<meta http-equiv="x-ua-compatible" content="chrome=1">
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="description" content="Neaticket: Tus tickets siempre contigo">
<meta name="author" content="Neaticket">
<meta name="keywords" content="ticket, tickets, facturas, compras, organizar, gestion">

<!-- Meta Apple -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

<!-- <meta name="google-signin-client_id" content="82364427804-t6bfg3k6uqjdm10srpja753fmi75scq7.apps.googleusercontent.com"> -->
<meta name="google-signin-client_id" content="<?php echo CLIENT_ID; ?>">

<!-- CSS -->
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
<!-- <link rel="stylesheet" href="assetsNew/css/bootstrap.css"> -->
<!-- <link rel="stylesheet" href="assetsNew/css/bootstrap.min.css"> -->
<link rel="stylesheet" href="assetsNew/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="assetsNew/bootstrap/css/bootstrap-social.css">
<link rel="stylesheet" href="assetsNew/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="assetsNew/font/typicons.min.css">
<link rel="stylesheet" href="assetsNew/css/animate.css">
<link rel="stylesheet" href="assetsNew/css/form-elements.css">
<link rel="stylesheet" href="assetsNew/css/style.css">

<!-- Daterangepicker -->
<link rel="stylesheet" type="text/css" media="all" href="lib/amd/daterangepicker.css">
<link rel="stylesheet" href="assetsNew/datepicker/css/datepicker.css">

<!-- <link rel="stylesheet" href="assetsNew/css/main.css"> -->
<!-- <link rel="stylesheet" href="assetsNew/css/style-modal.css"> -->
<!-- Include the plugin's CSS and JS: -->

<!-- JS -->
<script src="assetsNew/bootstrap/js/bootstrap.js"></script>
<script src="assetsNew/js/jquery-1.11.3.min.js"></script>
<!-- <script src="assetsNew/js/bootstrap.js"></script> -->
<!-- <script src="assetsNew/js/jquery.min.js"></script> -->
<script src="assetsNew/js/smoothscroll.js"></script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Favicon and touch icons -->
<!-- 16x16 -->
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
<!-- 32x32 -->
<link rel="shortcut icon" href="/favicon.png">
<!-- Apple -->
<link rel="apple-touch-icon" sizes="144x144" href="assetsNew/ico/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="114x114" href="assetsNew/ico/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="72x72" href="assetsNew/ico/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="57x57" href="assetsNew/ico/apple-touch-icon-57x57.png">
<!-- Apple2 -->
<link href="assetsNew/ico/apple-touch-icon.png" rel="apple-touch-icon">
<link href="assetsNew/ico/apple-touch-icon-76x76.png" rel="apple-touch-icon" sizes="76x76">
<link href="assetsNew/ico/apple-touch-icon-120x120.png" rel="apple-touch-icon" sizes="120x120">
<link href="assetsNew/ico/apple-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152">
<link href="assetsNew/ico/apple-touch-icon-180x180.png" rel="apple-touch-icon" sizes="180x180">
<!-- <link href="assetsNew/ico/icon-hires.png" rel="icon" sizes="192x192">
<link href="assetsNew/ico/icon-normal.png" rel="icon" sizes="128x128"> -->
<link rel="apple-touch-startup-image" href="assetsNew/ico/startup.png">

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-72014631-1', 'auto');
  ga('send', 'pageview');

</script>
