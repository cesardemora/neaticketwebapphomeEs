<div class="container">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a href="../index.php#page-top" class="page-scroll">
        <!-- <span class="icomoon icon-logo img-responsive margin-logo"></span> -->
        <img class="img-responsive" src="assetsNew/img/logoW.svg" alt="">
      </a>
        <!-- <a class="navbar-brand" href="index.php">Neaticket: Tus tickets siempre contigo.</a> -->
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="top-navbar-1">
        <ul class="nav navbar-nav navbar-right">
            <li>
                <span class="li-text">
                    ¿Eres un usuario?
                </span>
                <a href="index.php"><strong>aquí</strong></a>
                <span class="li-text">
                </span>
                <span class="li-social">
                    <!-- <a href="#"><i class="fa fa-facebook"></i></a> -->
                    <a href="http://bit.ly/1IanTbY" target="blank"><i class="fa fa-twitter"></i></a>
                    <a <a href="mailto:info@neaticket.com"><i class="fa fa-envelope"></i></a>
                    <!-- <a href="#"><i class="fa fa-skype"></i></a> -->
                </span>
            </li>
        </ul>
    </div>
</div>