<?php

$email_cliente = $_POST['email'];
$id = $_POST['id'];
$comercio = $_POST['empresa'];
$_SESSION['Ticket'] = "";

$genPdf = funcPdf($id);

crearMail($email_cliente,$genPdf,$comercio,$_SESSION['Ticket']);


    /////////////////////////////////////////////////////////////////////////////////////////

    function generaPass(){

        $str = "1234567890";
        $cadena = "";
        for($i=0;$i<=4;$i++) {
        $cadena .= substr($str,rand(0,10),1);
        }

        return $cadena;
    }

    function funcPdf($id){

        //Llamada al script mpdf
        require('class/mpdf/mpdf.php');

        require_once ('class/connect.php');
        require_once ('class/barcode.inc.php');

        $clase = new connect;
        $clase->dbConnects();

        $id_u= $id;

        $consulta_id_ticket = mysql_query( "SELECT * FROM TICKETS where id =  '$id_u'" );

        while ( $datos = mysql_fetch_array( $consulta_id_ticket ) ) {
            $id_ticket  =    $datos [ 'id' ];
            $comercio   =    $datos [ 'nombre_tienda' ];
            $num_ticket =    $datos [ 'num_ticket' ];
            $fecha      =    $datos [ 'fecha' ];
            $hora       =    $datos [ 'hora' ];
        }

        $archivo = "prueb/Ticket_".$comercio."_".$fecha."_".$hora.".pdf";
        $archivo_de_salida=$archivo;

        $consultaProductos = mysql_query( "SELECT * FROM DETALLETICKET where id_ticket =  '$id_ticket'" );

        $row_cnt = mysql_num_rows($consultaProductos);

        $long= 180+6*$row_cnt;

        $pdf=new mPDF('utf-8', array(100,$long));

        $pdf->AddPage();  //añadimos una página. Origen coordenadas, esquina superior izquierda, posición por defeto a 1 cm de los bordes.
        //$pdf->Image('assetsNew/img/nt_logo4.png',45,8,8,0);

        $consulta = mysql_query( "SELECT * FROM TICKETS where id =  '$id_u'" );

        while ( $datos = mysql_fetch_array( $consulta ) ) {
            $id =           $datos [ 'id' ];
            $id_user =      $datos [ 'cliente' ];
            $id_ticket =    $datos [ 'num_ticket' ];
            $_SESSION['Ticket'] = $id_ticket;
            $comercio =     $datos [ 'nombre_tienda' ];
            $mail_tienda =     $datos [ 'mail_tienda' ];
            $local =        $datos [ 'local' ];
            $fecha =        $datos [ 'fecha' ];
            $hora =         $datos [ 'hora' ];
            $importe =      $datos [ 'total' ];
            $entregado =    $datos [ 'entregado' ];
            $cambio =       $datos [ 'cambio' ];
            $pago =         $datos [ 'datadi' ];
            $codigo_barras = $datos [ 'datadi2' ];
            $tipo_iva =     $datos [ 'tipo_iva' ];
            $caja =         $datos [ 'caja' ];
            $cajero =       $datos [ 'cajero' ];
            $tarjeta =      $datos [ 'num_tarjeta' ];

            $consultaTienda = mysql_query( "SELECT * FROM TIENDAS where mail =  '$mail_tienda'" );
            while ( $datosTienda = mysql_fetch_array( $consultaTienda ) ) {
        
                $cif_tienda =     $datosTienda [ 'cif_empresa' ];
                $direccion_tienda = $datosTienda [ 'direccion' ];
                $provincia_tienda = $datosTienda [ 'cp' ] .", ".$datosTienda [ 'localidad' ];
                $email_tienda =     $datosTienda [ 'correo_info' ];
                $web_tienda =       $datosTienda [ 'web' ];
                $telefono_tienda =  $datosTienda [ 'telefono' ];
                $dias_devolucion =  $datosTienda [ 'dias_devolucion' ];
                $dev = date("d/m/Y", strtotime($fecha ." +".$dias_devolucion." day"));
            }

            //logo de la tienda
            //$pdf->Image('../empresa.jpg' , 0 ,0, 40 , 40,'JPG', 'http://php-estudios.blogspot.com');

            $comercio = trim($comercio);
            $comercio = htmlspecialchars($comercio);
            $comercio = str_replace(chr(160),'',$comercio);
            //$comercio = str_replace('&','&amp', $comercio);

            $style = array('width' => 0.5, 'cap' => 'square', 'join' => 'bevel', 'dash' => '10,20,5,10', 'phase' => 10, 'color' => array(255, 0, 0));

            // Encabezado de la factura
            $pdf->SetFont('Arial','',14);
            $pdf->ignore_invalid_utf8 = true;
            $pdf->Cell(0, 8, $comercio, 0, 1, "C");
            $pdf->SetDrawColor(188,188,188);

            $pdf->SetFont('Arial','',9);
            $pdf->SetTextColor(188,188,188);
            $pdf->MultiCell(0, //posición X
            5, //posición Y
            $direccion_tienda."\n"
            .$provincia_tienda."\n"
            ."Tel.: ".$telefono_tienda."\n",
            //.$cif_tienda."\n",
             0, // bordes 0 = no | 1 = si
             "C", // texto justificado
             false);

            $pdf->SetTextColor(0,0,0);
            $pdf->SetFont('Arial','',9);

            $pdf->SetXY(10, 41);
            $pdf->Cell(20, 5, "Fecha  : ", 0, 1, 'L');
            $pdf->SetXY(23, 41);
            $pdf->Cell(20, 5, $fecha, 0, 1, 'L');
            $pdf->SetXY(56, 41);
            $pdf->Cell(20, 5, "Hora:", 0, 1, 'L');
            $pdf->SetXY(70, 41);
            $pdf->Cell(20, 5, $hora, 0, 1, 'R');

            $pdf->SetXY(10, 46);
            $pdf->Cell(20, 5, "Caja    : ", 0, 1, 'L');
            $pdf->SetXY(23, 46);
            $pdf->Cell(20, 5, $caja, 0, 1, 'L');
            $pdf->SetXY(56, 46);
            $pdf->Cell(20, 5, "Le atendió: ", 0, 1, 'L');
            $pdf->SetXY(70, 46);
            $pdf->Cell(20, 5, $cajero, 0, 1, 'R');

            $pdf->SetXY(10, 51);
            $pdf->Cell(20, 5, "Ticket  : ", 0, 1, 'L');
            $pdf->SetXY(23, 51);
            $pdf->Cell(20, 5, $id_ticket, 0, 1, 'L');

            $pdf->Line(10,40,90,40,$style);
            $pdf->Line(10,57,90,57,$style);
            $pdf->Line(10,58,90,58,$style);

            //Creación de la tabla de los detalles de los productos productos
            $pdf->SetFont('Arial','B',9);
            $pdf->SetTextColor(0,0,0);
            $top_productos = 59;

            $pdf->SetXY(10, $top_productos);
            $pdf->Cell(10, 5, 'UD', 0, 1, 'C');
            $pdf->SetXY(15, $top_productos);
            $pdf->Cell(20, 5, 'MODELO', 0, 1, 'C');
            $pdf->SetXY(30, $top_productos);
            $pdf->Cell(20, 5, 'DESC.', 0, 1, 'C');
            $pdf->SetXY(65, $top_productos);
            $pdf->Cell(7, 5, 'TALLA', 0, 1, 'C');
            $pdf->SetXY(74, $top_productos);
            $pdf->Cell(20, 5, 'PRECIO', 0, 1, 'C');
            $pdf->Line(10,64,90,64);


            // $precio_subtotal = 0; // variable para almacenar el subtotal
            $y = 66; // variable para la posición top desde la cual se empezarán a agregar los datos
            $x=0;

            $consultaProductos = mysql_query( "SELECT * FROM DETALLETICKET where id_ticket ='$id'" );
            while ( $dato = mysql_fetch_array( $consultaProductos ) )
            {
                $cantidad =     $dato [ 'cantidad' ];
                $modelo =       $dato [ 'cod_articulo' ];
                $descripcion =  $dato [ 'descripcion' ];
                $talla =        $dato [ 'datadi' ];
                $precio =       $dato [ 'precio' ];

                $pdf->SetFont('Arial','',9);
                $pdf->SetXY(13, $y);
                $pdf->Cell(3, 5, $cantidad, 0, 1, 'L');
                $pdf->SetXY(18, $y);
                $pdf->Cell(15, 5, $modelo, 0, 1, 'L');
                $pdf->SetXY(35, $y);
                $pdf->Cell(23, 5, $descripcion, 0, 1, 'L');
                $pdf->SetXY(66, $y);
                $pdf->Cell(5, 5, $talla, 0, 1, 'C');
                $pdf->SetXY(80, $y);
                $pdf->Cell(10, 5, $precio." €", 0, 1, 'R');

                // aumento del top 5 cm
                $y = $y + 5;

            }

            $pdf->Ln(3);
            $pdf->SetFont('Arial','',10);

            $pdf->Cell(0, 5, "I.V.A: ".$tipo_iva."%", 0, 1, "C");
            $pdf->Cell(0, 5, "Subtotal: ".round($cambio,2)." €", 0, 1, "C");
            $pdf->SetFont('Arial','B',11);
            $pdf->Cell(0, 5, "TOTAL: ".$importe." €", 0, 1, "C");
            $pdf->Ln(2);
            $pdf->SetFont('Arial','',9);
            $pdf->Cell(0, 5, "Método de Pago: ".$pago, 0, 1, "C");
            if ($tarjeta != ""){
                $pdf->Cell(0, 5, "Tarjeta: ".$tarjeta, 0, 1, "C");
            }
            // $pdf->Line(15,130,85,130);
            //$pdf->Ln(2);

            new barCodeGenrator($codigo_barras,1,'barcode.gif', 155, 75, false);

            $pdf->Image('barcode.gif',23,$y + 30,69,10);

            //$pdf->WriteBarcode($codigo_barras);

            $pdf->Ln(3);
            $pdf->SetFont('Arial','',9);
            $pdf->SetTextColor(188,188,188);
            $pdf->Cell(0, 5, "Fecha límite de devolución: ".$dev, 0, 1, "C");
            $pdf->Cell(0, 5, "Este ticket es IMPRESCINDIBLE", 0, 1, "C");
            $pdf->Cell(0, 5, "para cualquier cambio o reclamación,", 0, 1, "C");
            $pdf->Cell(0, 5, "en un plazo de ".$dias_devolucion." dias, salvo tara o defecto.", 0, 1, "C");        

            //Recupero información Fiscal de la Empresa
            $consulta_empresa = mysql_query( "SELECT * FROM EMPRESAS where cif =  '$cif_tienda'" );

            $y += 64;
            while ( $datosEmpresa = mysql_fetch_array( $consulta_empresa ) ) {
                $datos_fiscales   =    $datosEmpresa [ 'datos_fiscales' ];
                $nombre_fiscal    =    $datosEmpresa [ 'nombre_fiscal' ];
                $direccion_fiscal =    $datosEmpresa [ 'direccion_fiscal' ];
                $cp_fiscal        =    $datosEmpresa [ 'cp_fiscal' ];
                $localidad_fiscal =    $datosEmpresa [ 'localidad_fiscal' ];

                if ($datos_fiscales === "1"){

                    $pdf->Line(10,$y,90,$y);
                    $pdf->Ln(3);
                    $pdf->SetFont('Arial','',9);
                    $pdf->SetTextColor(188,188,188);
                    //$pdf->SetTextColor(0,0,0);
                    $pdf->Cell(0, 5, "".$nombre_fiscal, 0, 1, "C");
                    $pdf->Cell(0, 5, "".$direccion_fiscal, 0, 1, "C");
                    $pdf->Cell(0, 5, "".$cp_fiscal."  ".$localidad_fiscal, 0, 1, "C");
                    $pdf->Cell(0, 5, "".$cif_tienda, 0, 1, "C");

                }
            }

            $pdf->WriteHTML('<div style="font-size:10px;font-family:Arial;margin-top:10px;margin-left:92px;">
             <a href="http://'.$web_tienda.'">'.$web_tienda.'</a></div>');
        }

            $pdf->Output($archivo_de_salida, 'F');//cierra el objeto pdf

            // //Eliminación del archivo en el servidor
            // unlink($archivo);

            // mysqli_close($conexion);
            return $archivo_de_salida;

    }

    /////////////////////////////////////////////////////////////////////////////////////////

    function crearMail($email_user, $archivo_de_salida, $comercio, $id){

        require_once ("lib/class.phpmailer.php");
        require_once('lib/class.smtp.php');

        $mail = new PHPMailer(true);

        // $cadena = generaPass();

        //$dominio    = "thinkandcloud.com"; // sin http://www
        $name       = "NEATICKET"; // También puede ser un campo del formulario como asunto o mensaje.
        $password   = "147258369";
        $SPLangDir  = "phpmailer/language/";
        $subject    = "Nuevo Ticket $id de $comercio";

        $Mensage = "<html><head>"
                . "<title>Neaticket</title>"
                . "</head>"
                . "<body>"

                . "<div style=\"color:#000000; text-align: center;\">"
                . "<img src=\"http://www.neaticket.com/assetsNew/img/logo_neatNew2.png\" /><hr>"
                . "<h2>Gracias por utilizar <b>Neaticket!</b></h2></div>"

                . "<div style=\"color:#000000;\">"
                . "Si recibes este correo es porque recientemente has aceptado un ticket electrónico en el comercio <b>".$comercio."</b> asociado a <b>Neaticket:</b><br><br>"
                . "</div>"

//                . "<div style=\"color:#000000;\">"
//                . "Comercio: <b>".$cadena."</b><br>"
//                . "Telefono:"+telefono+""
//                . "</div><br>"

//                . "<div style=\"color:#000000;\">"
//                . "Ya puedes entrar en www.neaticket.com y gestionar tus tickets con las siguentes claves:"
//                . "</div>"
//
//                . "<div style=\"color:#000000;\">"
//                . "<h3>Usuario: ".$email_user."<br>"
//                . "Contraseña: <font color=#3366BB></font></h3>"
//                . "</div>"

                . "<div style=\"color:#000000;\">Ten en cuenta que a partir de ahora encontrarás tus tickets electrónicos en www.neaticket.com<br>"
                . "Y recuerda que tienes disponible nuestra <b>app gratuita que te ayuda a gestionar las fechas de devolución</b> y garantía, "
                . "además de informarte de las ofertas de tus establecimientos favoritos.</div>"

                . "<div style=\"color:#000000; text-align: center;\">"
                . "<h3>Descarga <b>Neaticket </b>en:</h3>"
                . "<a href=\"https://bit.ly/1X8u6Br\" width=\"50%\" height=\"50%\" />"
                . "<img src=\"http://quikcu.com/wp-content/themes/responsivo/img/app_google.png\" width=\"140\" /></a>"

                // . "<a href=\"https://bit.ly/1X8u6Br\" width=\"50%\" height=\"50%\" />"
                . "<img src=\"http://quikcu.com/wp-content/themes/responsivo/img/app_ios.png\" width=\"140\" /></a>"
                . "</div><br>"

                . "<div style=\"color:#000000;\">"
                . "Gracias por comprar en <b>".$comercio.".</b><br>"
                . "<font color=gray>Te adjuntamos el ticket en Pdf.</font><br><br>"
                . "</div>"

                . "<div style=\"color:#000000;\">"
                . "<i>El Equipo Neaticket.</i><hr>"
                . "<div>"

                . "<div style=\"color:#696969; font-size:11px; text-align: center;\">"
                . "<em>Copyright © 2016 www.neaticket.com - All rights reserved</em>"
                . "</div>"
                . "</body></html>";

        $mail->IsSMTP();
        // $mail->IsSendMail();
        $mail->SetLanguage("es", $SPLangDir); // Set mailer to use SMTP
        $mail->Host = 'smtp.1and1.es'; // Specify main and backup SMTP servers
        $mail->SMTPAuth = true; // Enable SMTP authentication
        $mail->Username = "info@neaticket.com"; // SMTP username
        $mail->Password = "147258369"; // SMTP password
        $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;

        $mail->SetFrom('info@neaticket.com', 'Neaticket');

        $mail->From = "info@neaticket.com";
        $mail->FromName = utf8_decode($name);
        $mail->AddAddress("$email_user");        // Esta es la dirección a donde enviamos.

        $mail->SMTPDebug = 2;
        $mail->MailerDebug = false; // XXXXXXXXXXXXXXXXXXXXXX
        // $mail->hSubject = $subject; // Este es el titulo del email. Vamos el asunto.
        $mail->CharSet = "utf-8";
        $mail->IsHTML(true);
        $mail->Priority = 1;
        //$mail->MsgHTML("CONTENIDO EMAIL2");
        //$mail->MsgHTML($Mensage);

        //$mail->AddAddress($user."@".$dominio); // Esta es la dirección a donde enviamos.
        //$mail->AddReplyTo($user."@".$dominio, $name); // A que dirección se puede responder el correo.
        //$mail->AddCC ($user2."@".$dominio);

        $mail->Subject = $subject; // Este es el titulo del email. Vamos el asunto.
        $mail->Body = $Mensage; // Mensaje a enviar. Yo aquí uso la plantilla que te comentaba.
        // $Body = file_get_contents('contents.html');
        // $mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
        // $mail->msgHTML($Body);
        $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically

        // Si tenemos que enviar archivos adjuntos.
        $mail->AddAttachment("$archivo_de_salida");
        // $mail->AddAttachment("prueb/ticket_id4.pdf");


         if($mail->Send()){ // Envía el correo.
            $result .= "<b>El correo fue enviado correctamente.</b>";
            echo $result;
            //Eliminación del archivo en el servidor
            unlink($archivo_de_salida);
        }else{
            error_log($mail->ErrorInfo, 0);
            $result .= "<b class='red'>Hubo un inconveniente.</b><br />".$mail->ErrorInfo."Por favor,
            inténtalo más tarde.<br /><br />Recuerde que puede ponerse en contacto con nosotros por teléfono
            o directamente por correo electrónico.<br />Perdonen por las molestias causadas.";
            echo $result;
        }


    }


 ?>