<?php

include('class/connect.php');
$clase = new connect;
$clase->dbConnects();

?>

<!DOCTYPE html>
<html lang="en">

    <head>

        <?php include("lib/headNew.php"); ?>

    </head>

    <body>

        <!-- Top menu -->
        <nav class="navbar navbar-inverse navbar-no-bg" role="navigation">

            <?php include("lib/navNewEmp.php"); ?>

        </nav>

        <!-- Loader -->
        <div class="loader">
            <div class="loader-img"></div>
        </div>

                <!-- Top content -->
        <div class="top-content">

            <?php include("lib/contentNewEmp.php"); ?>

        </div>

        <!-- MODAL: Privacy policy -->
        <?php include("lib/modalNew.php"); ?>

        <!-- Javascript  scripts.php -->
        <?php include("lib/scripts.php"); ?>

    </body>

</html>