<?php


?>

<div class="container">
	<div class="row">
		<div  id="capa" class="container wow fadeInUp">
			<div class="jumbotron">
			    <h1>Listado de <strong>Usuarios</strong></h1>

					<!-- <h3>Listado de Tickets</h3> -->
					<?php

					$mail_tienda = $_SESSION['email'];

					$sql = "SELECT * FROM TICKETS WHERE mail_tienda='$mail_tienda'";

					if(!$resultado = $con->query($sql)){
                                            die('Ocurrio un error ejecutando el query [' . $con->error . ']');
					}

					$con->close();

					?>


				<div class="container2">
					    <!-- <h2>Lista de usuarios</h2> -->

					  <!-- <div id="content"> -->
					<!-- <div class="buscar">
					    <form method="POST" class="form-inline" action="index_buscar.php">
					      <div class="form-group">
					        <input class='form-control' type="text" name="id" size="20" placeholder='escribe el id'>
					      </div>
					        <button class="btn btn-default" type="submit" name="B1">Buscar</button>
					    </form>
					</div> -->

					  <!-- <div id="content"> -->
					<div class="buscar">
					    <form method="POST" class="form-inline" action="index_buscarNew.php">
					      <div class="form-group">
					        <input class='form-control' type="text" name="email" size="20" placeholder='escribe el email'>
					      </div>

					      <div class="form-group">
							<button class="btn" type="submit" name="B2">Buscar</button>
					      </div>

					    </form>
					</div>

					<div class="table-responsive">
					    <table class="table" id="tabla">
					      <thead>
					        <tr>
					          <th>Editar</th>
					          <th>Id_Emp</th>
					          <th>Nombre</th>
					          <th>Email</th>
					          <th>Password</th>
					          <!-- <th>Comercio</th> -->
					          <th>Eliminar</th>
					        </tr>
					      </thead>

						<tbody>
							<?php
							  while($fila = $resultado->fetch_assoc()){
							      echo '
							        <tr id="fila_'.$fila['id'].'"> '
							?>

							        <td><a href="editUserEmp.php?id=<?php echo $fila['id']; ?>"><?php echo $fila['id']; ?></a></td>
							<?php
							      echo '
							          <td>'.$fila['id_emp'].'</td>
							          <td>'.$fila['nick'].'</td>
							          <td>'.$fila['email'].'</td>
							          <td>'.$fila['pass'].'</td>
							          <td><a onclick="eliminar('.$fila['id'].')">Eliminar</a></td>
							        </tr>';
							     }
							 ?>
						</tbody>
					</table>
				</div>

					<!--
					 Creamos el formulario que vamos a enviar para crear una persona
					 Este formulario lo vamos a enviar por AJAX con jQuery y por eso le colocamos un id
					 La dirección a donde se enviara el formulario va en el código de jQuery y no en el formulario
					-->

					<!--   <div class="jumbotron">
					<h2>Añadir usuarios</h2>
					</div>

					<form id="formulario" action="" class="form-group" >
					    <label></label><br>
					    <input type='text' class='form-control' placeholder='nick' name="nick" >

					    <label></label><br>
					    <input type='email' class='form-control' placeholder='email' name="email">

					    <label></label><br>
					    <input type='text' class='form-control' placeholder='password' name="pass">

					    <label></label><br>
					    <input type='text' class='form-control' placeholder='id_emp' name="id_emp">

					    <label></label><br>
					    <button type="submit" value="Submit" class="btn btn-success">Añadir</button>
					    <a href="index_emp.php" type="button" class="btn btn-default btn-primary">
					    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Volver</a>
					</form> -->


					<h1>Añadir <strong>Usuarios</strong></h1>

					<form id="formulario" action="" class="form-inline">
						<div class="form-group">
						  <input type='text' class='form-control' placeholder='nombre' name="nick">
						</div>

						<div class="form-group">
						  <input type='email' class='form-control' placeholder='email' name="email">
						</div>

						<div class="form-group">
						  <input type='text' class='form-control' placeholder='password' name="pass">
						</div>

						<div class="form-group">
						  <input type='text' class='form-control' placeholder='id_emp' name="id_emp">
						</div>

						<div class="form-group">
							<button type="submit" class="btn">Añadir</button>
						</div>

					</form>

					<div class="form-group"><br>
						<a href="index2emp.php#login"><button name="boton2" id="boton2" type="button" class="btn">
						<span class="typcn typcn-arrow-left"></span> Volver</button></a>
					</div>


				</div>

					<!-- link para importar jQuery a nuestro proyecto -->
					<script src="https://code.jquery.com/jquery.js"></script>

					<!-- link con el archivo JavaScript de bootstrap -->
					<script src="assetsNew/js/bootstrap.min.js"></script>


					<script type="text/javascript">

					        // Esta primera función agrega un disparador de  evento a la acción submit,
					        // esto quiere decir que cuando se presione el botón de submit del formulario
					        // con id 'formulario' entonces se ejecutara el código contenido en el.
					        $( "#formulario" ).submit(function( event ) {
					            event.preventDefault();

					            // Como estamos enviamos un formulario, la acción a realizar por AJAX debe ser post.
					            // Vamos a ver los parámetros que necesita el método $.post de jQuery para funcionar
					            // 1. Dirección a donde se va a enviar el formulario por medio de POST
					            // 2. Datos del formulario, la función serializa() convierte el formulario en una cadena de texto
					            // 3. Función que se ejecutara cuando se reciba la respuesta del servidor.

					            $.post('agregar.php',
					            $('#formulario').serialize(),
					            function(data) {
					               location.reload();
					                // Si todo salió bien en el servidor se devolvera éxito => true y se ejecutara el else
					                // pero si algo salió mal entonces se mostrar una alerta con el mensaje error
					                if (data.exito != true){
					                    alert('Error');
					                }else{

					                    // Si la persona se creo bien en la base de datos entonces insertamos una fila
					                    // con su información al final de la tabla
					                    $('#tabla tr:last').after(
					                     '<tr id="fila_'+data.id+'">'+
					                          '<td>'+data.id+'</td>'+
					                          '<td>'+data.nick+'</td>'+
					                          '<td>'+data.email+'</td>'+
					                          '<td>'+data.pass+'</td>'+
					                          '<td>'+data.id_emp+'</td>'+
					                          '<td><a onclick="eliminar('+data.id+')">Eliminar</a></td>'+
					                        '</tr>'
					                     );

					                     location.reload();
					                }
					            });

					        });

					        //Esta es la función que se llama al eliminar una persona de la tabla
					        function eliminar(id){

					            // con el método $.get hacemos una petición GET mediante AJAX con jQuery
					            // 1. El primer parámetro es la dirección a donde se va a hacer la petición y los parámetros de la misma
					            // en este caso el parámetro será el id de la persona que se va a eliminar.
					            // 2. El segundo parámetro es la función que se va a ejecutar cuando se reciba la respuesta del servidor.

					            $.get('eliminar.php?id='+id,
					            function(data){
					                if (data.exito != true){
					                  alert('Error');
					                }else{
					                    // si la respuesta fue exitosa entonces eliminamos la fila de la tabla
					                    $('#fila_'+id).remove();
					                }
					            });
					        }

					</script>

					</table>
				</div>

				<!-- <div>
					<a href="index2emp.php"><button name="boton2" id="boton2" type="button" class="btn">
					<span class="typcn typcn-arrow-left"></span> Volver</button></a>
				</div> -->
			</div>
		</div>
	</div>
</div>
