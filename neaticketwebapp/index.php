<?php
include('class/connectA.php');
$clase = new connect;
$clase->dbConnects();
?>

<!DOCTYPE html>
<html lang="es">
<head>
	<?php include("lib/headNew.php"); ?>
</head>
<body>
	<!-- Top menu -->
	<?php include("lib/navNew.php"); ?>
	<!-- Top content -->
	<?php include("lib/contentNew.php"); ?>
	<!-- MODAL: Privacy policy -->
	<?php include("lib/modalNew.php"); ?>
	<!-- Javascript  scripts.php -->
	<?php include("lib/scripts.php"); ?>
</body>
</html>