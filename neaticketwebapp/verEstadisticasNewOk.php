<?php include('lib/scriptsEstadisticas.php'); ?>

<div class="container">
	<div class="row">
		<div  id="capa" class="container wow fadeInUp">
			<div class="jumbotron">
			    <h1><strong>Estadísticas </strong><?php echo $literalDatos; ?></h1>

				<div class="container2">
                                    <!-- ///// BUSQUEDAS //////////////////////////////////////////////////////////////////////////////////////// -->
                                    <div class="form-group">
                                        <form method="POST" class="form" action="">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                        <label class="control-label">Búsqueda por rango de días:</label>
                                                        <div class="input-group input2">
                                                              <!-- <input type="date" class="form-control" name="dia" placeholder="Dia" required=""> -->
                                                              <!-- <input placeholder="Búsqueda por Día..." class="form-control" type="text" name="dia" onfocus="this.type='date';this.setAttribute('onfocus','');this.blur();this.focus();"> -->
                                                            <span class="input-group-btn">
                                                            <input type="text" id="daterange" name="daterange" value='<?php echo $valorDate;?>'>
                                                            <!-- <button class="btn" type="submit" name="BuscarDia">Ver</button> -->
                                                            </span>
                                                        </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <label class="control-label">Búsqueda por Tiendas:</label>
                                                    <div class="input-group">
                                                        <!--<select name="Tiendas" class="form-group" onchange= "this.form.submit()" >-->
                                                        <select name="Tiendas" id="Tiendas" class="form-group" onchange= "TiendaSelectHandler(this)" >
                                                            <?php
                                                                while ($filaT=  mysql_fetch_assoc($resultadoTienda)){

                                                                    if ($primera == true){
                                                                        if ($tienda == "Todas")
                                                                            $seleccion = "selected";
                                                                        else
                                                                            $seleccion = "";

                                                                        echo "<option value='Todas' ".$seleccion.">Todas</option>";

                                                                        $primera = false;
                                                                    }

                                                                    if ($filaT['mail']==$tienda){
                                                                        $seleccion = "selected";
                                                                        $_SESSION['id_tienda_filtro']= $filaT['id_tienda'];
                                                                    }else
                                                                        $seleccion = "";

                                                                    echo "<option value='".$filaT['mail']."' ".$seleccion.">".$filaT['id_tienda']."</option>";
                                                                }
                                                             ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <label id="label-provincias" class="control-label">Búsqueda por Provincia:</label>
                                                    <div class="input-group">
                                                        <select name="Provincias" id="Provincias" class="form-group" onchange = "ProvinciaSelectHandler(this)" >
                                                            <?php
                                                                while ($filaP=  mysql_fetch_assoc($resultadoProvincia)){

                                                                    if ($primeraP == true){
                                                                        if ($provincia == "Todas")
                                                                            $seleccion = "selected";
                                                                        else
                                                                            $seleccion = "";

                                                                        echo "<option value='Todas' ".$seleccion.">Todas</option>";

                                                                        $primeraP = false;
                                                                    }

                                                                    if ($filaP['provincia']==$provincia)
                                                                        $seleccion = "selected";
                                                                    else
                                                                        $seleccion = "";

                                                                    echo "<option value='".$filaP['provincia']."' ".$seleccion.">".$filaP['provincia']."</option>";
                                                                }
                                                             ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <label class="control-label">Buscar:</label>
                                                    <div class="input-group">
                                                        <span class="input-group-btn">
                                                            <!-- <button class="btn btn4" type="submit" name="Buscar">Buscar</button> -->
                                                            <button class="btn btn4" type="submit" name="Buscar">
                                                            <i class="fa fa-search" aria-hidden="true"></i> Buscar</i></button>
                                                            <a href="index_estadisticas.php"><button class="btn right" name="" id="" type="button">
                                                            <i class="fa fa-refresh" aria-hidden="true"></i> Reiniciar</button></a>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="row">-->


                                                <!-- <div class="col-lg-4">
                                                    <label class="control-label">Limpiar:</label>
                                                    <div class="input-group">
                                                        <span class="input-group-btn">
                                                        <button type="reset" name="Limpiar" class="btn4">Limpiar</button>
                                                        </span>
                                                    </div>
                                                </div> -->
                                            <!-- </div> -->
                                        </form>
                                        <!-- </form> -->
                                    </div>

                                    <!-- ///// End //////////////////////////////////////////////////////////////////////////////////////// -->

                                    <div class="row">
                                        <div class="col-lg-3 col-md-6">
                                            <div class="panel panel-blue">
                                                <div class="panel-heading">
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <i class="fa fa-female fa-3x"></i>
                                                        </div>
                                                        <div class="col-xs-9 text-right">
                                                            <div class="huge"><?php echo obtenerMujeres($nombre_marca,$filtros,$filtroTienda,$filtroProv); ?>%</div>
                                                            <div>Clientes Mujeres</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer colorblue">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn5 dropdown-toggle" data-toggle="dropdown">
                                                            Ver Detalles <span class="caret"></span>
                                                        </button>

                                                        <ul class="dropdown-menu colorblue">
                                                            <li><div class="container3">Mujeres <span class="labelVerDetalles"><?php echo obtenerMujeres($nombre_marca,$filtros,$filtroTienda,$filtroProv); ?>%</span></div></li>
                                                            <li role="separator" class="divider"></li>
                                                            <li><div class="container3">Hombres <span class="labelVerDetalles"><?php echo obtenerHombres($nombre_marca,$filtros,$filtroTienda,$filtroProv); ?>%</span></div></li>
                                                        </ul>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-6">
                                            <div class="panel panel-blue2">
                                                <div class="panel-heading">
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <i class="fa fa-male fa-3x"></i>
                                                        </div>
                                                        <div class="col-xs-9 text-right">
                                                            <div class="huge"><?php echo obtenerHombres($nombre_marca,$filtros,$filtroTienda,$filtroProv); ?>%</div>
                                                            <div>Clientes Hombres</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer colorblue2">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn5 dropdown-toggle" data-toggle="dropdown">
                                                            Ver Detalles <span class="caret"></span>
                                                        </button>

                                                        <ul class="dropdown-menu colorblue2">
                                                            <li><div class="container3">Mujeres <span class="labelVerDetalles"><?php echo obtenerMujeres($nombre_marca,$filtros,$filtroTienda,$filtroProv); ?>%</span></div></li>
                                                            <li role="separator" class="divider"></li>
                                                            <li><div class="container3">Hombres <span class="labelVerDetalles"><?php echo obtenerHombres($nombre_marca,$filtros,$filtroTienda,$filtroProv); ?>%</span></div></li>
                                                        </ul>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-6">
                                            <div class="panel panel-yellow">
                                                <div class="panel-heading">
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <i class="fa fa-money fa-3x"></i>
                                                        </div>
                                                        <div class="col-xs-9 text-right">
                                                            <div class="huge"><?php echo obtenerGastoMedio($nombre_marca,$filtros,$filtroTienda,$filtroProv); ?>€</div>
                                                            <div>Gasto medio ticket</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer colororange">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn5 dropdown-toggle" data-toggle="dropdown">
                                                            Ver Detalles <span class="caret"></span>
                                                        </button>

                                                        <ul class="dropdown-menu colororange">
                                                            <li><div class="container3">Mujeres <span class="labelVerDetalles"><?php echo obtenerMujeres($nombre_marca,$filtros,$filtroTienda,$filtroProv); ?>%</span></div></li>
                                                            <li role="separator" class="divider"></li>
                                                            <li><div class="container3">Hombres <span class="labelVerDetalles"><?php echo obtenerHombres($nombre_marca,$filtros,$filtroTienda,$filtroProv); ?>; ?>%</span></div></li>
                                                        </ul>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-6">
                                            <div class="panel panel-yellow2">
                                                <div class="panel-heading">
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <i class="fa fa-shopping-cart fa-3x"></i>
                                                        </div>
                                                        <div class="col-xs-9 text-right">
                                                            <div class="huge"><?php echo obtenerComprasCliente($nombre_marca,$filtros,$filtroTienda,$filtroProv); ?></div>
                                                            <div>Compras por cliente</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer coloryellow">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn5 dropdown-toggle" data-toggle="dropdown">
                                                            Ver Detalles <span class="caret"></span>
                                                        </button>

                                                        <ul class="dropdown-menu coloryellow">
                                                            <li><div class="container3">Mujeres <span class="labelVerDetalles"><?php echo obtenerMujeres($nombre_marca,$filtros,$filtroTienda,$filtroProv); ?>%</span></div></li>
                                                            <li role="separator" class="divider"></li>
                                                            <li><div class="container3">Hombres <span class="labelVerDetalles"><?php echo obtenerHombres($nombre_marca,$filtros,$filtroTienda,$filtroProv); ?>%</span></div></li>
                                                        </ul>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-6">
                                            <div class="panel panel-green">
                                                <div class="panel-heading">
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <i class="fa fa-user-plus fa-3x"></i>
                                                        </div>
                                                        <div class="col-xs-9 text-right">
                                                            <div class="huge"><?php $aux= obtenerEdad($nombre_marca,$filtros,$filtroTienda,$filtroProv); echo $aux[0]; ?>%</div>
                                                            <div><?php $aux= obtenerEdad($nombre_marca,$filtros,$filtroTienda,$filtroProv); echo 'Edad '.$aux[1]; ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer colorgreen">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn5 dropdown-toggle" data-toggle="dropdown">
                                                            Ver Detalles <span class="caret"></span>
                                                        </button>

                                                        <ul class="dropdown-menu colorgreen">
                                                            <li><div class="container3">Mujeres <span class="labelVerDetalles"><?php echo obtenerMujeres($nombre_marca,$filtros,$filtroTienda,$filtroProv); ?>%</span></div></li>
                                                            <li role="separator" class="divider"></li>
                                                            <li><div class="container3">Hombres <span class="labelVerDetalles"><?php echo obtenerHombres($nombre_marca,$filtros,$filtroTienda,$filtroProv); ?>%</span></div></li>
                                                        </ul>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-6">
                                            <div class="panel panel-green2">
                                                <div class="panel-heading">
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <i class="fa fa-credit-card fa-3x"></i>
                                                        </div>
                                                        <div class="col-xs-9 text-right">
                                                            <div class="huge"><?php echo obtenerPagoTarjeta($nombre_marca,$filtros,$filtroTienda,$filtroProv); ?>%</div>
                                                            <div>Pago con tarjeta</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer colorgreen2">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn5 dropdown-toggle" data-toggle="dropdown">
                                                            Ver Detalles <span class="caret"></span>
                                                        </button>

                                                        <ul class="dropdown-menu colorgreen2">
                                                            <li><div class="container3">Mujeres <span class="labelVerDetalles"><?php echo obtenerMujeres($nombre_marca,$filtros,$filtroTienda,$filtroProv); ?>%</span></div></li>
                                                            <li role="separator" class="divider"></li>
                                                            <li><div class="container3">Hombres <span class="labelVerDetalles"><?php echo obtenerHombres($nombre_marca,$filtros,$filtroTienda,$filtroProv); ?>%</span></div></li>
                                                        </ul>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-6">
                                            <div class="panel panel-red">
                                                <div class="panel-heading">
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <i class="fa fa-clock-o fa-3x"></i>
                                                        </div>
                                                        <div class="col-xs-9 text-right">
                                                            <div class="huge"><?php echo obtenerHoraMax($nombre_marca,$filtros,$filtroTienda,$filtroProv); ?>-<?php echo obtenerHoraMax($nombre_marca,$filtros,$filtroTienda,$filtroProv) +1; ?>h</div>
                                                            <div>Hora máx. ventas</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer colorred">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn5 dropdown-toggle" data-toggle="dropdown">
                                                            Ver Detalles <span class="caret"></span>
                                                        </button>

                                                        <ul class="dropdown-menu colorred">
                                                            <li><div class="container3">Mujeres <span class="labelVerDetalles"><?php echo obtenerMujeres($nombre_marca,$filtros,$filtroTienda,$filtroProv); ?>%</span></div></li>
                                                            <li role="separator" class="divider"></li>
                                                            <li><div class="container3">Hombres <span class="labelVerDetalles"><?php echo obtenerHombres($nombre_marca,$filtros,$filtroTienda,$filtroProv); ?>%</span></div></li>

                                                        </ul>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-6">
                                            <div class="panel panel-red2">
                                                <div class="panel-heading">
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <i class="fa fa-calendar fa-3x"></i>
                                                        </div>
                                                        <div class="col-xs-9 text-right">
                                                            <div class="huge"><?php echo obtenerDiaSemana($nombre_marca,$filtros,$filtroTienda,$filtroProv); ?></div>
                                                            <div>Día de más ventas</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer colorred2">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn5 dropdown-toggle" data-toggle="dropdown">
                                                            Ver Detalles <span class="caret"></span>
                                                        </button>

                                                        <ul class="dropdown-menu colorred2">
                                                            <li><div class="container3"><h4>Dias de la Semana</h4></div></li>
                                                            <li role="separator" class="divider"></li>
                                                            <li><div class="container3">Sábado <span class="labelVerDetalles"><?php echo obtenerMujeres($nombre_marca,$filtros,$filtroTienda,$filtroProv); ?></span> Tickets</div></li>
                                                            <li role="separator" class="divider"></li>
                                                            <li><div class="container3">Viernes <span class="labelVerDetalles"><?php echo obtenerHombres($nombre_marca,$filtros,$filtroTienda,$filtroProv); ?></span> Tickets</div></li>
                                                        </ul>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ///// End //////////////////////////////////////////////////////////////////////////////////////// -->

                                    <!--TABLA Estadísticas-->

                                    <div class="table-responsive">
                                        <table class="table" id="tabla">
                                            <thead>
                                                <tr>
                                                  <th></th>
                                                  <th>Tienda</th>
                                                  <th>Neatickets</th>
                                                  <th>Clientes Nuevos</th>
                                                  <th>Porcentaje</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <?php
                                                    $totalLeidos = 0;
                                                    $totalGenerados = 0;
                                                    $totalClientes = 0;
                                                    $numTiendas = 0;
                                                    $sumaProcentajes = 0;
                                                    if(isset($_SESSION['id_tienda_filtro']) && $_SESSION['id_tienda_filtro'] != ''){
                                                        $filtrosEstTienda = " and (S.tienda = '".$_SESSION['id_tienda_filtro']."'";
                                                        $sinEspacio = str_replace (" ", "", $_SESSION['id_tienda_filtro']);
                                                        $filtrosEstTienda .= " OR S.tienda = '$sinEspacio') ";
                                                    }
                                                    // echo $filtrosEstTienda;echo $filtrosEstProv;
                                                    $result = mysql_query("SELECT S.fecha_sesion, S.tienda,
                                                                            SUM(tickets_generados), SUM(clientes_nuevos),
                                                                            ROUND(SUM(tickets_generados)/SUM(tickets_leidos) *100, 0) AS 'Porcentaje'
                                                                            FROM SESIONES_DESKTOP AS S WHERE
                                                                            S.tienda like '".$nombre_marca."%' AND
                                                                            S.tienda NOT IN ('PRUEBAS','ULANKA (PRUEBAS)')
                                                                            ".$filtrosFecha."
                                                                            ".$filtrosEstTienda."
                                                                            ".$filtrosEstProv."
                                                                            GROUP BY S.tienda
                                                                            ORDER BY Porcentaje DESC");
                                                    while($row = mysql_fetch_row ( $result )) {

                                                        $numTiendas +=1;
                                                        echo "<tr><td>".$numTiendas."</td><td>".strtoupper($row[1])."</td><td>".$row[2]."</td><td>".$row[3]."</td><td>".$row[4]."%</td><tr>";
                                                        $totalGenerados = $totalGenerados + $row[2];
                                                        $totalClientes = $totalClientes + $row[3];
                                                        $sumaProcentajes = $sumaProcentajes + $row[4];

                                                    }

                                                    echo "<tr><td></td><td><b><big>TOTAL</big></b></td><td><b><big>".$totalGenerados."</big></b></td><td><b><big>".$totalClientes."</big></b></td><td></td><td></td><td></td><tr>";

                                                    if ($numTiendas > 0){
                                                        $avgGenerados = round($totalGenerados / $numTiendas , 1);
                                                        $avgNuevos = round($totalClientes / $numTiendas, 1);
                                                        $avgPorcentaje = round($sumaProcentajes / $numTiendas, 0);
                                                    }

                                                    echo "<tr><td></td><td><b><big>MEDIA</big></b></td><td><b><big>".$avgGenerados."</big></b></td><td><b><big>".$avgNuevos."</big></b></td><td><b><big>".$avgPorcentaje."%</big></b></td><td></td><td></td><tr>";

                                                 ?>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="form-group"><br>
                                        <a href="index2emp.php#login"><button name="boton2" id="boton2" type="button" class="btn">
                                        <span class="typcn typcn-arrow-left"></span> Volver</button></a>
                                    </div>

                                    </div>

			</div>
		</div>
	</div>
</div>
