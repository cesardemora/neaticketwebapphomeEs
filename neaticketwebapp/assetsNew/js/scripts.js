
jQuery(document).ready(function() {

    /*
        Fullscreen background
    */
    $.backstretch("assetsNew/img/3c.jpg");

    $('#top-navbar-1').on('shown.bs.collapse', function(){
    	$.backstretch("resize");
    });
    $('#top-navbar-1').on('hidden.bs.collapse', function(){
    	$.backstretch("resize");
    });

    /*
        Form validation
    */
    $('.registration-form input[type="text"], .registration-form textarea').on('focus', function() {
    	$(this).removeClass('input-error');
    });

    $('.registration-form').on('submit', function(e) {

    	$(this).find('input[type="text"], textarea').each(function(){
    		if( $(this).val() == "" ) {
    			e.preventDefault();
    			$(this).addClass('input-error');
    		}
    		else {
    			$(this).removeClass('input-error');
    		}
    	});

    });

     /*
        Background slideshow
    */
    // $.backstretch([
    //                                "assetsNew/img/backgrounds/1.jpg"
    //                              , "assetsNew/img/backgrounds/2.jpg"
    //                              , "assetsNew/img/backgrounds/3.jpg"
    //                              ], {duration: 3000, fade: 750});

    // $.backstretch("assetsNew/img/backgrounds/2.jpg");
    // $.backstretch("assetsNew/img/backgrounds/3.jpg");
    // $.backstretch("assetsNew/img/backgrounds/1.jpg");

    // $('#top-navbar-1').on('shown.bs.collapse', function(){
    //     $('.top-content').backstretch("resize");
    // });
    // $('#top-navbar-1').on('hidden.bs.collapse', function(){
    //     $('.top-content').backstretch("resize");
    // });

    // $('a[data-toggle="tab"]').on('shown.bs.tab', function() {
    //     $('.testimonials-container').backstretch("resize");
    // });


    /*
        Modals
    */
    $('.launch-modal').on('click', function(e){
        e.preventDefault();
        $( '#' + $(this).data('modal-id') ).modal();
    });


    // $(document).ready(function () {
    //     $("button#submit").click(function(){
    //         $.ajax({
    //             type: "POST",
    //             url: "lib/contacto.php", //process to mail
    //             data: $('form.contact').serialize(),
    //             success: function(msg){
    //                 $("#thanks").html(msg) //hide button and show thank you
    //                 $("#form-content").modal('hide'); //hide popup
    //             },
    //             error: function(){
    //                 alert("failure");
    //             }
    //         });
    //     });
    // });


});

