<?php

    require_once ('class/connect.php');
    
    $clase = new connect;
    $clase->dbConnects();

    $conn = dbConnect();
    $OK = true; // We use this to verify the status of the update.
    $msg = '';
    
    // If 'buscar' is in the array $_POST proceed to make the query.
    //if (isset($_GET['ide'])) {
    if(isset($_SESSION['email'])){
        
        mysql_query("SET NAMES 'utf8'");
        $consulta = "SELECT * FROM SECTORES ORDER BY id ASC";
        $resultado = mysql_query($consulta);
        
        $consulta2 = "SELECT * FROM CODIGOSBARRAS ORDER BY id ASC";
        $resultado2 = mysql_query($consulta2);
        // Create the query
        $sql = 'SELECT * FROM TIENDAS WHERE mail = ?';
        // we have to tell the PDO that we are going to send values to the query
        $stmt = $conn->prepare($sql);
        // Now we execute the query passing an array toe execute();
        $results = $stmt->execute(array($_SESSION['email']));
        // Extract the values from $result
        $row = $stmt->fetch();

        if (empty($row)) {
                $result = "No se encontraron resultados !!";
        }
    }
    // remove comment to verify the information in the $_POST array.
    // var_dump($_POST);
    if (array_key_exists('update', $_POST)) {
            // Create the query
            $sql = 'UPDATE TIENDAS SET password = ?, direccion = ?, cp = ?, telefono = ?, telefono_servicio = ?, web = ?, correo_info = ?, dias_devolucion = ?, meses_garantia = ?, sector = ?, horario = ?, moneda = ?, localidad = ?, tipo_barras = ?, id_tienda = ?  WHERE mail = ?';
            // we have to tell the PDO that we are going to send values to the query
            $stmt = $conn->prepare($sql);
            // Now we execute the query passing an array to execute();
            $OK = $stmt->execute(array($_POST['pass'],$_POST['direccion'],$_POST['cp'],$_POST['telefono'],$_POST['telefono_servicio'],$_POST['web'],$_POST['correo_info'],$_POST['dias_devolucion'],$_POST['meses_garantia'],$_POST['Sectores'],$_POST['horario'],$_POST['moneda'],$_POST['localidad'],$_POST['tipo_barras'],$_POST['id_tienda'],$_SESSION['email']));
            // In case of any error, we get the values.
            $error = $stmt->errorInfo();
            // We use this to verify the integrity of the update.
            if (!$OK) {
                    echo $error[2];

            } else {
                    // echo '<p>El registro se actualizo correctamente</p>';
                    //header("refresh: 0;editUserNewEmp.php?ide=".$_GET['ide']);
                    header("refresh: 0;index2emp.php");             
                    exit;
            }
    }
 ?>

<!DOCTYPE html>
<html lang="en">

    <head>

        <?php include("lib/headNew.php"); ?>

    </head>

    <body>

        <!-- Top menu -->
        <nav class="navbar navbar-inverse navbar-no-bg" role="navigation">

            <?php include("lib/navNew.php"); ?>

        </nav>

        <!-- Loader -->
        <div class="loader">
            <div class="loader-img"></div>
        </div>

        <div class="container">
            <div class="row">
                <div  id="capa" class="container wow fadeInUp">>

                    <?php
                            // If there are no records.
                            if(!$OK) :
                                    echo "";
                            else :
                    ?>

                    <div class="jumbotron">
                        <h1>Editar<strong> Perfil</strong> Empresa</h1>
                            
                            <form form role="form" class="registration-form" name="login_user" id="form1" method="post" action="">
                                <div>
                                    <div class="form-group">
                                        <label class="sr-only2" for="ide">Email Tienda</label>
                                        <input type="text" name="ide" id="ide" disabled="disabled" value="<?php echo $row['mail'];?>" class="form-first-name form-control">
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="sr-only2" for="marca">Nombre Marca</label>
                                        <input type="text" name="marca" id="marca" disabled="disabled" value="<?php echo $row['nombre_marca'];?>" class="form-first-name form-control">
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="sr-only2" for="cif">CIF</label>
                                        <input type="text" name="cif" id="cif" disabled="disabled" value="<?php echo $row['cif_empresa'];?>" class="form-first-name form-control">
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="sr-only2" for="form-id">Password</label>
                                        <input type="text" name="pass" id="pass" value="<?php echo $row['password'];?>" class="form-first-name form-control" required>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="sr-only2" for="form-id">Identificador Tienda</label>
                                        <input type="text" name="id_tienda" id="id_tienda" value="<?php echo $row['id_tienda'];?>" class="form-first-name form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label class="sr-only2" for="form-nick">Dirección</label>
                                        <input type="text" name="direccion" id="direccion" value="<?php echo $row['direccion'];?>" class="form-first-name form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label class="sr-only2" for="form-id">Código Postal</label>
                                        <input type="number" name="cp" id="cp" value="<?php echo $row['cp'];?>" class="form-first-name form-control" required>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="sr-only2" for="form-id">Localidad</label>
                                        <input type="text" name="localidad" id="localidad" value="<?php echo $row['localidad'];?>" class="form-first-name form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only2" for="form-id">Teléfono</label>
                                        <input type="number" name="telefono" id="telefono" value="<?php echo $row['telefono'];?>" class="form-first-name form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only2" for="form-id">Teléfono Servicio Técnico</label>
                                        <input type="number" name="telefono_servicio" id="telefono_servicio" value="<?php echo $row['telefono_servicio'];?>" class="form-first-name form-control">
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only2" for="form-id">Web</label>
                                        <input type="text" name="web" id="web" value="<?php echo $row['web'];?>" class="form-first-name form-control" placeholder="www.example.com">
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only2" for="form-id">Correo de Contacto</label>
                                        <input type="email" name="correo_info" id="correo_info" value="<?php echo $row['correo_info'];?>" class="form-first-name form-control">
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only2" for="form-id">Días de Devolución</label>
                                        <input type="number" maxlength="3" name="dias_devolucion" id="dias_devolucion" value="<?php echo $row['dias_devolucion'];?>" class="form-first-name form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only2" for="form-id">Meses de Garantía</label>
                                        <input type="number" maxlength="3" name="meses_garantia" id="meses_garantia" value="<?php echo $row['meses_garantia'];?>" class="form-first-name form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only2" for="form-id">Sector: </label>
                                        <select name="Sectores" class="form-group" >
                                            <?php
                                                $sector = $row['sector'];
                                                while ($fila=  mysql_fetch_assoc($resultado)){

                                                    if ($fila['nombre']==$sector)
                                                        $seleccion = "selected";
                                                    else 
                                                        $seleccion = "";
                                                    
                                                    if ($fila['nombre']!="Todos")
                                                        echo "<option value='".$fila['nombre']."' ".$seleccion.">".$fila['nombre']."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only2" for="form-id">Horario</label>
                                        <input type="text" name="horario" id="horario" value="<?php echo $row['horario'];?>" class="form-first-name form-control">
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only2" for="form-id">Moneda</label>
                                        <input type="text" maxlength="3" name="moneda" id="moneda" value="<?php echo $row['moneda'];?>" class="form-first-name form-control" placeholder="EUR">
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only2" for="form-id">Tipo Código de Barras</label>
                                        <select name="tipo_barras" class="form-group" >
                                            <?php
                                                $tipo = $row['tipo_barras'];
                                                while ($fila2=  mysql_fetch_assoc($resultado2)){

                                                    if ($fila2['nombre']==$tipo)
                                                        $seleccion = "selected";
                                                    else 
                                                        $seleccion = "";
                                                    
                                                    echo "<option value='".$fila2['nombre']."' ".$seleccion.">".$fila2['nombre']."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>                                    
                                    
                                    <div class="form-group btn2">
                                        <button type="submit" name="update" class="btn4">Actualizar</button>
                                        <a href="index2emp.php"><button name="boton2" id="boton2" type="button" class="btn">
                                        <span class="typcn typcn-arrow-left"></span> Volver</button></a>
                                    </div>

                                </div>

                            <?php endif;?>
                            </form>
                    </div>
                            <!-- </form> -->
                </div>
            </div>
        </div> <!--/ .container -->

<!-- MODAL: Privacy policy -->
        <?php include("lib/modalNew.php"); ?>

        <!-- Javascript -->
        <script type="" src="assetsNew/js/jquery-1.11.3.min.js"></script>
        <script type="" src="assetsNew/js/bootstrap.min.js"></script>
        <script type="" src="assetsNew/js/jquery.backstretch.min.js"></script>
        <script type="" src="assetsNew/js/retina-1.1.0.min.js"></script>
        <script type="" src="assetsNew/js/wow.min.js"></script>
        <script type="" src="assetsNew/js/scripts.js"></script>
        <script type="" src="assetsNew/js/scripts-screen.js"></script>
        <script type="" src="assetsNew/js/main.js"></script>

    </body>

</html>