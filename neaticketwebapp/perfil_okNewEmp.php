<?php

    $nombre_marca = $_SESSION['nombre_marca'];
    mysql_query("SET NAMES 'utf8'");
    $consulta = "SELECT * FROM TIENDAS WHERE id_tienda not in ('ULANKA (PRUEBAS)') and nombre_marca = '".$nombre_marca."' ORDER BY mail ASC";
    $resultado = mysql_query($consulta);
    $primera = true;

    if(isset($_GET['tienda'])){
        $tienda = $_GET['tienda'];
    }else{
        if(isset($_POST['Tiendas']))
            $tienda = $_POST['Tiendas'];
        else
            $tienda = "Todas";
    }

    /* capturar variable por método GET */
    if (isset($_GET['pos']))
      $ini=$_GET['pos'];
    else
      $ini=1;

    $url = basename($_SERVER ["PHP_SELF"]);
    $limit_end = 15;
    $init = ($ini-1) * $limit_end;

?>

<div class="container">
    <div class="row">
        <div  id="capa" class="container wow fadeInUp">
            <div class="jumbotron">
                <h1>Listado de <strong>Tickets</strong></h1>
                <div class="container2">

                    <!-- ******************************************************************************** -->

                    <div class="form-group">
                        <form method="POST" class="form" action="">
                            <div class="row">
                                <div class="col-lg-3">
                                        <label class="control-label">Búsqueda por Nº Ticket::</label>
                                        <div class="">
                                              <!-- <input type="date" class="form-control" name="dia" placeholder="Dia" required=""> -->
                                              <!-- <input placeholder="Búsqueda por Día..." class="form-control" type="text" name="dia" onfocus="this.type='date';this.setAttribute('onfocus','');this.blur();this.focus();"> -->
                                            <span class="input-group-btn">
                                            <input type="text" id="daterange" name="daterange" value='<?php echo $valorDate;?>'>
                                            <!-- <button class="btn" type="submit" name="BuscarDia">Ver</button> -->
                                            </span>
                                        </div>
                                </div>
                                <div class="col-lg-3">
                                    <label class="control-label">Búsqueda por Email:</label>
                                    <div class="width100">
                                        <!--<select name="Tiendas" class="form-group" onchange= "this.form.submit()" >-->
                                        <select name="Tiendas" id="Tiendas" class="form-group" onchange= "TiendaSelectHandler(this)" >
                                            <?php
                                                while ($filaT=  mysql_fetch_assoc($resultadoTienda)){

                                                    if ($primera == true){
                                                        if ($tienda == "Todas")
                                                            $seleccion = "selected";
                                                        else
                                                            $seleccion = "";

                                                        echo "<option value='Todas' ".$seleccion.">Todas</option>";

                                                        $primera = false;
                                                    }

                                                    if ($filaT['mail']==$tienda){
                                                        $seleccion = "selected";
                                                        $_SESSION['id_tienda_filtro']= $filaT['id_tienda'];
                                                    }else
                                                        $seleccion = "";

                                                    echo "<option value='".$filaT['mail']."' ".$seleccion.">".$filaT['id_tienda']."</option>";
                                                }
                                             ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <label id="label-provincias" class="control-label">Búsqueda por Día:</label>
                                    <div class="">
                                        <select name="Provincias" id="Provincias" class="form-group" onchange = "ProvinciaSelectHandler(this)" >
                                            <?php
                                                while ($filaP=  mysql_fetch_assoc($resultadoProvincia)){

                                                    if ($primeraP == true){
                                                        if ($provincia == "Todas")
                                                            $seleccion = "selected";
                                                        else
                                                            $seleccion = "";

                                                        echo "<option value='Todas' ".$seleccion.">Todas</option>";

                                                        $primeraP = false;
                                                    }

                                                    if ($filaP['provincia']==$provincia)
                                                        $seleccion = "selected";
                                                    else
                                                        $seleccion = "";

                                                    echo "<option value='".$filaP['provincia']."' ".$seleccion.">".$filaP['provincia']."</option>";
                                                }
                                             ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <label class="control-label">Buscar:</label>
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button class="btn btn4" type="submit" name="Buscar"><i class="fa fa-search" aria-hidden="true"></i> Buscar</button>
                                            <a href="index_estadisticas.php">
                                            <button class="btn right" name="" id="" type="button">
                                            <i class="fa fa-refresh" aria-hidden="true"></i> Reiniciar</button></a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <!-- ******************************************************************************** -->

                    <div class="form-group">
                        <div class="row">
                            <form method="GET" class="form-inline" action="index_buscarTicketsNew.php">
                                <div class="col-lg-4">
                                <label id="label-provincias" class="control-label">Búsqueda por Nº Ticket:</label>
                                    <div class="input-group">
                                      <input type="text" class="form-control" name="num_ticket" placeholder='número de ticket'>
                                      <span class="input-group-btn">
                                        <button class="btn btn-secondary" type="submit" name="BuscarTicket">Buscar</button>
                                      </span>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                <label id="label-provincias" class="control-label">Búsqueda por Email:</label>
                                    <div class="input-group">
                                      <input type="email" class='form-control' name="emailCliente" placeholder='email del cliente'>
                                      <span class="input-group-btn">
                                        <button class="btn btn-secondary" type="submit" name="BuscarCliente">Buscar</button>
                                      </span>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                <label id="label-provincias" class="control-label">Búsqueda por Día:</label>
                                    <div class="input-group">
                                        <input class='form-control-static' type="date" name="fecha_ticket" placeholder='fecha compra'>
                                        <span class="input-group-btn">
                                        <button class="btn btn-secondary" type="submit" name="BuscarFecha">Buscar</button>
                                      </span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <!-- ******************************************************************************** -->

                    <!-- </form> -->
                    <form method="POST" class="form-inline" action="index2TicketsEmp.php">
                        <div class="row">
                            <div class="col-lg-4">
                                <label for="tienda_name" class="control-label">Búsqueda por Tiendas:</label>
                                <div class="input-group">
                                    <select name="Tiendas" class="form-group" onchange = "this.form.submit()" >

                                        <?php
                                            while ($fila=  mysql_fetch_assoc($resultado)){

                                                if ($primera == true){
                                                    if ($tienda == "Todas")
                                                        $seleccion = "selected";
                                                    else
                                                        $seleccion = "";

                                                    echo "<option value='Todas' ".$seleccion.">Todas</option>";

                                                    $primera = false;
                                                }

                                                if ($fila['mail']==$tienda)
                                                    $seleccion = "selected";
                                                else
                                                    $seleccion = "";

                                                echo "<option value='".$fila['mail']."' ".$seleccion.">".$fila['id_tienda']."</option>";
                                            }
                                        ?>

                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>

                    <!-- ******************************************************************************** -->

                    <!-- Table -->
                    <div class="table-responsive">
                        <table class="table">

                            <?php
                                $mail_tienda = $_SESSION['email'];
                                //$cif_empresa = $_SESSION['cif_empresa'];
                                $nombre_marca = $_SESSION['nombre_marca'];

                                //Consulta para obtener todos los ticket de una tienda
                                //$result = mysql_query("SELECT * FROM TICKETS WHERE mail_tienda='$mail_tienda'");
                                //Consulta para obtener todos los ticket de todas las tiendas de una empresa
                                //$result = mysql_query("SELECT * FROM TICKETS WHERE mail_tienda IN (SELECT mail FROM TIENDAS WHERE cif_empresa = '$cif_empresa') ORDER BY fecha DESC, hora DESC");
                                //Consulta para obtener todos los ticket de todas las tiendas de una Marca
                                if((isset($_POST['Tiendas']) || isset($_GET['tienda'])) && $tienda != "Todas"){
                                    $sql = "SELECT * FROM TICKETS WHERE mail_tienda = '".$tienda."' ORDER BY fecha DESC, hora DESC";
                                    $count = "SELECT COUNT(*) FROM TICKETS WHERE mail_tienda = '".$tienda."' ORDER BY fecha DESC, hora DESC";

                                }else{
                                    $sql = "SELECT * FROM TICKETS WHERE mail_tienda IN (SELECT mail FROM TIENDAS WHERE nombre_marca = '$nombre_marca') ORDER BY fecha DESC, hora DESC";
                                    $count = "SELECT COUNT(*) FROM TICKETS WHERE mail_tienda IN (SELECT mail FROM TIENDAS WHERE nombre_marca = '$nombre_marca') ORDER BY fecha DESC, hora DESC";

                                }
                                $sql .= " LIMIT $init, $limit_end";
                                $result = mysql_query($sql);

                                if ($row = mysql_fetch_array($result)){

                                    $num = $con->query($count);
                                    $x = $num->fetch_array();
                                    $total = ceil($x[0]/$limit_end);

                                    echo "<thead><br><tr>
                                            <td><strong>TICKET</strong></td>
                                            <td><strong>CLIENTE</strong></td>
                                            <td><strong>FECHA</strong></td>
                                            <td><strong>HORA</strong></td>
                                            <td><strong>TIENDA</strong></td>
                                            <td><strong>CAJA</strong></td>
                                            <td><strong>CAJERO</strong></td>
                                            <td><strong>PVP</strong></td>
                                            <td></td>
                                            </tr></thead>";
                                    do {
                                        $date = date("d/m/Y", strtotime($row['fecha']));

                                        //<td><strong>ESTADO</strong></td>
                                        //mostramos solo con el nombre en el mail de la tienda
                                        $caracter   = '@';
                                        $posicion = strpos($row['mail_tienda'], $caracter);
                                        $nom_tienda = substr($row['mail_tienda'], 0, $posicion);

                                        echo '<tr><td>'.$row['num_ticket'].'</td>';
                                        echo '<td>'.$row['cliente'].'</td>';
                                        echo '<td>'.$date.'</td>';
                                        echo '<td>'.$row['hora'].'</td>';
                                        echo '<td>'.$nom_tienda.'</td>';
                                        echo '<td>'.$row['caja'].'</td>';
                                        echo '<td>'.$row['cajero'].'</td>';
                                        echo '<td>'.$row['total'].'</td>';
                                    //  echo '<td>'.$row['estado'].'</td>';

                                        if ($row['fichero'] != "")
                                            echo '<td><a href="'.$row['fichero'].'" target="blank" class="typcn typcn-zoom-in"></a></td></tr>';
                                            //echo '<td><a href="TicketsPDF/ULANKA (PRUEBAS)/2016-05-02/2016-05-02_14_45_25.pdf.png" target="blank" class="typcn typcn-zoom-in"></a></td></tr>';
                                        else
                                            echo '<td><a href="ticketPdf_ok_1.php?id='.$row['id'].'&tipo=2" target="blank" class="typcn typcn-zoom-in"></a></td></tr>';
                                                //echo '<td><a href="reporte_historial.php?id='.$row['id'].'">ver</a></td></tr>';
                                    } while ($row = mysql_fetch_array($result));

                                } else {
                                    echo "¡ No se ha encontrado ningún registro !";
                                }
                            ?>

                        </table>
                        <div class="row">
                            <div class="col-md-8"></div>
                            <div class="col-md-4">

                            <?php
                            if ($total > 1){
                                /* numeración de registros [importante]*/
                                echo '<ul class="pagination">';
                                /****************************************/
                                if(($ini - 1) == 0){
                                  echo "<li><a href='#'>&laquo;</a></li>";
                                }else{
                                  echo "<li><a href='$url?pos=".($ini-1)."&tienda=$tienda'><b>&laquo;</b></a></li>";
                                }
                                /****************************************/
                                $fin = $ini +4;
                                if($fin >= $total)
                                    $fin = $total;

                                for($k= $ini ; $k <= $fin; $k++){
                                  if($ini == $k){
                                    echo "<li class='active'><a href='#'><b>".$k."</b></a></li>";
                                  }else{
                                    echo "<li><a href='$url?pos=$k&tienda=$tienda'>".$k."</a></li>";
                                  }
                                }
                                /****************************************/
                                if($ini == $total){
                                  echo "<li><a href='#'>&raquo;</a></li>";
                                }else{
                                  echo "<li><a href='$url?pos=".($ini+1)."&tienda=$tienda'><b>&raquo;</b></a></li>";
                                }
                                /*******************END*******************/
                                echo "</ul>";
                            }
                            ?>
                            </div>
                        </div>
                    </div>

                    <div>
                        <a href="index2emp.php#login"><button name="boton2" id="boton2" type="button" class="btn">
                        <span class="typcn typcn-arrow-left"></span> Volver</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
