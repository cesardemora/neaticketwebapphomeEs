<?php
/*
 * @author Puneet Mehta
 * @website: http://www.PHPHive.info
 * @facebook: https://www.facebook.com/pages/PHPHive/1548210492057258
 */

require('class/http.php');
require('class/oauth_client.php');
require('config2.php');
// require('config.php');
require('lib/generaPass.php');

require_once 'config.php';
if (!isset($_SESSION["user_id"]) && $_SESSION["user_id"] == "") {
  // user already logged in the site
  header("location:" . SITE_URL);
}

$client = new oauth_client_class;

// set the offline access only if you need to call an API
// when the user is not present and the token may expire
$client->offline = FALSE;

$client->debug = false;
$client->debug_http = true;
$client->redirect_uri = REDIRECT_URL;

$client->client_id = CLIENT_ID;
$application_line = __LINE__;
$client->client_secret = CLIENT_SECRET;

if (strlen($client->client_id) == 0 || strlen($client->client_secret) == 0)
  die('Please go to Google APIs console page ' .
          'http://code.google.com/apis/console in the API access tab, ' .
          'create a new client ID, and in the line ' . $application_line .
          ' set the client_id to Client ID and client_secret with Client Secret. ' .
          'The callback URL must be ' . $client->redirect_uri . ' but make sure ' .
          'the domain is valid and can be resolved by a public DNS.');

/* API permissions
 */
$client->scope = SCOPE;
if (($success = $client->Initialize())) {
  if (($success = $client->Process())) {
    if (strlen($client->authorization_error)) {
      $client->error = $client->authorization_error;
      $success = false;
    } elseif (strlen($client->access_token)) {
      $success = $client->CallAPI(
              'https://www.googleapis.com/oauth2/v1/userinfo', 'GET', array(), array('FailOnAccessError' => true), $user);
    }
  }
  $success = $client->Finalize($success);
}
if ($client->exit)
  exit;
if ($success) {
  // Now check if user exist with same email ID
  // $sql = "SELECT COUNT(*) AS count from google_users where email = :email_id";
  // $sql = "SELECT COUNT(*) AS count from usuarios where email = :email_id";
  $sql = "SELECT COUNT(*) AS count from CLIENTES where mail = :email_id";
  try {
    $stmt = $DB->prepare($sql);
    $stmt->bindValue(":email_id", $user->email);
    $stmt->execute();
    $result = $stmt->fetchAll();

    $_SESSION["login_redes"] = "yes";
    
    if ($result[0]["count"] > 0) {
      // User Exist

      $_SESSION["name"] = $user->name;
      $_SESSION["email_redes"] = $user->email;
      $_SESSION["new_user"] = "no";

//      $_SESSION['emailC']=                $ses["mail"];
//      $_SESSION['nick']=                  $ses["nombre"];
//      $_SESSION['pass']=                  $ses["password"];
//      $_SESSION['recibir_ofertas']=       $ses["recibir_ofertas"];
//      $_SESSION['recibir_correos']=       $ses["recibir_correos"];
//      $_SESSION['recibir_devoluciones']=  $ses["recibir_devoluciones"];
//      $_SESSION['recibir_garantias']=     $ses["recibir_garantias"];
//      $_SESSION['fecha']=                 $ses["fecha_alta"];
      //$_SESSION['foto']=                    $ses["foto"];

    } else {
      // New user, Insert in database
      // $sql = "INSERT INTO `google_users` (`name`, `email`) VALUES " . "( :name, :email)";
      // $sql = "INSERT INTO `usuarios` (`nick`, `email`, `pass`) VALUES " . "( :name, :email, '$password')";
      $sql = "INSERT INTO `CLIENTES` (`nombre`, `mail`, `password`) VALUES " . "( :name, :email, '$password')";
      $stmt = $DB->prepare($sql);
      $stmt->bindValue(":name", $user->name);
      $stmt->bindValue(":email", $user->email);
      $stmt->execute();
      $result = $stmt->rowCount();
      if ($result > 0) {
        $_SESSION["name"] = $user->name;
        $_SESSION["email_redes"] = $user->email;
        $_SESSION["new_user"] = "yes";
        $_SESSION["e_msg"] = "";
      }
    }
    //Registramos el acceso en la BD
    $ipuser= $_SERVER['REMOTE_ADDR'];
    $sql2 = "INSERT INTO ACCESOSWEB (email,tipo,ip,mensaje) VALUES (:email, :tipo, :ip,:mensaje)";
    $stmt = $DB->prepare($sql2);
    $stmt->bindValue(":email", $user->email);
    $stmt->bindValue(":tipo", 'usuario');
    $stmt->bindValue(":ip", $ipuser);
    $stmt->bindValue(":mensaje", 'Acceso correcto');
    $stmt->execute();
    
  } catch (Exception $ex) {
    $_SESSION["e_msg"] = $ex->getMessage();
  }

  $_SESSION["user_id"] = $user->id;
} else {
  $_SESSION["e_msg"] = $client->error;
}
header("location:index.php");
exit;
?>