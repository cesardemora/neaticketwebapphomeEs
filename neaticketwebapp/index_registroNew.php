<?php

include('class/connectA.php');
$clase = new connect;
$clase->dbConnects();

?>

<!DOCTYPE html>
<html lang="en">

    <head>

        <?php include("lib/headNew.php"); ?>

    </head>

    <body>

        <!-- Top menu -->
        <nav class="navbar navbar-inverse navbar-no-bg" role="navigation">

            <?php include("lib/navNew.php"); ?>

        </nav>

        <!-- Loader -->
        <div class="loader">
            <div class="loader-img"></div>
        </div>

        <!-- Top content -->
        <div class="top-content">

            <?php include("lib/contentNewReg2.php"); ?>

        </div>

        <!-- MODAL: Privacy policy -->
        <?php include("lib/modalNew.php"); ?>

        <!-- Javascript  scripts.php -->
        <?php include("lib/scripts.php"); ?>

    </body>

</html>