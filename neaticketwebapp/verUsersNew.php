<?php
/* capturar variable por método GET */
if (isset($_GET['pos']))
  $ini=$_GET['pos'];
else
  $ini=1; 

$url = basename($_SERVER ["PHP_SELF"]);
$limit_end = 15;
$init = ($ini-1) * $limit_end;

?>

<div class="container">
	<div class="row">
		<div  id="capa" class="container wow fadeInUp">
			<div class="jumbotron">
			    
                            <h1>                               
                                Listado de <strong>Clientes</strong>                           
                            </h1>

                                <?php

                                $mail_tienda = $_SESSION['email'];
                                //$cif_empresa = $_SESSION['cif_empresa'];
                                $nombre_marca = $_SESSION['nombre_marca'];

                                //Consulta para sacar todos los clientes de una Tienda
                                //$sql = "SELECT DISTINCT CLIENTES.* FROM TICKETS, CLIENTES WHERE cliente = mail AND mail_tienda='$mail_tienda'";

                                //Consulta para obtener todos los clientes de todas las tiendas de una empresa.
                                //$sql = "SELECT DISTINCT CLIENTES.* FROM TICKETS, CLIENTES WHERE cliente = mail AND mail_tienda IN (select mail FROM TIENDAS where cif_empresa = '$cif_empresa')";

                                //Consulta para obtener todos los clientes de todas las tiendas de una Marca.
                                $sql = "SELECT DISTINCT CLIENTES.* FROM TICKETS, CLIENTES WHERE cliente = mail AND mail_tienda IN (select mail FROM TIENDAS where nombre_marca = '$nombre_marca') ORDER BY fecha_alta DESC";
                                $sql .= " LIMIT $init, $limit_end";
                                $count = "SELECT COUNT(DISTINCT CLIENTES.mail) as 'contador' FROM TICKETS, CLIENTES WHERE cliente = mail AND mail_tienda IN (select mail FROM TIENDAS where nombre_marca = '$nombre_marca') ORDER BY fecha_alta DESC";

                                if(!$resultado = $con->query($sql)){
                                    die('Ocurrio un error ejecutando el query [' . $con->error . ']');
                                }else{
                                    $num = $con->query($count);
                                    $x = $num->fetch_array();
                                    $total = ceil($x[0]/$limit_end);
                                }

                                $con->close();

                                ?>


				<div class="container2">

					  <!-- <div id="content"> -->
                                          <div class="buscar"><br>
					    <form method="GET" class="form-inline" action="index_buscarNew.php">
					      <div class="form-group">
					        <input class='form-control' type="text" name="email" size="25" placeholder='escribe el email'>
					      </div>

					      <div class="form-group">
							<button class="btn" type="submit" name="BuscarCliente">Buscar</button>
					      </div>
                                              
                                            <div class="form-group">
                                                <input class='form-control-static' type="date" name="fecha_alta" size="" placeholder='fecha alta'>
                                            </div>
                                            <div class="form-group">
                                                <button class="btn" type="submit" name="BuscarAlta">Buscar</button>
                                            </div>

					    </form>
					</div>
					<div class="table-responsive">
					    <table class="table" id="tabla">
					      <thead>
					        <tr>
					          <th>Email</th>
<!--					          <th>Nombre</th>-->
					          <th>Sexo</th>
                                                  <th>Rango Edad</th>
                                                  <th>Ofertas</th>
                                                  <th>Correos</th>
                                                  <th>Devol.</th>
                                                  <th>Garantías</th>
                                                  <th>Fecha Alta</th>
					        </tr>
					      </thead>

						<tbody>
							<?php
							  while($fila = $resultado->fetch_assoc()){

                                                                if ($fila['recibir_ofertas'] == "1")
                                                                    $ofertas = "Sí";
                                                                else
                                                                    $ofertas = "No";

                                                                if ($fila['recibir_correos'] == "1")
                                                                    $correos = "Sí";
                                                                else
                                                                    $correos = "No";

                                                                if ($fila['recibir_devoluciones'] == "1")
                                                                    $devol = "Sí";
                                                                else
                                                                    $devol = "No";

                                                                if ($fila['recibir_garantias'] == "1")
                                                                    $garantias = "Sí";
                                                                else
                                                                    $garantias = "No";

                                                                $date = date("d/m/Y", strtotime($fila['fecha_alta']));

//                                                                  echo '
//                                                                    <tr id="fila_'.$fila['mail'].'"> 
//                                                                    <td>'.$fila['nombre'].'</td>'
                                                            ?>
                                                                    <td><a href="index_buscarTicketsNew.php?mailCliente=<?php echo $fila['mail']; ?>"><?php echo $fila['mail']; ?></a></td>
                                                            <?php
                                                                  echo '
                                                                      <td>'.$fila['sexo'].'</td>
                                                                      <td>'.utf8_encode($fila['edad']).'</td>
                                                                      <td>'.$ofertas.'</td>
                                                                      <td>'.$correos.'</td>
                                                                      <td>'.$devol.'</td>
                                                                      <td>'.$garantias.'</td>
                                                                      <td>'.$date.'</td>
                                                                    </tr>';
							     }
							 ?>
						</tbody>
					</table>
                                        <div class="row">
                                            <div class="col-md-8"></div>
                                            <div class="col-md-4">

                                            <?php
                                            if ($total > 1){
    //                                            /* numeración de registros [importante]*/
                                                echo '<ul class="pagination">';
                                                /****************************************/
                                                if(($ini - 1) == 0){
                                                  echo "<li><a href='#'>&laquo;</a></li>";
                                                }else{
                                                  echo "<li><a href='$url?pos=".($ini-1)."'><b>&laquo;</b></a></li>";  
                                                }
                                                /****************************************/
                                                $fin = $ini +4;                                                                                            
                                                if($fin >= $total)
                                                    $fin = $total;
                                                
                                                for($k= $ini ; $k <= $fin; $k++){
                                                  if($ini == $k){
                                                    echo "<li class='active'><a href='#'><b>".$k."</b></a></li>";
                                                  }else{
                                                    echo "<li><a href='$url?pos=$k'>".$k."</a></li>";
                                                  }
                                                }
                                                /****************************************/
                                                if($ini == $total){
                                                  echo "<li><a href='#'>&raquo;</a></li>";
                                                }else{
                                                  echo "<li><a href='$url?pos=".($ini+1)."'><b>&raquo;</b></a></li>";
                                                }
                                                /*******************END*******************/
                                                echo "</ul>";
                                            }
                                            ?>
                                            </div>
                                        </div>
                                    </div>
                                    
<!--					<h1>Añadir <strong>Cliente</strong></h1>

                                        <form method="POST" class="form-inline" action="index_buscarNew.php">
						<div class="form-group">
						  <input type='text' class='form-control' placeholder='nombre' name="nick">
						</div>

						<div class="form-group">
						  <input type='email' class='form-control' placeholder='email' name="email">
						</div>

						<div class="form-group">
						  <input type='text' class='form-control' placeholder='password' name="pass">
						</div>

						<div class="form-group">
							<button type="submit" class="btn" name="AgregarCliente">Añadir</button>
                                                        <button type="submit" value="Submit" class="btn btn-success">Añadir</button>
						</div>

					</form>-->

					<div class="form-group"><br>
						<a href="index2emp.php#login"><button name="boton2" id="boton2" type="button" class="btn">
						<span class="typcn typcn-arrow-left"></span> Volver</button></a>
					</div>


				</div>


<!--					<script type="text/javascript">

					        // Esta primera función agrega un disparador de  evento a la acción submit,
					        // esto quiere decir que cuando se presione el botón de submit del formulario
					        // con id 'formulario' entonces se ejecutara el código contenido en el.
					        $( "#formulario" ).submit(function( event ) {
					            event.preventDefault();

					            // Como estamos enviamos un formulario, la acción a realizar por AJAX debe ser post.
					            // Vamos a ver los parámetros que necesita el método $.post de jQuery para funcionar
					            // 1. Dirección a donde se va a enviar el formulario por medio de POST
					            // 2. Datos del formulario, la función serializa() convierte el formulario en una cadena de texto
					            // 3. Función que se ejecutara cuando se reciba la respuesta del servidor.

					            $.post('agregar.php',
					            $('#formulario').serialize(),
					            function(data) {
					               location.reload();
					                // Si todo salió bien en el servidor se devolvera éxito => true y se ejecutara el else
					                // pero si algo salió mal entonces se mostrar una alerta con el mensaje error
					                if (data.exito != true){
					                    alert('Error');
					                }
                                                        //else{

					                    // Si la persona se creo bien en la base de datos entonces insertamos una fila
					                    // con su información al final de la tabla
//					                    $('#tabla tr:last').after(
//					                     '<tr id="fila_'+data.id+'">'+
//					                          '<td>'+data.id+'</td>'+
//					                          '<td>'+data.nick+'</td>'+
//					                          '<td>'+data.email+'</td>'+
//					                          '<td>'+data.pass+'</td>'+
//					                          '<td>'+data.id_emp+'</td>'+
//					                          '<td><a onclick="eliminar('+data.id+')">Eliminar</a></td>'+
//					                        '</tr>'
//					                     );
//
//					                     location.reload();
					                //}
					            });

					        });

					        //Esta es la función que se llama al eliminar una persona de la tabla
					        function eliminar(id){

					            // con el método $.get hacemos una petición GET mediante AJAX con jQuery
					            // 1. El primer parámetro es la dirección a donde se va a hacer la petición y los parámetros de la misma
					            // en este caso el parámetro será el id de la persona que se va a eliminar.
					            // 2. El segundo parámetro es la función que se va a ejecutar cuando se reciba la respuesta del servidor.

					            $.get('eliminar.php?id='+id,
					            function(data){
					                if (data.exito != true){
					                  alert('Error');
					                }else{
					                    // si la respuesta fue exitosa entonces eliminamos la fila de la tabla
					                    $('#fila_'+id).remove();
					                }
					            });
					        }

					</script>-->

				</div>

			</div>
		</div>
	</div>
</div>
