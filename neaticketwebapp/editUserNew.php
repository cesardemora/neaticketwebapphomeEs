<?php

	require_once ('class/connect.php');

	$conn = dbConnect();
	$OK = true; // We use this to verify the status of the update.
	$msg = '';
	// If 'buscar' is in the array $_POST proceed to make the query.
	//if (isset($_GET['mail'])) {
        if(isset($_SESSION['emailC'])){
		// Create the query
		$sql = 'SELECT * FROM CLIENTES WHERE mail = ?';
		// we have to tell the PDO that we are going to send values to the query
		$stmt = $conn->prepare($sql);
		// Now we execute the query passing an array toe execute();
		//$results = $stmt->execute(array($_GET['mail']));
                $results = $stmt->execute(array($_SESSION['emailC']));
		// Extract the values from $result
		$row = $stmt->fetch();

		if (empty($row)) {
			$result = "No se encontraron resultados !!";
		}
	}
	// remove comment to verify the information in the $_POST array.
	// var_dump($_POST);
	if (array_key_exists('update', $_POST)) {
                
                if(!isset($_POST['ofertas']))
                    $ofertas = "0";
                else 
                    $ofertas = "1";
                
                if(!isset($_POST['devol']))
                    $devoluciones = "0";
                else 
                    $devoluciones = "1";

                if(!isset($_POST['garantias']))
                    $garantias = "0";
                else 
                    $garantias = "1";
                
                if(!isset($_POST['correos']))
                    $correos = "0";
                else 
                    $correos = "1";
                
                
                // Create the query
		$sql = 'UPDATE CLIENTES SET nombre = ?, password = ?, recibir_ofertas = ?, recibir_devoluciones = ?, recibir_garantias = ?, recibir_correos = ? WHERE mail = ?';
		// we have to tell the PDO that we are going to send values to the query
		$stmt = $conn->prepare($sql);
		// Now we execute the query passing an array to execute();
		$OK = $stmt->execute(array($_POST['nick'],$_POST['pass'],$ofertas,$devoluciones,$garantias,$correos,$_SESSION['emailC']));
		// In case of any error, we get the values.
		$error = $stmt->errorInfo();
		// We use this to verify the integrity of the update.
		if (!$OK) {
			echo $error[2];

		} else {
			// echo '<p>El registro se actualizo correctamente</p>';
			header("refresh: 0;index.php");
			exit;
		}
	}
 ?>

<!DOCTYPE html>
<html lang="en">

    <head>

        <?php include("lib/headNew.php"); ?>

    </head>

    <body>

        <!-- Top menu -->
        <nav class="navbar navbar-inverse navbar-no-bg" role="navigation">

            <?php include("lib/navNew.php"); ?>

        </nav>

        <!-- Loader -->
        <div class="loader">
            <div class="loader-img"></div>
        </div>



            <div class="container">
                    <div class="row">
                        <div  id="capa" class="container wow fadeInUp">
                                <!-- <form class="form-group" name="login_user" id="form1" method="POST" action=""> -->

                            <?php
                                    // If there are no records.
                                    if(!$OK) :
                                            echo "";
                                    else :
                            ?>

                    <div class="jumbotron">
                        <h1>Editar<strong> Perfil</strong></h1>
                            <form form role="form" class="registration-form" name="login_user" id="form1" method="post" action="">
                                    <div>

                            <div class="form-group">
                                    <label class="sr-only2" for="form-id">Email</label>
                                    <input type="text" name="id" id="id" disabled="disabled" value="<?php echo $row['mail'];?>" class="form-first-name form-control">
                            </div>

                            <div class="form-group">
                                    <label class="sr-only2" for="form-nick">Nombre</label>
                                    <input type="text" name="nick" id="nick" value="<?php echo $row['nombre'];?>" class="form-first-name form-control">
                            </div>

                            <div class="form-group">
                                    <label class="sr-only2" for="form-id">Password</label>
                                    <input type="text" name="pass" id="pass" value="<?php echo $row['password'];?>" class="form-first-name form-control">
                            </div>
                            <div class="form-group">
                                <label class="sr-only2" for="form-not">Recibir Notificaciones </label><br>
                                <?php 
                                if ($row['recibir_ofertas']== "1"){
                                    ?><input type="checkbox" name="ofertas" value="1" checked>  Ofertas <br> <?php 
                                }else{
                                    ?><input type="checkbox" name="ofertas" value="0">  Ofertas <br> <?php 
                                }
                                if ($row['recibir_devoluciones']== "1"){
                                    ?><input type="checkbox" name="devol" value="1" checked>  Devoluciones <br> <?php 
                                }else{
                                    ?><input type="checkbox" name="devol" value="0">  Devoluciones <br> <?php 
                                }
                                if ($row['recibir_garantias']== "1"){
                                    ?><input type="checkbox" name="garantias" value="1" checked>  Garantias <br> <?php 
                                }else{
                                    ?><input type="checkbox" name="garantias" value="0">  Garantias <br> <?php 
                                }
                                ?>
                            </div>
                            <div class="form-group">
                                <label class="sr-only2" for="form-correo">¿Quieres recibir una copia de los tickets en tú Email? </label>
                                <?php 
                                if ($row['recibir_correos']== "1"){
                                    ?><input type="checkbox" name="correos" value="1" checked>  <br> <?php 
                                }else{
                                    ?><input type="checkbox" name="correos" value="0">  <br> <?php 
                                }
                                ?>
                            </div>

                            <div class="form-group btn2">
                                <button type="submit" name="update" class="btn4">Actualizar</button>
                                        <a href="index.php"><button name="boton2" id="boton2" type="button" class="btn">
                                        <span class="typcn typcn-arrow-left"></span> Volver</button></a>
                                </div>

                            </div>

                        <?php endif;?>
                            </form>
                    </div>
                            <!-- </form> -->
                    </div>
            </div>
    </div> <!--/ .container -->

<!-- MODAL: Privacy policy -->
        <?php include("lib/modalNew.php"); ?>

        <!-- Javascript -->
        <script type="" src="assetsNew/js/jquery-1.11.3.min.js"></script>
        <script type="" src="assetsNew/js/bootstrap.min.js"></script>
        <script type="" src="assetsNew/js/jquery.backstretch.min.js"></script>
        <script type="" src="assetsNew/js/retina-1.1.0.min.js"></script>
        <script type="" src="assetsNew/js/wow.min.js"></script>
        <script type="" src="assetsNew/js/scripts.js"></script>
        <script type="" src="assetsNew/js/scripts-screen.js"></script>
        <script type="" src="assetsNew/js/main.js"></script>

    </body>

</html>