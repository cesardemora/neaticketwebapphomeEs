<?php

$nombre_marca = $_SESSION['nombre_marca'];

mysql_query("SET NAMES 'utf8'");
    $consulta = "SELECT * FROM TIENDAS WHERE id_tienda not in ('ULANKA (PRUEBAS)') and nombre_marca = '".$nombre_marca."' ORDER BY mail ASC";
    $resultado = mysql_query($consulta);
    $primera = true;

    if(isset($_POST['Tiendas']))
        $tienda = $_POST['Tiendas'];
    else
        $tienda = "Todas";


$filtros = "";

$filtrosEst = "";
$filtrosEst2 = "";
$semana = "";
$seleccionMes = "";

//Aberiguamos que función se está ejecutando y asignamos el literal

if(isset($_POST['BuscarDia']) && $_POST['dia'] != ""){
    $Funcion = "BuscarDia"; 
    $literalDatos= $_POST['dia'];
    $filtros = " and fecha = '$literalDatos' ";
}else{
   if(isset($_POST['BuscarSemana']) && $_POST['semana'] != ""){ 
       $Funcion = "BuscarSemana";
       $literalDatos= $_POST['semana'];
       $order = array ("-", "W");
       $semana = str_replace ($order, "", $_POST['semana']); 
       $filtros = " and YEARWEEK(fecha,1) =  '$semana' ";
       $filtrosEst = " and YEARWEEK(H.timestamp,1) =  '$semana' ";
       $filtrosEst2 = " and YEARWEEK(S.fecha_sesion,1) =  '$semana' ";
   }else{
       if(isset($_POST['BuscarMes']) && $_POST['mes'] != ""){ 
            $Funcion = "BuscarMes";
            $literalDatos= $_POST['mes'];
            $seleccionMes = explode("-", $_POST['mes']); 
            $filtros = "and YEAR(fecha) = '".$seleccionMes[0]."' AND MONTH(fecha) = '".$seleccionMes[1]."' ";
            $filtrosEst = "and YEAR(H.timestamp) = '".$seleccionMes[0]."' AND MONTH(H.timestamp) = '".$seleccionMes[1]."' ";
            $filtrosEst2 = "and YEAR(S.fecha_sesion) = '".$seleccionMes[0]."' AND MONTH(S.fecha_sesion) = '".$seleccionMes[1]."' ";
       }else{
            $Funcion = "BuscarGeneral"; 
            $literalDatos= 'Generales';
       }
   }
}

function obtenerClientes($marca,$filtro){
    //Obtenemos el número de clientes que tienen informado el campo sexo
    $sql = "SELECT COUNT(DISTINCT C.mail) as 'contador' FROM `TICKETS` AS T, `CLIENTES` AS C WHERE C.sexo is not null and C.mail = T.cliente and nombre_tienda = '$marca'".$filtro;

    if(!$resultado = mysql_query($sql)){
        die('Ocurrio un error ejecutando el query [' . $con->error . ']');
    }else{
        $fila = mysql_fetch_assoc($resultado);
        $clientes = $fila['contador'];
        return $clientes;
    }
}
function obtenerMujeres($marca,$filtro){
    $avgMujeres = 0;
    //Obtenemos el número de clientes que son Mujeres
    $sql = "SELECT COUNT(DISTINCT C.mail) as 'Mujeres' FROM `TICKETS` AS T, `CLIENTES` AS C WHERE C.sexo = 'Mujer' and C.mail = T.cliente and nombre_tienda ='$marca'".$filtro;
    if(!$resultado = mysql_query($sql)){
        die('Ocurrio un error ejecutando el query [' . $con->error . ']');
    }else{
            $fila = mysql_fetch_assoc($resultado);
            $mujeres = $fila['Mujeres'];
            $res = obtenerClientes($marca,$filtro);
            if ($res > 0)
                $avgMujeres = round(($mujeres / $res) * 100 , 1);
    }            
    return $avgMujeres;
}
function obtenerHombres($marca,$filtro){
    $avgHombres = 0;
    //Obtenemos el número de clientes que son Hombres
    $sql = "SELECT COUNT(DISTINCT C.mail) as 'Hombres' FROM `TICKETS` AS T, `CLIENTES` AS C WHERE C.sexo = 'Hombre' and C.mail = T.cliente and nombre_tienda ='$marca'".$filtro;
    if(!$resultado = mysql_query($sql)){
        die('Ocurrio un error ejecutando el query [' . $con->error . ']');
    }else{
            $fila = mysql_fetch_assoc($resultado);
            $hombres = $fila['Hombres'];

            $res = obtenerClientes($marca,$filtro);
            if ($res > 0)
                $avgHombres = round(($hombres / $res) * 100 , 1);
    }
    return $avgHombres;
}
function obtenerEdad($marca,$filtro){
    $avgEntre = array(0," ");
    //Obtenemos el número de clientes que tienes edad 'Entre 25 y 40'
    $sql = "SELECT COUNT(DISTINCT C.mail) as 'contEdad', C.edad FROM `TICKETS` AS T, `CLIENTES` AS C WHERE C.edad is not null and C.mail = T.cliente and nombre_tienda ='$marca'".$filtro." GROUP BY C.edad ORDER BY COUNT(DISTINCT C.mail) DESC";
    if(!$resultado = mysql_query($sql)){
        die('Ocurrio un error ejecutando el query [' . $con->error . ']');
    }else{
        $fila = mysql_fetch_assoc($resultado);
        $edad = $fila['contEdad'];
        $avgEntre[1] = $fila['edad'];

        $res = obtenerClientes($marca,$filtro);
        if ($res > 0)
            $avgEntre[0] = round(($edad / $res) * 100 , 1);
            
    } 
    return $avgEntre;
}

function obtenerNumTickets($marca,$filtro){
    //Obtenemos el número de Tickets que tienen informado el campo forma de pago
    $sql = "SELECT COUNT(*) as 'tickets' FROM `TICKETS` AS T WHERE T.datadi is not null and nombre_tienda = '$marca'".$filtro;

    if(!$resultado = mysql_query($sql)){
        die('Ocurrio un error ejecutando el query [' . $con->error . ']');
    }else{
        $fila = mysql_fetch_assoc($resultado);
        $num_tickets = $fila['tickets'];
        return $num_tickets;
    }
}
function obtenerPagoTarjeta($marca,$filtro){
    $avgTajeta = 0;
    //Obtenemos el número de Tickets que tienen informado el campo forma de pago distinto de efectivo
    $sql = "SELECT COUNT(*) as 'tarjeta' FROM `TICKETS` AS T WHERE T.datadi is not null and T.datadi not IN ('Efectivo') and nombre_tienda = '$marca'".$filtro;

    if(!$resultado = mysql_query($sql)){
        die('Ocurrio un error ejecutando el query [' . $con->error . ']');
    }else{
        $fila = mysql_fetch_assoc($resultado);
        $pago_tarjeta = $fila['tarjeta'];
        $res = obtenerNumTickets($marca,$filtro);
        if ($res > 0)
            $avgTajeta = round(($pago_tarjeta / $res) * 100 , 1);       
    }
    return $avgTajeta;
}

function obtenerGastoMedio($marca,$filtro){
    $gasto = 0;
    //Obtenemos el número de Tickets que tienen informado el campo forma de pago distinto de efectivo
    $sql = "SELECT round (SUM(total) /COUNT(*),2) AS 'gasto_medio_ticket' from TICKETS WHERE total > 0 and nombre_tienda = '$marca'".$filtro;

    if(!$resultado = mysql_query($sql)){
        die('Ocurrio un error ejecutando el query [' . $con->error . ']');
    }else{
        $fila = mysql_fetch_assoc($resultado);
        $gasto = $fila['gasto_medio_ticket'];
    }
    return $gasto;
}
function obtenerNumTicketsTotal($marca,$filtro){
    //Obtenemos el número de Tickets totales
    $sql = "SELECT COUNT(DISTINCT(T.num_ticket)) AS 'ticketTotal' from TICKETS AS T WHERE nombre_tienda = '$marca'".$filtro;

    if(!$resultado = mysql_query($sql)){
        die('Ocurrio un error ejecutando el query [' . $con->error . ']');
    }else{
        $fila = mysql_fetch_assoc($resultado);
        $num_ticketsT = $fila['ticketTotal'];
        return $num_ticketsT;
    }
}
function obtenerComprasCliente($marca,$filtro){
    $avgCompras = 0;
    //Obtenemos el número de Tickets que tienen informado el campo forma de pago distinto de efectivo
    $sql = "SELECT COUNT(DISTINCT(C.mail)) AS 'clientesMarca' from TICKETS AS T, CLIENTES AS C WHERE T.cliente = C.mail and nombre_tienda = '$marca'".$filtro;

    if(!$resultado = mysql_query($sql)){
        die('Ocurrio un error ejecutando el query [' . $con->error . ']');
    }else{
        $fila = mysql_fetch_assoc($resultado);
        $clientes_marca = $fila['clientesMarca'];
        $res = obtenerNumTicketsTotal($marca,$filtro);
        if ($res > 0)
            $avgCompras = round(($res / $clientes_marca) , 2);
        
    }
    return $avgCompras;
}
function obtenerHoraMax($marca,$filtro){
    $horaMax = 0;
    $hora = 0;
    //Obtenemos el número de Tickets que tienen informado el campo forma de pago distinto de efectivo
    $sql = "SELECT HOUR(hora) as 'hora', count(HOUR(hora)) as 'ticketsHoras' FROM `TICKETS` WHERE nombre_tienda = '$marca'".$filtro." GROUP BY HOUR(hora) ";

    if(!$resultado = mysql_query($sql)){
        die('Ocurrio un error ejecutando el query [' . $con->error . ']');
    }else{
        while($fila = mysql_fetch_assoc($resultado)){  
            if ($fila['ticketsHoras'] > $horaMax){
                $horaMax = $fila['ticketsHoras'];
                $hora = $fila['hora'];
            }
        } 
    }
    return $hora;
}
function obtenerDiaSemana($marca,$filtro){
    $dia = "";
    //DIA DE LA SEMANA CON MAS VENTAS 
    $sql = "SELECT DAYOFWEEK(fecha) as 'dia', count(DAYOFWEEK(fecha)) FROM `TICKETS` WHERE nombre_tienda = '$marca'".$filtro."GROUP BY DAYOFWEEK(fecha) ORDER BY count(DAYOFWEEK(fecha)) DESC";
    
    if(!$resultado = mysql_query($sql)){
        die('Ocurrio un error ejecutando el query [' . $con->error . ']');
    }else{
        $fila = mysql_fetch_assoc($resultado);   
        switch ($fila['dia']) {
            case 1:
                $dia = "Domingo";
                break;
            case 2:
                $dia = "Lunes";
                break;
            case 3:
                $dia = "Martes";
                break;
            case 4:
                $dia = "Miércoles";
                break;
            case 5:
                $dia = "Jueves";
                break;
            case 6:
                $dia = "Viernes";
                break;
            case 7:
                $dia = "Sábado";
                break;
            default:
                $dia = "";
                break;
        }
    }              
    return $dia;
}
    
$con->close();

?>
<div class="container">
	<div class="row">
		<div  id="capa" class="container wow fadeInUp">
			<div class="jumbotron">
			    <h1><strong>Estadísticas </strong><?php echo $literalDatos; ?></h1>

                    <div class="container2">
                        <div class="form-group">
                            <form method="POST" class="form" action="">
                                <div class="row">
                                    <div class="col-lg-3">
                                            <label class="control-label">Búsqueda por rango de días:</label>
                                            <div class="input-group">
                                                  <!-- <input type="date" class="form-control" name="dia" placeholder="Dia" required=""> -->
                                                  <!-- <input placeholder="Búsqueda por Día..." class="form-control" type="text" name="dia" onfocus="this.type='date';this.setAttribute('onfocus','');this.blur();this.focus();"> -->
                                                <span class="input-group-btn">
                                                <input type="text" id="daterange" value="">
                                                <!-- <button class="btn" type="submit" name="BuscarDia">Ver</button> -->
                                                </span>
                                            </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <label class="control-label">Búsqueda por Tiendas:</label>
                                        <div class="input-group">
                                            <select name="Tiendas" class="form-group" onchange= "this.form.submit()">
                                                <?php
                                                    while ($fila=  mysql_fetch_assoc($resultado)){

                                                        if ($primera == true){
                                                            if ($tienda == "Todas")
                                                                $seleccion = "selected";
                                                            else
                                                                $seleccion = "";

                                                            echo "<option value='Todas' ".$seleccion.">Todas</option>";

                                                            $primera = false;
                                                        }

                                                        if ($fila['mail']==$tienda)
                                                            $seleccion = "selected";
                                                        else
                                                            $seleccion = "";

                                                        echo "<option value='".$fila['mail']."' ".$seleccion.">".$fila['id_tienda']."</option>";
                                                    }
                                                 ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <label class="control-label">Búsqueda por Provincia:</label>
                                        <div class="input-group">
                                            <select name="Tiendas" class="form-group" onchange= "this.form.submit()" >
                                                <?php
                                                    while ($fila=  mysql_fetch_assoc($resultado)){

                                                        if ($primera == true){
                                                            if ($tienda == "Todas")
                                                                $seleccion = "selected";
                                                            else
                                                                $seleccion = "";

                                                            echo "<option value='Todas' ".$seleccion.">Todas</option>";

                                                            $primera = false;
                                                        }

                                                        if ($fila['mail']==$tienda)
                                                            $seleccion = "selected";
                                                        else
                                                            $seleccion = "";

                                                        echo "<option value='".$fila['mail']."' ".$seleccion.">".$fila['id_tienda']."</option>";
                                                    }
                                                 ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <label class="control-label">Buscar</label>
                                        <!-- <label class="control-label">Limpiar:</label> -->
                                        <div class="input-group">
                                            <span class="input-group-btn">
                                            <button class="btn4" type="submit" name="BuscarDia">Buscar</button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                               <!--  <div class="row">
                                    <div class="col-lg-4">
                                        <label class="control-label">Limpiar:</label>
                                        <div class="input-group">
                                            <span class="input-group-btn">
                                            <button class="btn4" type="submit" name="BuscarDia">Buscar</button>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <label class="control-label">Limpiar:</label>
                                        <div class="input-group">
                                            <span class="input-group-btn">
                                            <button type="reset" name="Limpiar" class="btn4">Limpiar</button>
                                            </span>
                                        </div>
                                    </div>
                                </div> -->
                            </form>
                            <!-- </form> -->
                        </div>

    <!-- ///// End //////////////////////////////////////////////////////////////////////////////////////// -->

                                    <div class="row">
                                        <div class="col-lg-3 col-md-6">
                                            <div class="panel panel-blue2">
                                                <div class="panel-heading">
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <i class="fa fa-female fa-3x"></i>
                                                        </div>
                                                        <div class="col-xs-9 text-right">
                                                            <div class="huge"><?php echo obtenerMujeres($nombre_marca,$filtros); ?>%</div>
                                                            <div>Clientes Mujeres</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer">
                                                    <span class=""></span>

                                                    <div class="btn-group">
                                                        <button type="button" class="btn5 dropdown-toggle" data-toggle="dropdown">
                                                            Ver Detalles <span class="caret"></span>
                                                        </button>

                                                        <ul class="dropdown-menu color1">
                                                            <li><div class="container3">Mujeres <span class="labelVerDetalles"><?php echo obtenerMujeres($nombre_marca,$filtros); ?>%</span></div></li>
                                                            <li role="separator" class="divider"></li>
                                                            <li><div class="container3">Hombres <span class="labelVerDetalles"><?php echo obtenerHombres($nombre_marca,$filtros); ?>%</span></div></li>
                                                        </ul>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-6">
                                            <div class="panel panel-blue2">
                                                <div class="panel-heading">
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <i class="fa fa-male fa-3x"></i>
                                                        </div>
                                                        <div class="col-xs-9 text-right">
                                                            <div class="huge"><?php echo obtenerHombres($nombre_marca,$filtros); ?>%</div>
                                                            <div>Clientes Hombres</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer">
                                                    <span class=""></span>

                                                    <div class="btn-group">
                                                        <button type="button" class="btn5 dropdown-toggle" data-toggle="dropdown">
                                                            Ver Detalles <span class="caret"></span>
                                                        </button>

                                                        <ul class="dropdown-menu color1">
                                                            <li><div class="container3">Mujeres <span class="labelVerDetalles"><?php echo obtenerMujeres($nombre_marca,$filtros); ?>%</span></div></li>
                                                            <li role="separator" class="divider"></li>
                                                            <li><div class="container3">Hombres <span class="labelVerDetalles"><?php echo obtenerHombres($nombre_marca,$filtros); ?>%</span></div></li>
                                                        </ul>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-6">
                                            <div class="panel panel-yellow2">
                                                <div class="panel-heading">
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <i class="fa fa-money fa-3x"></i>
                                                        </div>
                                                        <div class="col-xs-9 text-right">
                                                            <div class="huge"><?php echo obtenerGastoMedio($nombre_marca,$filtros); ?>€</div>
                                                            <div>Gasto medio ticket</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer color2">
                                                    <span class=""></span>

                                                    <div class="btn-group">
                                                        <button type="button" class="btn5 dropdown-toggle" data-toggle="dropdown">
                                                            Ver Detalles <span class="caret"></span>
                                                        </button>

                                                        <ul class="dropdown-menu color2">
                                                            <li><div class="container3">Mujeres <span class="labelVerDetalles"><?php echo obtenerMujeres($nombre_marca,$filtros); ?>%</span></div></li>
                                                            <li role="separator" class="divider"></li>
                                                            <li><div class="container3">Hombres <span class="labelVerDetalles"><?php echo obtenerHombres($nombre_marca,$filtros); ?>%</span></div></li>
                                                        </ul>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-6">
                                            <div class="panel panel-yellow2">
                                                <div class="panel-heading">
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <i class="fa fa-shopping-cart fa-3x"></i>
                                                        </div>
                                                        <div class="col-xs-9 text-right">
                                                            <div class="huge"><?php echo obtenerComprasCliente($nombre_marca,$filtros); ?></div>
                                                            <div>Compras por cliente</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer color2">
                                                    <span class=""></span>

                                                    <div class="btn-group">
                                                        <button type="button" class="btn5 dropdown-toggle" data-toggle="dropdown">
                                                            Ver Detalles <span class="caret"></span>
                                                        </button>

                                                        <ul class="dropdown-menu color2">
                                                            <li><div class="container3">Mujeres <span class="labelVerDetalles"><?php echo obtenerMujeres($nombre_marca,$filtros); ?>%</span></div></li>
                                                            <li role="separator" class="divider"></li>
                                                            <li><div class="container3">Hombres <span class="labelVerDetalles"><?php echo obtenerHombres($nombre_marca,$filtros); ?>%</span></div></li>
                                                        </ul>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-6">
                                            <div class="panel panel-green2">
                                                <div class="panel-heading">
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <i class="fa fa-user-plus fa-3x"></i>
                                                        </div>
                                                        <div class="col-xs-9 text-right">
                                                            <div class="huge"><?php $aux= obtenerEdad($nombre_marca,$filtros); echo $aux[0]; ?>%</div>
                                                            <div><?php $aux= obtenerEdad($nombre_marca,$filtros); echo 'Edad '.$aux[1]; ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer color3">
                                                    <span class=""></span>

                                                    <div class="btn-group">
                                                        <button type="button" class="btn5 dropdown-toggle" data-toggle="dropdown">
                                                            Ver Detalles <span class="caret"></span>
                                                        </button>

                                                        <ul class="dropdown-menu color3">
                                                            <li><div class="container3">Mujeres <span class="labelVerDetalles"><?php echo obtenerMujeres($nombre_marca,$filtros); ?>%</span></div></li>
                                                            <li role="separator" class="divider"></li>
                                                            <li><div class="container3">Hombres <span class="labelVerDetalles"><?php echo obtenerHombres($nombre_marca,$filtros); ?>%</span></div></li>
                                                        </ul>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-6">
                                            <div class="panel panel-green2">
                                                <div class="panel-heading">
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <i class="fa fa-credit-card fa-3x"></i>
                                                        </div>
                                                        <div class="col-xs-9 text-right">
                                                            <div class="huge"><?php echo obtenerPagoTarjeta($nombre_marca,$filtros); ?>%</div>
                                                            <div>Pago con tarjeta</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer color3">
                                                    <span class=""></span>

                                                    <div class="btn-group">
                                                        <button type="button" class="btn5 dropdown-toggle" data-toggle="dropdown">
                                                            Ver Detalles <span class="caret"></span>
                                                        </button>

                                                        <ul class="dropdown-menu color3">
                                                            <li><div class="container3">Mujeres <span class="labelVerDetalles"><?php echo obtenerMujeres($nombre_marca,$filtros); ?>%</span></div></li>
                                                            <li role="separator" class="divider"></li>
                                                            <li><div class="container3">Hombres <span class="labelVerDetalles"><?php echo obtenerHombres($nombre_marca,$filtros); ?>%</span></div></li>
                                                        </ul>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-6">
                                            <div class="panel panel-red2">
                                                <div class="panel-heading">
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <i class="fa fa-clock-o fa-3x"></i>
                                                        </div>
                                                        <div class="col-xs-9 text-right">
                                                            <div class="huge"><?php echo obtenerHoraMax($nombre_marca,$filtros); ?>-<?php echo obtenerHoraMax($nombre_marca,$filtros) +1; ?>h</div>
                                                            <div>Hora máx. ventas</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer color4">
                                                    <span class=""></span>

                                                    <div class="btn-group">
                                                        <button type="button" class="btn5 dropdown-toggle" data-toggle="dropdown">
                                                            Ver Detalles <span class="caret"></span>
                                                        </button>

                                                        <ul class="dropdown-menu color4">
                                                            <li><div class="container3">Mujeres <span class="labelVerDetalles"><?php echo obtenerMujeres($nombre_marca,$filtros); ?>%</span></div></li>
                                                            <li role="separator" class="divider"></li>
                                                            <li><div class="container3">Hombres <span class="labelVerDetalles"><?php echo obtenerHombres($nombre_marca,$filtros); ?>%</span></div></li>

                                                        </ul>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-6">
                                            <div class="panel panel-red2">
                                                <div class="panel-heading">
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <i class="fa fa-calendar fa-3x"></i>
                                                        </div>
                                                        <div class="col-xs-9 text-right">
                                                            <div class="huge"><?php echo obtenerDiaSemana($nombre_marca,$filtros); ?></div>
                                                            <div>Día de más ventas</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer color4">
                                                    <span class=""></span>

                                                    <div class="btn-group">
                                                        <button type="button" class="btn5 dropdown-toggle" data-toggle="dropdown">
                                                            Ver Detalles <span class="caret"></span>
                                                        </button>

                                                        <ul class="dropdown-menu color4">
                                                            <li><div class="container3"><h4>Dias de la Semana</h4></div></li>
                                                            <li role="separator" class="divider"></li>
                                                            <li><div class="container3">Sábado <span class="labelVerDetalles"><?php echo obtenerMujeres($nombre_marca,$filtros); ?></span> Tickets</div></li>
                                                            <li role="separator" class="divider"></li>
                                                            <li><div class="container3">Viernes <span class="labelVerDetalles"><?php echo obtenerHombres($nombre_marca,$filtros); ?></span> Tickets</div></li>
                                                        </ul>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
    <!-- ///// End //////////////////////////////////////////////////////////////////////////////////////// -->

                              <!--TABLA Estadísticas-->

<!--                                        <form method="POST" action="">
                                            <div class="form-group">
                                                <input class='form-control-static' type="month" name="mes" size="" placeholder='Mes ' required>
                                                <button class="btn" type="submit" name="BuscarMes">Ver</button>
                                            </div>
                                        </form>-->
<!--                                    </div>-->

                                    <div class="table-responsive">
                                        <table class="table" id="tabla">
                                            <thead>
                                                <tr>
                                                  <th></th>
                                                  <th>Tienda</th>
                                                  <th>Neatickets</th>
                                                  <th>Clientes Nuevos</th>
                                                  <th>Porcentaje</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <?php
                                                    $totalLeidos = 0;
                                                    $totalGenerados = 0;
                                                    $totalClientes = 0;
                                                    $numTiendas = 0;
                                                    $sumaProcentajes = 0;
                                                                                                       
                                                    switch ($Funcion) {
                                                        case "BuscarDia":
                                                            $fecha2 = $_POST['dia'];
                                                            $result = mysql_query("SELECT S.fecha_sesion, S.tienda, 
                                                                            tickets_generados, clientes_nuevos,
                                                                            ROUND(tickets_generados/Count(DISTINCT ticket) *100, 0) AS 'Porcentaje'
                                                                            FROM `HISTORICO_TICKETS` AS H, SESIONES_DESKTOP AS S WHERE
                                                                            H.`tienda`= S.tienda AND
                                                                            S.fecha_sesion = '".$fecha2."' AND
                                                                            H.timestamp like '".$fecha2."%' AND
                                                                            S.tienda like '".$nombre_marca."%' AND
                                                                            S.tienda NOT IN ('PRUEBAS','ULANKA (PRUEBAS)')
                                                                            GROUP BY H.tienda
                                                                            ORDER BY Porcentaje DESC");
                                                            while($row = mysql_fetch_row ( $result )) {

                                                                $numTiendas +=1;
                                                                echo "<tr><td>".$numTiendas."</td><td>".strtoupper($row[1])."</td><td>".$row[2]."</td><td>".$row[3]."</td><td>".$row[4]."%</td><tr>";
                                                                $totalGenerados = $totalGenerados + $row[2];
                                                                $totalClientes = $totalClientes + $row[3]; 
                                                                $sumaProcentajes = $sumaProcentajes + $row[4]; 

                                                            } 

                                                            break;

                                                        case "BuscarSemana":
                                                        case "BuscarMes": 
                                                        default: //BurcarGeneral
                                                            //Obenetemos los registros leidos por semana/Mes/General y tienda
                                                            $resultLeidos = mysql_query("SELECT H.tienda, Count(DISTINCT ticket) AS 'leidos'
                                                                        FROM `HISTORICO_TICKETS` AS H WHERE
                                                                        H.tienda like '".$nombre_marca."%' AND
                                                                        H.tienda NOT IN ('PRUEBAS','ULANKA (PRUEBAS)')
                                                                        ".$filtrosEst."
                                                                        GROUP BY H.tienda;");
                                                            $arrayTiendas = array();
                                                            $arrayLeidos = array();
                                                            $i = 0;
                                                            while($rowL = mysql_fetch_row ( $resultLeidos )) {
                                                                $arrayTiendas[$i] = $rowL[0];
                                                                $arrayLeidos[$i] = $rowL[1];  
                                                                $i += 1;
                                                            }
                                                            $array_pares = array_combine($arrayTiendas,$arrayLeidos);
                                                            //Obtenemos los datos y porcentajes de las tiendas por semana/Mes/General
                                                            $result = mysql_query("SELECT S.tienda, 
                                                                    SUM(tickets_generados), SUM(clientes_nuevos)
                                                                    FROM SESIONES_DESKTOP AS S WHERE
                                                                    S.tienda like '".$nombre_marca."%' AND
                                                                    S.tienda NOT IN ('PRUEBAS','ULANKA (PRUEBAS)')
                                                                    ".$filtrosEst2."
                                                                    GROUP BY S.tienda ORDER BY SUM(tickets_generados) DESC");
                                                            while($row = mysql_fetch_row ( $result )) {
                                                                $numTiendas +=1;
                                                                $porcentajeAux = 0;
                                                                //echo var_dump($array_pares[$row[0]]);
                                                                if ($array_pares[$row[0]])
                                                                    $porcentajeAux = round(($row[1] / $array_pares[$row[0]]) *100, 0);

                                                                echo "<tr><td>".$numTiendas."</td><td>".strtoupper($row[0])."</td><td>".$row[1]."</td><td>".$row[2]."</td><td>".$porcentajeAux."%</td><tr>";
                                                                $totalGenerados = $totalGenerados + $row[1];
                                                                $totalClientes = $totalClientes + $row[2]; 
                                                                $sumaProcentajes = $sumaProcentajes + $porcentajeAux; 

                                                            } 
                                                            break;
                                                        
                                                    }                                                                                                
                                                    
                                                    echo "<tr><td></td><td><b><big>TOTAL</big></b></td><td><b><big>".$totalGenerados."</big></b></td><td><b><big>".$totalClientes."</big></b></td><td></td><td></td><td></td><tr>";
                                                    
                                                    if ($numTiendas > 0){
                                                        $avgGenerados = round($totalGenerados / $numTiendas , 1);
                                                        $avgNuevos = round($totalClientes / $numTiendas, 1);
                                                        $avgPorcentaje = round($sumaProcentajes / $numTiendas, 0);
                                                    }

                                                    echo "<tr><td></td><td><b><big>MEDIA</big></b></td><td><b><big>".$avgGenerados."</big></b></td><td><b><big>".$avgNuevos."</big></b></td><td><b><big>".$avgPorcentaje."%</big></b></td><td></td><td></td><tr>";

                                                 ?>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="form-group"><br>
                                        <a href="index2emp.php#login"><button name="boton2" id="boton2" type="button" class="btn">
                                        <span class="typcn typcn-arrow-left"></span> Volver</button></a>
                                    </div>


                                </div>

			</div>
		</div>
	</div>
</div>
