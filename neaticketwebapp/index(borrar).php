<?php

// include('lib/conexion.php');
// include('lib/conexion3.php');

include('class/connectA.php');
$clase = new connect;
$clase->dbConnects();

?>

<?php
/*
 * @author Puneet Mehta
 * @website: http://www.PHPHive.info
 * @facebook: https://www.facebook.com/pages/PHPHive/1548210492057258
 */

?>

<!DOCTYPE html>
<html lang="en">

    <head>

        <?php include("lib/headNew.php"); ?>

    </head>

    <body>

        <!-- Top menu -->
        <nav class="navbar navbar-inverse navbar-no-bg" role="navigation">

            <?php include("lib/navNew.php"); ?>

        </nav>

        <!-- Loader -->
        <div class="loader">

            <div class="loader-img"></div>

        </div>

        <!-- Top content -->
        <div class="top-content">

            <?php include("lib/contentNew.php"); ?>

        </div>

        <!-- MODAL: Privacy policy -->
          <?php include("lib/modalNew.php"); ?>

        <!-- Javascript -->
        <script type="" src="assetsNew/js/jquery-1.11.3.min.js"></script>
        <script type="" src="assetsNew/bootstrap/js/bootstrap.min.js"></script>
        <script type="" src="assetsNew/js/jquery.backstretch.min.js"></script>
        <script type="" src="assetsNew/js/retina.min.js"></script>
        <script type="" src="assetsNew/js/wow.min.js"></script>
        <script type="" src="assetsNew/js/scripts.js"></script>
        <script type="" src="assetsNew/js/scripts-screen.js"></script>
        <!-- <script type="" src="assetsNew/js/scripts-modal.js"></script> -->

        <!-- Bootstrap Core JavaScript -->
        <script src="assetsNew/bootstrap/js/bootstrap.js" type="text/javascript"></script>

        <!-- Bootstrap Carousel ================================================== -->
        <script>
        $('.carousel').carousel({
          interval: 3500
        })
        </script>

        <!-- Plugin JavaScript -->
        <!-- <script type="" src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js" ></script>
        <script type="" src="assets/js/classie.js"></script>
        <script type="" src="assets/js/cbpAnimatedHeader.js"></script> -->

        <!-- Contact Form JavaScript -->
        <!-- <script src="assets/js/jqBootstrapValidation.js" type="text/javascript"></script> -->

        <!-- Custom Theme JavaScript -->
        <!-- <script src="assets/js/agency.js" type="text/javascript"></script> -->

        <!--[if lt IE 10]>
            <script src="assetsNew/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>