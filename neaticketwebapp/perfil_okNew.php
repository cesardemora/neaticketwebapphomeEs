<?php

    mysql_query("SET NAMES 'utf8'");
    $consulta = "SELECT * FROM SECTORES ORDER BY id ASC";
    $resultado = mysql_query($consulta);

    if(isset($_POST['Sectores']))
        $sector = $_POST['Sectores'];
    else
        $sector = "Todos";


?>

<div class="container">
    <div class="row">
        <div  id="capa" class="container wow fadeInUp">
            <div class="jumbotron">
                
                <h1>Listado de <strong>Tickets</strong></h1>
                <?php
                    echo '<h3>'.$_SESSION['emailC'].'</h3>';
                 ?>

                <div class="buscar">
                    <form method="POST" class="form-inline" action="index_buscarTickets.php">

                        <div class="form-group">
                          <input class='form-control' type="text" name="nombreComercio" size="25" placeholder='escribe un Comercio'>
                        </div>

                        <div class="form-group">
                                  <button class="btn" type="submit" name="BuscarComercio">Buscar</button>
                        </div>

                        <div class="form-group">
                          <input class='form-control-static' type="date" name="fecha_compra" size="25" placeholder='fecha compra'>
                        </div>

                        <div class="form-group">
                          <button class="btn" type="submit" name="BuscarFecha">Buscar</button>
                        </div>

                    </form><br>
                    <form method="POST" class="form-inline" action="index2Tickets.php">
                        <label for="sector_name">Sector: </label>
                        <select name="Sectores" class="form-group" onchange = "this.form.submit()" >

                            <?php
                                while ($fila=  mysql_fetch_assoc($resultado)){

                                    if ($fila['nombre']==$sector)
                                        $seleccion = "selected";
                                    else 
                                        $seleccion = "";

                                    echo "<option value='".$fila['nombre']."' ".$seleccion.">".$fila['nombre']."</option>";
                                }
                             ?>
                        </select>
                    </form>

                </div>

                <!-- Table -->
                <div class="table-responsive">
                    <table class="table">
                        <?php

                            $nick = $_SESSION['nick'];
                            $email = $_SESSION['emailC'];

                            if(isset($_POST['Sectores']) && $sector != "Todos")
                                $result = mysql_query("SELECT * FROM TICKETS WHERE estado='activo' AND cliente='".$email."' AND sector='".$sector."' ORDER BY fecha DESC, hora DESC");
                            else
                                $result = mysql_query("SELECT * FROM TICKETS WHERE estado='activo' AND cliente='".$email."' ORDER BY fecha DESC, hora DESC");


                            if ($row = mysql_fetch_array($result)){

                                echo "<thead><tr>

                                        <td><strong>COMERCIO</strong></td>
                                        <td><strong>TICKET</strong></td>
                                        <td><strong>FECHA</strong></td>
                                        <td><strong>HORA</strong></td>
                                        <td><strong>PVP</strong></td>
                                        <td></td>
                                        </tr></thead>";
                                do {

                                    $date = date("d/m/Y", strtotime($row['fecha']));

                                    echo '<td>'.$row['nombre_tienda'].'</td>';
                                    echo '<td>'.$row['num_ticket'].'</td>';
                                    echo '<td>'.$date.'</td>';
                                    echo '<td>'.$row['hora'].'</td>';
                                    echo '<td>'.$row['total'].'</td>';

                                    if ($row['fichero'] != "")
                                        echo '<td><a href="'.$row['fichero'].'" target="blank" class="typcn typcn-zoom-in"></a></td></tr>';
                                    else
                                        echo '<td><a href="ticketPdf_ok_1.php?id='.$row['id'].'&tipo=1" target="blank" class="typcn typcn-zoom-in"></a></td></tr>';

                                        // <a href="#"><span class="typcn typcn-vendor-apple"></span></a>
                                        //echo '<td><a href="reporte_historial.php?id='.$row['id'].'">ver</a></td></tr>';

                                } while ($row = mysql_fetch_array($result));

                            } else {
                                echo "¡ No se ha encontrado ningún registro !";
                            }

                        ?>

                    </table>
                </div>

                <div>
                    <a href="index.php#login"><button name="boton2" id="boton2" type="button" class="btn">
                    <span class="typcn typcn-arrow-left"></span> Volver</button></a>
                </div>

            </div>
        </div>
    </div>
</div>
