<?php

include_once('database.class.php');

class empresas{

//Función  que permite traer todos los empresas en la tabla

    public function getJSONEmpresas(){
        
        $client['datos'] = array();

	$sql = "SELECT * FROM EMPRESAS";
	$db = new database();
	$res = $db->ejecutarConsulta($sql);
        
        while( $row = mysql_fetch_array($res) ) {
            array_push($client['datos'], array('cif' => $row['cif'], 'nombre' => $row['nombre'], 'mail' => $row['mail'], 'password' => $row['password'], 'logo' => base64_encode($row['logo']), 'fecha_alta' => $row['fecha_alta'], 'fecha_baja' => $row['fecha_baja'], 'tipo_logo' => $row['tipo_logo'], 'datos_fiscales' => $row['datos_fiscales'], 'nombre_fiscal' => $row['nombre_fiscal'], 'direccion_fiscal' => $row['direccion_fiscal'], 'cp_fiscal' => $row['cp_fiscal'], 'localidad_fiscal' => $row['localidad_fiscal']));           
        }

        mysql_free_result($res);
        
        return $client;
    }
//Función  que devuelve una empresa filtrado por cif


    public function getJSONEmpresa($cif){
        
        $client['datos'] = array();

	$sql = "SELECT * FROM EMPRESAS WHERE cif= '".$cif."'";
	$db = new database();
	$res = $db->ejecutarConsulta($sql);
        
        while( $row = mysql_fetch_array($res) ) {
            array_push($client['datos'], array('cif' => $row['cif'], 'nombre' => $row['nombre'], 'mail' => $row['mail'], 'password' => $row['password'], 'logo' => base64_encode($row['logo']), 'fecha_alta' => $row['fecha_alta'], 'fecha_baja' => $row['fecha_baja'], 'tipo_logo' => $row['tipo_logo'], 'datos_fiscales' => $row['datos_fiscales'], 'nombre_fiscal' => $row['nombre_fiscal'], 'direccion_fiscal' => $row['direccion_fiscal'], 'cp_fiscal' => $row['cp_fiscal'], 'localidad_fiscal' => $row['localidad_fiscal']));         }
        mysql_free_result($res);
        
        return $client;
    }
}
?>