<?php

include_once('database.class.php');

class tickets{

//Función  que permite traer todos los tickets en la tabla

    public function getJSONTickets(){
	$json['datos'] = array(); 
        
	$sql = "SELECT * FROM TICKETS ORDER BY fecha DESC, hora DESC";
	$db = new database();
	$res = $db->ejecutarConsulta($sql);
        
        while( $row = mysql_fetch_array($res)) {
            array_push($json['datos'], array('id' => $row['id'], 'num_ticket' => $row['num_ticket'], 'mail_tienda' => $row['mail_tienda'], 'nombre_tienda' => $row['nombre_tienda'], 'sector' => $row['sector'], 'sub_sector' => $row['sub_sector'], 'fecha' => $row['fecha'], 'hora' => $row['hora'], 'caja' => $row['caja'], 'cajero' => $row['cajero'],  'operacion' => $row['operacion'],  'id_oferta' => $row['id_oferta'],  'cliente' => $row['cliente'],  'total' => $row['total'],  'entregado' => $row['entregado'], 'cambio' => $row['cambio'], 'aviso_devolucion' => $row['aviso_devolucion'], 'aviso_garantia' => $row['aviso_garantia'], 'recurso_imagen' => base64_encode($row['recurso_imagen']), 'estado' => $row['estado'], 'tipo_iva' => $row['tipo_iva'], 'datadi' => $row['datadi'], 'datadi2' => $row['datadi2'], 'num_tarjeta' => $row['num_tarjeta'], 'fichero' => $row['fichero']));           
        }
        mysql_free_result($res);
        
        return $json; 
    }   

//Función  que devuelve los tickets asociados a un cliente

    public function getJSONTicketCliente($mail){
	$json['datos'] = array(); 
        
	$sql = "SELECT * FROM TICKETS WHERE cliente= '".$mail."' AND estado='activo' ORDER BY fecha DESC, hora DESC";
	$db = new database();
	$res = $db->ejecutarConsulta($sql);
        
        while( $row = mysql_fetch_array($res)) {
            array_push($json['datos'], array('id' => $row['id'], 'num_ticket' => $row['num_ticket'], 'mail_tienda' => $row['mail_tienda'], 'nombre_tienda' => $row['nombre_tienda'], 'sector' => $row['sector'], 'sub_sector' => $row['sub_sector'], 'fecha' => $row['fecha'], 'hora' => $row['hora'], 'caja' => $row['caja'], 'cajero' => $row['cajero'],  'operacion' => $row['operacion'],  'id_oferta' => $row['id_oferta'],  'cliente' => $row['cliente'],  'total' => $row['total'],  'entregado' => $row['entregado'], 'cambio' => $row['cambio'], 'aviso_devolucion' => $row['aviso_devolucion'], 'aviso_garantia' => $row['aviso_garantia'], 'recurso_imagen' => base64_encode($row['recurso_imagen']), 'estado' => $row['estado'], 'tipo_iva' => $row['tipo_iva'], 'datadi' => $row['datadi'], 'datadi2' => $row['datadi2'], 'num_tarjeta' => $row['num_tarjeta'], 'fichero' => $row['fichero']));           
        }
        mysql_free_result($res);
        
        return $json;
    }
    
//Función  que devuelve un ticket filtrado por mail

    public function getJSONTicketId($id){
	$json['datos'] = array(); 
        
	$sql = "SELECT * FROM TICKETS WHERE id= '".$id."'";
	$db = new database();
	$res = $db->ejecutarConsulta($sql);
        
        while( $row = mysql_fetch_array($res)) {
            array_push($json['datos'], array('id' => $row['id'], 'num_ticket' => $row['num_ticket'], 'mail_tienda' => $row['mail_tienda'], 'nombre_tienda' => $row['nombre_tienda'], 'sector' => $row['sector'], 'sub_sector' => $row['sub_sector'], 'fecha' => $row['fecha'], 'hora' => $row['hora'], 'caja' => $row['caja'], 'cajero' => $row['cajero'],  'operacion' => $row['operacion'],  'id_oferta' => $row['id_oferta'],  'cliente' => $row['cliente'],  'total' => $row['total'],  'entregado' => $row['entregado'], 'cambio' => $row['cambio'], 'aviso_devolucion' => $row['aviso_devolucion'], 'aviso_garantia' => $row['aviso_garantia'], 'recurso_imagen' => base64_encode($row['recurso_imagen']), 'estado' => $row['estado'], 'tipo_iva' => $row['tipo_iva'], 'datadi' => $row['datadi'], 'datadi2' => $row['datadi2'], 'num_tarjeta' => $row['num_tarjeta'], 'fichero' => $row['fichero']));           
        }
        mysql_free_result($res);
        
        return $json;
    }

    public function getJSONActualizarTicket($id){
	$json['datos'] = array(); 
        
	$sql = "UPDATE TICKETS SET estado='borrado' WHERE id= '".$id."'";
	$db = new database();
	$res = $db->incluirRegistro($sql);
        
       return $res;
       
    }
}
?>