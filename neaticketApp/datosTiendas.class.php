<?php header('Content-type: application/json');

    include_once('tiendas.class.php');

    $tiendas = new tiendas();

    $dato = $_GET['tipo'];

    switch ($dato){
        case "getTienda":
            //La Siguiente Línea pasa los datos a Formato JSON, y son enviados al Dispositivo
            $mail1 = $_REQUEST['mail'];
            echo json_encode($tiendas->getJSONTienda($mail1));
            break;
        case "mostrarTiendas":
            //La Siguiente Línea pasa los datos a Formato JSON, y son enviados al Dispositivo
            echo json_encode($tiendas->getJSONTiendas());
            break;

    }
?>