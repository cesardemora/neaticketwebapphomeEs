<?php

include_once('database.class.php');

class ofertas{

//Función  que permite traer todos los ofertas en la tabla

    public function getJSONOfertas(){
	$json['datos'] = array(); 
        
        $sql = "SELECT * FROM OFERTAS ORDER BY fecha_ini DESC";
	$db = new database();
	$res = $db->ejecutarConsulta($sql);
        
        while( $row = mysql_fetch_row($res)) {
            array_push($json['datos'], array('id' => $row[0], 'nombre' => $row[1], 'cif_empresa' => $row[2], 'estado' => $row[3], 'ambito' => $row[4], 'texto' => $row[5], 'imagen_oferta' => base64_encode($row[6]), 'logo_empresa' => base64_encode($row[7]), 'fecha_ini' => $row[8], 'fecha_fin' => $row[9], 'nombre_marca' => $row[10]));           
        }
        mysql_free_result($res);
        
        return $json;
    }
//Función  que devuelve los ofertas de un mismo CIF asociados a un cliente
//Esta función puede desaparecer despues de la subida de la versión de la APP 1.3.4

    public function getJSONOfertaCliente($mail){
	$json['datos'] = array(); 
        
	//$sql = "SELECT O.id, nombre, cif_empresa, O.estado, ambito, texto, fecha_ini, fecha_fin, imagen_oferta, T.recurso_imagen, T.id , T.sector FROM TICKETS AS T, OFERTAS AS O WHERE id_oferta= O.id AND cliente= '".$mail."' AND fecha_fin >= CURRENT_DATE GROUP BY O.id ORDER BY fecha_ini DESC";
        $sql = "SELECT O.id, nombre, O.cif_empresa, O.estado, ambito, texto, fecha_ini, fecha_fin, imagen_oferta, T.recurso_imagen, T.id , T.sector FROM TICKETS AS T, OFERTAS AS O , TIENDAS AS TI WHERE cliente= '".$mail."' AND T.mail_tienda = TI.mail AND TI.cif_empresa = O.cif_empresa AND O.fecha_fin >= CURRENT_DATE GROUP BY O.id ORDER BY O.fecha_ini DESC";

        $db = new database();
	$res = $db->ejecutarConsulta($sql);
        
        while( $row = mysql_fetch_row($res) ) {
            array_push($json['datos'], array('id' => $row[0], 'nombre' => $row[1], 'cif_empresa' => $row[2], 'estado' => $row[3], 'ambito' => $row[4], 'texto' => $row[5], 'imagen_oferta' => base64_encode($row[8]), 'logo_empresa' => base64_encode($row[9]), 'fecha_ini' => $row[6], 'fecha_fin' => $row[7], 'id_ticket' => $row[10], 'sector' => $row[11]));           
        }
        mysql_free_result($res);
        
        return $json; 
    }
//Función  que devuelve los ofertas de una misma Marca asociados a un cliente

    public function getJSONOfertaMarcaCliente($mail){
	$json['datos'] = array(); 
        
        $sql = "SELECT O.id, nombre, O.cif_empresa, O.estado, ambito, texto, fecha_ini, fecha_fin, imagen_oferta, T.recurso_imagen, T.id , T.sector, O.nombre_marca FROM TICKETS AS T, OFERTAS AS O , TIENDAS AS TI WHERE cliente= '".$mail."' AND T.mail_tienda = TI.mail AND TI.nombre_marca = O.nombre_marca AND O.fecha_fin >= CURRENT_DATE GROUP BY O.id ORDER BY O.fecha_ini DESC";

        $db = new database();
	$res = $db->ejecutarConsulta($sql);
        
        while( $row = mysql_fetch_row($res) ) {
            array_push($json['datos'], array('id' => $row[0], 'nombre' => $row[1], 'cif_empresa' => $row[2], 'estado' => $row[3], 'ambito' => $row[4], 'texto' => $row[5], 'imagen_oferta' => base64_encode($row[8]), 'logo_empresa' => base64_encode($row[9]), 'fecha_ini' => $row[6], 'fecha_fin' => $row[7], 'id_ticket' => $row[10], 'sector' => $row[11], 'nombre_marca' => $row[12]));           
        }
        mysql_free_result($res);
        
        return $json; 
    }

//Función  que devuelve un oferta filtrado por id
    public function getJSONOfertaId($id){
	$json['datos'] = array(); 
        
        $sql = "SELECT * FROM OFERTAS WHERE id= '".$id."'";
	$db = new database();
	$res = $db->ejecutarConsulta($sql);
        
        while( $row = mysql_fetch_row($res) ) {
            array_push($json['datos'], array('id' => $row[0], 'nombre' => $row[1], 'cif_empresa' => $row[2], 'estado' => $row[3], 'ambito' => $row[4], 'texto' => $row[5], 'imagen_oferta' => base64_encode($row[6]), 'logo_empresa' => base64_encode($row[7]), 'fecha_ini' => $row[8], 'fecha_fin' => $row[9], 'nombre_marca' => $row[10]));           
        }
        mysql_free_result($res);
        
        return $json;
    }
}
?>