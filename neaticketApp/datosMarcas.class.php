<?php header('Content-type: application/json');
//header('Content-Type: text/html; charset=UTF-8');

    include_once('marcas.class.php');

    $marcas = new marcas();

    $dato = $_GET['tipo'];

    switch ($dato){
        case "getMarcaNombre":
            //La Siguiente Línea pasa los datos a Formato JSON, y son enviados al Dispositivo
            $nombreM = $_REQUEST['Nombre'];
            echo json_encode($marcas->getJSONMarca($nombreM));
            break;
        case "mostrarMarcas":
            //La Siguiente Línea pasa los datos a Formato JSON, y son enviados al Dispositivo
            echo json_encode($marcas->getJSONMarcas());
            break;

    }
?>