<?php

include_once('database.class.php');

class avisos{

//Función  que permite traer todos los avisos en la tabla

    private function getAvisos(){
	$sql = "SELECT * FROM AVISOS ORDER BY fecha_vencimiento ASC";
	$db = new database();
	return $db->ejecutarConsulta($sql);
	}
//Función  que devuelve un aviso filtrado por mail

    private function getAviso($mail){
	$sql = "SELECT * FROM AVISOS WHERE cliente= '".$mail."' AND estado IN ('activo','notificado') AND fecha_vencimiento >= CURRENT_DATE ORDER BY fecha_vencimiento ASC";
	$db = new database();
	return $db->ejecutarConsulta($sql);
	}

    public function getJSONAvisos(){
	$json = array(); 	
	$result = $this-> getAvisos();		
	if(mysql_num_rows($result)){
		while($row=mysql_fetch_assoc($result)){
		$json['datos'][]=$row;	
		}
	}
        return $json; 
    }
    public function getJSONAvisosCliente($mail){
	$json = array(); 	
	$result = $this-> getAviso($mail);		
	if(mysql_num_rows($result)){
		while($row=mysql_fetch_assoc($result)){
		$json['datos'][]=$row;	
		}
	}
        return $json; 
    }
    
    public function getJSONActualizarAvisos($id){ 
        
	$sql = "UPDATE AVISOS SET estado='borrado' WHERE id_ticket= '".$id."'";
	$db = new database();
	$res = $db->incluirRegistro($sql);
        
       return $res;
       
    }
    
    public function getJSONActualizarAviso($id){ 
        
	$sql = "UPDATE AVISOS SET estado='borrado' WHERE id= '".$id."'";
	$db = new database();
	$res = $db->incluirRegistro($sql);
        
       return $res;
       
    }
}
?>