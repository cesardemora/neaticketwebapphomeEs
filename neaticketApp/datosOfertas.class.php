<?php header('Content-type: application/json');

    include_once('ofertas.class.php');

    $ofertas = new ofertas();

    $dato = $_GET['tipo'];

    switch ($dato){
        case "getOfertasCliente":
            //La Siguiente Línea pasa los datos a Formato JSON, y son enviados al Dispositivo
            $mail1 = $_REQUEST['mail'];
            echo json_encode($ofertas->getJSONOfertaCliente($mail1));
            break;
        case "getOfertasMarcaCliente":
            //La Siguiente Línea pasa los datos a Formato JSON, y son enviados al Dispositivo
            $mail2 = $_REQUEST['mail'];
            echo json_encode($ofertas->getJSONOfertaMarcaCliente($mail2));
            break;
        case "getOfertaId":
            //La Siguiente Línea pasa los datos a Formato JSON, y son enviados al Dispositivo
            $id = $_REQUEST['id'];
            echo json_encode($ofertas->getJSONOfertaId($id));
            break;
        case "mostrarOfertas":
            //La Siguiente Línea pasa los datos a Formato JSON, y son enviados al Dispositivo
            echo json_encode($ofertas->getJSONOfertas());
            break;

    }
?>