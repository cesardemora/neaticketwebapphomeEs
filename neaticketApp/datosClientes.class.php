<?php header('Content-Type: text/html; charset=UTF-8');

    include_once('clientes.class.php');

    $clientes = new clientes();

    $dato = $_GET['tipo'];

    switch ($dato){
    case "addCliente":
       $parametro1 = $_REQUEST['mail'];
       $parametro2 = $_REQUEST['nombre'];
       $parametro3 = $_REQUEST['pass'];
       if ($parametro2 =="") $parametro2 = $parametro1;
       if ($parametro3 =="") $parametro3 = $parametro1;
       $cadena = str_replace("%20"," ",$parametro2);

       $add = $clientes->setClienteSQL($parametro1,$cadena,$parametro3);      
       echo ("Parametros registrados -->" .$add);
       break;
    case "actualizarCliente":
       $parametro1 = $_REQUEST['mail'];
       $parametro2 = $_REQUEST['campo'];
       $parametro3 = $_REQUEST['valor'];

       $add = $clientes->updateCliente($parametro1,$parametro2,$parametro3);      
       echo json_encode("Parametros registrados -->" .$add);
       break;
    case "getCliente":
       //La Siguiente Línea pasa los datos a Formato JSON, y son enviados al Dispositivo
       $mail1 = $_REQUEST['mail'];
       echo json_encode($clientes->getJSONCliente($mail1));
       break;
    case "mostrarClientes":
       //La Siguiente Línea pasa los datos a Formato JSON, y son enviados al Dispositivo
       echo json_encode($clientes->getJSONClientes());
       break;

    }
?>