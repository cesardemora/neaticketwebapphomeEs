<?php header('Content-Type: text/html; charset=UTF-8');

    include_once('avisos.class.php');

    $avisos = new avisos();

    $dato = $_GET['tipo'];

    switch ($dato){
        case "getAvisosCliente":
            //La Siguiente Línea pasa los datos a Formato JSON, y son enviados al Dispositivo
            $mail1 = $_REQUEST['mail'];
            echo json_encode($avisos->getJSONAvisosCliente($mail1));
            break;
        case "mostrarAvisos":
            //La Siguiente Línea pasa los datos a Formato JSON, y son enviados al Dispositivo
            echo json_encode($avisos->getJSONAvisos());
            break;
        case "borrarAvisosIdTicket":
            //La Siguiente Línea pasa los datos a Formato JSON, y son enviados al Dispositivo
            $id = $_REQUEST['id'];
            echo json_encode($avisos->getJSONActualizarAvisos($id));
            break;
        case "borrarAviso":
            //La Siguiente Línea pasa los datos a Formato JSON, y son enviados al Dispositivo
            $id = $_REQUEST['id'];
            echo json_encode($avisos->getJSONActualizarAviso($id));
            break;

    }
?>