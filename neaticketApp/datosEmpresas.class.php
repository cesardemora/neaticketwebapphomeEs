<?php header('Content-type: application/json');
//header('Content-Type: text/html; charset=UTF-8');

    include_once('empresas.class.php');

    $empresas = new empresas();

    $dato = $_GET['tipo'];

    switch ($dato){
        case "getEmpresaCif":
            //La Siguiente Línea pasa los datos a Formato JSON, y son enviados al Dispositivo
            $cif1 = $_REQUEST['cif'];
            echo json_encode($empresas->getJSONEmpresa($cif1));
            break;
        case "mostrarEmpresas":
            //La Siguiente Línea pasa los datos a Formato JSON, y son enviados al Dispositivo
            echo json_encode($empresas->getJSONEmpresas());
            break;

    }
?>