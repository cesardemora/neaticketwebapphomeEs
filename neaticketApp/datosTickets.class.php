<?php header('Content-type: application/json');

    include_once('tickets.class.php');

    $tickets = new tickets();

    $dato = $_GET['tipo'];

    switch ($dato){
        case "getTicketsCliente":
            //La Siguiente Línea pasa los datos a Formato JSON, y son enviados al Dispositivo
            $mail1 = $_REQUEST['mail'];
            echo json_encode($tickets->getJSONTicketCliente($mail1));
            break;
        case "getTicketId":
            //La Siguiente Línea pasa los datos a Formato JSON, y son enviados al Dispositivo
            $id = $_REQUEST['id'];
            echo json_encode($tickets->getJSONTicketId($id));
            break;
        case "borrarTicketId":
            //La Siguiente Línea pasa los datos a Formato JSON, y son enviados al Dispositivo
            $id = $_REQUEST['id'];
            echo json_encode($tickets->getJSONActualizarTicket($id));
            break;
        case "mostrarTickets":
            //La Siguiente Línea pasa los datos a Formato JSON, y son enviados al Dispositivo
            echo json_encode($tickets->getJSONTickets());
            break;

    }
?>